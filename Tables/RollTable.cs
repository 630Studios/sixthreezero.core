﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif



namespace SixThreeZero.Core.Tables
{
    [System.Serializable]
    public class RollTableEntry
    {
        public int MinRoll = 0;
        public int MaxRoll = 10;
       
    }

    [System.Serializable]
    public class RollTableEntry<T>: RollTableEntry
    {
        public T Target;

        public RollTableEntry()
        {
            Target = default(T);
        }
    }

    [System.Serializable]
    public class RollTable<T>  where T: RollTableEntry
    {
        

        public List<T> Entries = new List<T>();

        public void SetMinRollDown(int index, int value)
        {
            if (index != 0)
            {
                if (Entries[index - 1].MaxRoll >= value)
                    SetMaxRollDown(index - 1, value - 1);
            }
            Entries[index].MinRoll = value;

            if (value > Entries[index].MaxRoll)
                SetMaxRollDown(index, value);
        }


        public void SetMaxRollDown(int index, int value)
        {
            if (Entries[index].MinRoll > value)
                SetMinRollDown(index, value);

            Entries[index].MaxRoll = value;

            if (index != Entries.Count - 1)
            {
                if (value > Entries[index + 1].MinRoll)
                    SetMinRollDown(index + 1, value + 1);
            }

        }

        public T AddEntry()
        {
            
            T newEntry = System.Activator.CreateInstance<T>();
            if (Entries.Count == 0)
            {
                newEntry.MinRoll = 0;
                newEntry.MaxRoll = 10;
            }else
            {
                newEntry.MinRoll = Entries[Entries.Count - 1].MaxRoll + 1;
                newEntry.MaxRoll = newEntry.MinRoll;
            }
            Entries.Add(newEntry);
            return newEntry;
        }

        public void MoveEntryUp(int index)
        {
            if (index == 0 || index >= Entries.Count)
                return;

            T entry = Entries[index];
            T oEntry = Entries[index - 1];
            int minRoll = entry.MinRoll;
            int maxRoll = entry.MaxRoll;
            Entries.RemoveAt(index);
            entry.MinRoll = oEntry.MinRoll;
            entry.MaxRoll = oEntry.MaxRoll;
            oEntry.MinRoll = minRoll;
            oEntry.MaxRoll = maxRoll;
            Entries.Insert(index - 1, entry);

        }

        public void MoveEntryDown(int index)
        {
            if (index < 0 || index > Entries.Count - 2)
                return;

            T entry = Entries[index];
            T oEntry = Entries[index + 1];
            int minRoll = entry.MinRoll;
            int maxRoll = entry.MaxRoll;
            Entries.RemoveAt(index);
            entry.MinRoll = oEntry.MinRoll;
            entry.MaxRoll = oEntry.MaxRoll;
            oEntry.MinRoll = minRoll;
            oEntry.MaxRoll = maxRoll;
            Entries.Insert(index + 1, entry);

        }

        public T GetEntryFromRoll(int roll)
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                if (roll >= Entries[i].MinRoll && roll <= Entries[i].MaxRoll)
                    return Entries[i];
            }

            return null;
        }
        

    }







}


