﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

using UnityEditor;




namespace SixThreeZero.Core.Tables
{


    public class RollTableView<T> where T : RollTableEntry
    {

        public delegate void ViewEntryDelegate(T entryView);


        public static void Display(RollTable<T> table, ViewEntryDelegate entryView)
        {


            //table.name = EditorGUILayout.TextField("Name", table.name);
            int newVal;

            for (int i = 0; i < table.Entries.Count; i++)
            {
                GUILayout.BeginHorizontal();

                EditorGUI.BeginChangeCheck();
                newVal = EditorGUILayout.IntField(table.Entries[i].MinRoll, GUILayout.Width(40));
                if (EditorGUI.EndChangeCheck())
                    table.SetMinRollDown(i, newVal);

                EditorGUI.BeginChangeCheck();
                newVal = EditorGUILayout.IntField(table.Entries[i].MaxRoll, GUILayout.Width(40));
                if (EditorGUI.EndChangeCheck())
                    table.SetMaxRollDown(i, newVal);


                entryView(table.Entries[i]);
                //table.Entries[i].Target = (GameObject)EditorGUILayout.ObjectField(table.Entries[i].Target, typeof(GameObject), false);


                if (i != 0)
                {
                    if (GUILayout.Button("U", EditorStyles.miniButtonLeft, GUILayout.Width(24)))
                        table.MoveEntryUp(i);
                }
                else
                    GUILayout.Space(24);

                if (i != (table.Entries.Count - 1))
                {
                    if (GUILayout.Button("D", EditorStyles.miniButtonRight, GUILayout.Width(24)))
                    {
                        table.MoveEntryDown(i);
                    }
                }
                else
                    GUILayout.Space(24);
                 

                GUILayout.EndHorizontal();
            }
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Add Entry", EditorStyles.miniButton))
            {
                table.AddEntry();
                AssetDatabase.SaveAssets();
            }
            GUILayout.EndHorizontal();
        }


    }




}