﻿using UnityEngine;
using UnityEditor;
using SixThreeZero.Core;

namespace SixThreeZero.Core.Tables
{
    [CustomEditor(typeof(DRollTable))]
    public class DRollTableInspector : Editor
    {


        [MenuItem("Assets/Create/630Studios/DRollTable")]
        public static void CreateDRollTable()
        {
            ScriptableObjectUtility.CreateAsset<DRollTable>();
        }

        public static DRollTable CreateNewDRollTable()
        {
            return ScriptableObjectUtility.CreateAsset<DRollTable>();
        }

        public override void OnInspectorGUI()
        {

            DRollTable table = (DRollTable)target;
            EditorGUI.BeginChangeCheck();
            table.name = EditorGUILayout.TextField("Name", table.name);
            int newVal;

            for (int i = 0; i < table.Entries.Count; i++)
            {
                GUILayout.BeginHorizontal();

                EditorGUI.BeginChangeCheck();
                GUI.SetNextControlName("txtMinRoll" + i);
                newVal = EditorGUILayout.IntField(table.Entries[i].MinRoll, GUILayout.Width(60));
                if (EditorGUI.EndChangeCheck() )
                    table.SetMinRollDown(i, newVal);

                EditorGUI.BeginChangeCheck();
                GUI.SetNextControlName("txtMaxRoll" + i);
                newVal = EditorGUILayout.IntField(table.Entries[i].MaxRoll, GUILayout.Width(60));
                if (EditorGUI.EndChangeCheck() )
                    table.SetMaxRollDown(i, newVal);

                table.Entries[i].Target = (GameObject)EditorGUILayout.ObjectField(table.Entries[i].Target, typeof(GameObject), false);


                if (i != 0)
                {
                    if (GUILayout.Button("U", EditorStyles.miniButtonLeft, GUILayout.Width(24)))
                        table.MoveEntryUp(i);
                }
                else
                    GUILayout.Space(24);

                if (i != (table.Entries.Count - 1))
                {
                    if (GUILayout.Button("D", EditorStyles.miniButtonRight, GUILayout.Width(24)))
                    {
                        table.MoveEntryDown(i);
                    }
                }
                else
                    GUILayout.Space(24);


                GUILayout.EndHorizontal();
            }

            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(table);

            if (GUILayout.Button("Add Entry"))
            {
                table.AddEntry();
                EditorUtility.SetDirty(table);
            }
        }


    }

}
