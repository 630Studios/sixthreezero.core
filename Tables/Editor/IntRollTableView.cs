﻿using UnityEditor;
using UnityEngine;

namespace SixThreeZero.Core.Tables
{
    public class IntRollTableView: RollTableView<IntRollTableEntry>
    {
        public static  void Display(IntRollTable table)
        {
            IntRollTableView.Display(table, (e)=>ViewEntry(e));
        }

        public static void ViewEntry(IntRollTableEntry entry)
        {
            entry.Target = EditorGUILayout.IntField (entry.Target, GUILayout.Width(40)); 
        }
    } 
}
