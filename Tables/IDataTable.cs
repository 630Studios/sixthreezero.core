﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace SixThreeZero.Core.Tables
{
    public interface IDataTable
    {
        int Count { get; }
        string[] GetNames();
    }
            
}
