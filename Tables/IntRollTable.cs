﻿using UnityEngine;
using System.Collections.Generic;




    namespace SixThreeZero.Core.Tables
    {
        [System.Serializable]
        public class IntRollTableEntry : RollTableEntry<int> { }

        [System.Serializable]
        public class IntRollTable : RollTable<IntRollTableEntry> { }





    }
