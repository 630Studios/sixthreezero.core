﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
namespace SixThreeZero.Core.Tables
{ 
    [System.Serializable]
    public class DataTableEntry
    {
        [SerializeField, HideInInspector]
        protected int _Id = -1;
        public int Id { get { return _Id; } }
        public string Name;

        protected DataTableEntry()
        {

        }

        public DataTableEntry(int id)
        {
            this._Id = id;
        }


    }
}