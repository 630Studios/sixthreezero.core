﻿using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace SixThreeZero.Core.Tables
{



    [System.Serializable]
    public class DataTable<T> : ISerializationCallbackReceiver, IDataTable where T : DataTableEntry
    {

        [SerializeField, HideInInspector]
        protected List<T> Entries = new List<T>();

        protected Dictionary<int, T> _LookupTable = new Dictionary<int, T>();


        [SerializeField, HideInInspector]
        protected int _NextID = 0;


        public int Count { get { return Entries.Count; } }

        /// <summary>
        /// Gets and entry from its list by its index in the list. This is different from an entries ID.
        /// </summary>
        /// <param name="index">The entries position in the list.</param>
        /// <returns></returns>
        public T Item(int index)
        {
            return Entries[index];
        }

        /// <summary>
        /// Gets an entry from the table based on its ID. This is different then its index in the list.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetByID(int id)
        {
            if (_LookupTable.ContainsKey(id))
                return _LookupTable[id];
            else
                return null;
        }


        public void OnBeforeSerialize()
        {

        }

        public void OnAfterDeserialize()
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                _LookupTable[Entries[i].Id] = Entries[i];
            }
        }

        /// <summary>
        /// Creates a new entry in the table and returns the entry.
        /// </summary>
        /// <returns></returns>
        public T CreateEntry()
        {
            int currentID = _NextID;
            _NextID++;
            T newEntry = System.Activator.CreateInstance(typeof(T), new System.Object[] { currentID }) as T;

            Entries.Add(newEntry);
            _LookupTable[currentID] = newEntry;
            return newEntry;
        }

        public void DeleteEntryByIndex(int index)
        {
            T entry = Entries[index];
            Entries.RemoveAt(index);
            _LookupTable.Remove(entry.Id);

        }

        public void DeleteEntryByID(int id)
        {
            T entry = _LookupTable[id];
            Entries.Remove(entry);
            _LookupTable.Remove(entry.Id);

        }

        public void Clear()
        {
            Entries.Clear();
            _LookupTable.Clear();
            _NextID = 0;
        }

        public string[] GetNames()
        {
            return Entries.Select(p => p.Name).ToArray();
        }

        public int GetIDFromName(string name)
        {
            T entry = Entries.Where(p => p.Name == name).FirstOrDefault();
            if (entry != null)
                return entry.Id;
            else
                return -1;
        }
    }

}