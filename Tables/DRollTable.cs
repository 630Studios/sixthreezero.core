﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif



namespace SixThreeZero.Core
{
    [System.Serializable]
    public class DRollTable : ScriptableObject
    {
        [System.Serializable]
        public class DRollTableEntry
        {
            public int MinRoll = 0;
            public int MaxRoll = 10;
            public GameObject Target = null;

            public DRollTableEntry()
            {
                
            }
        }

        public List<DRollTableEntry> Entries = new List<DRollTableEntry>();

        public void SetMinRollDown(int index, int value)
        {
            if (index != 0)
            {
                if (Entries[index - 1].MaxRoll >= value)
                    SetMaxRollDown(index - 1, value - 1);
            }
            Entries[index].MinRoll = value;

            if (value > Entries[index].MaxRoll)
                SetMaxRollDown(index, value);
        }


        public void SetMaxRollDown(int index, int value)
        {
            if (Entries[index].MinRoll > value)
                SetMinRollDown(index, value);

            Entries[index].MaxRoll = value;

            if (index != Entries.Count-1)
            {
                if (value > Entries[index + 1].MinRoll)
                    SetMinRollDown(index + 1, value + 1);
            }

        }

        public DRollTableEntry GetEntry(int i)
        {
            return Entries[i];
        }

        public DRollTableEntry AddEntry()
        {
            DRollTableEntry newEntry = new DRollTableEntry();
            if (Entries.Count == 0)
            {
                newEntry.MinRoll = 0;
                newEntry.MaxRoll = 100;
            }
            else
            {
                newEntry.MinRoll = Entries[Entries.Count - 1].MaxRoll + 1;
                newEntry.MaxRoll = newEntry.MinRoll;
            }
            Entries.Add(newEntry);
            return newEntry;
        }

        public void MoveEntryUp(int index)
        {
            if (index == 0 || index >= Entries.Count)
                return;

            DRollTableEntry entry = Entries[index];
            DRollTableEntry oEntry = Entries[index - 1];
            int minRoll = entry.MinRoll;
            int maxRoll = entry.MaxRoll;
            Entries.RemoveAt(index);
            entry.MinRoll = oEntry.MinRoll;
            entry.MaxRoll = oEntry.MaxRoll;
            oEntry.MinRoll = minRoll;
            oEntry.MaxRoll = maxRoll;
            Entries.Insert(index - 1, entry);
            
        }

        public void MoveEntryDown(int index)
        {
            if (index < 0 || index > Entries.Count-2)
                return;

            DRollTableEntry entry = Entries[index];
            DRollTableEntry oEntry = Entries[index + 1];
            int minRoll = entry.MinRoll;
            int maxRoll = entry.MaxRoll;
            Entries.RemoveAt(index);
            entry.MinRoll = oEntry.MinRoll;
            entry.MaxRoll = oEntry.MaxRoll;
            oEntry.MinRoll = minRoll;
            oEntry.MaxRoll = maxRoll;
            Entries.Insert(index + 1, entry);

        }

        public DRollTableEntry GetEntryFromRoll(int roll)
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                if (roll >= Entries[i].MinRoll && roll <= Entries[i].MaxRoll)
                        return Entries[i];
            }

            return null;
        }

        public GameObject GenerateFromRoll(int roll)
        {
            for (int i = 0; i < Entries.Count; i++)
            {
                if (roll >= Entries[i].MinRoll && roll <= Entries[i].MaxRoll)
                    if (Entries[i].Target != null)
                        return GameObject.Instantiate(Entries[i].Target);
                    else
                        return null;
            }

            return null;
        }

        public GameObject Roll()
        {
            int maxRoll = Entries[Entries.Count - 1].MaxRoll;
            int roll = Random.Range(0, maxRoll);
            for (int i = 0; i < Entries.Count; i++)
            {
                if (roll >= Entries[i].MinRoll && roll <= Entries[i].MaxRoll)
                    if (Entries[i].Target != null)
                        return GameObject.Instantiate(Entries[i].Target);
                    else
                        return null;
            }

            return null;
        }

        public int GetMinValue()
        {
            return Entries.OrderBy(p => p.MinRoll).Select(p=>p.MinRoll).FirstOrDefault();
        }

        public int GetMaxValue()
        {
            return Entries.OrderBy(p => p.MaxRoll).Select(p => p.MaxRoll).FirstOrDefault();
        }

        public GameObject GenerateRandom()
        {
            int val = Random.Range(0, Entries.Count);

            if (Entries[val].Target != null)
                return GameObject.Instantiate(Entries[val].Target);
            else
                return null;
        }

    }









#if UNITY_EDITOR

    



#endif
}


