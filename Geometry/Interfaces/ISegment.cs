﻿using UnityEngine;


namespace SixThreeZero.Core.Geometry
{
    public interface ISegment<T> where T: struct
    {
        T PointA { get; set; }
        T PointB { get; set; }
        T[] GetPath(int segments);

        Vector2[] GetPath2D(int segments);
        Vector3[] GetPath3D(int segments);

        T GetMidPoint();

    }
}
