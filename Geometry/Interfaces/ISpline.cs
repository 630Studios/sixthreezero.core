﻿using UnityEngine;
using System.Collections.Generic;


namespace SixThreeZero.Core.Geometry
{
    public interface ISpline<K, T> where K: IBezier where T : struct
    { 
        List<K> Segments { get; set;}
        

        T[] GetPath(int segments);
        T[] GetPoints();
        T[] GetPointsUnique();
        T[] GetControls();
        T[] GetControlsAbsolute();

        Vector2[] GetPath2D(int segments);
        Vector3[] GetPath3D(int segments);
        Vector3[] GetPathEditor(int segments);


        
        void SetPathPoint(int index, T point);
        void SetControlPoints(T[] controls);
        void MovePointChained(int index, T amount);
        void AddPointToEnd(T point);




    }
}
