﻿using UnityEngine;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Core.Geometry
{
    public interface IParametricSegment
    {
        DynamicVector2 DynamicPointA { get; set; }
        DynamicVector2 DynamicPointB { get; set; }

    }
}
