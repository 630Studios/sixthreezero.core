﻿using UnityEngine;
using System.Collections.Generic;

using System;

namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class Segment2D: ISegment<Vector2>
    {
        [SerializeField]
        protected Vector2 _PointA;

        [SerializeField]
        protected Vector2 _PointB;

        public Vector2 PointA
        {
            get { return _PointA; }
            set { _PointA = value; }
        }

        public Vector2 PointB
        {
            get { return _PointB; }
            set { _PointB = value; }
        }




        public virtual Vector2 GetMidPoint()
        {
            return _PointA.midpoint(_PointB);
        }

        public virtual Vector2 GetPoint(float time)
        {
            return _PointA + ((_PointB - _PointA) * time);
        }

        public virtual Vector2[] GetPath(int segments)
        {
            return new Vector2[] { _PointA, _PointB };
        }

        public virtual Vector2[] GetPath2D(int segments)
        {
            return new Vector2[] { _PointA, _PointB };
        }

        public virtual Vector3[] GetPath3D(int segments)
        {
            return new Vector3[] { _PointA.ToVector3(), _PointB.ToVector3() };
        }
    }
}
