﻿using UnityEngine;
using System.Collections.Generic;

using System;

namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class Segment3D : ISegment<Vector3>
    {
        [SerializeField]
        protected Vector3 _PointA;

        [SerializeField]
        protected Vector3 _PointB;

        public Vector3 PointA
        {
            get { return _PointA; }
            set { _PointA = value; }
        }

        public Vector3 PointB
        {
            get { return _PointB; }
            set { _PointB = value; }
        }

        public virtual Vector3 GetMidPoint()
        {
            return _PointA.midpoint(_PointB);
        }

        public virtual Vector3[] GetPath(int segments)
        {
            return new Vector3[] { _PointA, _PointB };
        }

        public virtual Vector2[] GetPath2D(int segments)
        {
            return new Vector2[] { _PointA, _PointB };
        }

        public virtual Vector3[] GetPath3D(int segments)
        {
            return new Vector3[] { _PointA, _PointB };
        }
    }
}
