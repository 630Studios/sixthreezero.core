﻿using UnityEngine;
using System.Collections.Generic;

using System;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class ParametricSegment2D : ISegment<Vector2>
    {
        [SerializeField]
        protected DynamicVector2 _PointA;

        [SerializeField]
        protected DynamicVector2 _PointB;

        protected DynamicScope _Scope;
        public DynamicScope Scope
        {
            get { return _Scope; }
            set { SetScope(value); }
        }
        public Vector2 PointA
        {
            get { return _PointA.Value; }
            set { _PointA.Value = value; }
        }

        public Vector2 PointB
        {
            get { return _PointB.Value; }
            set { _PointB.Value = value; }
        }

        public DynamicVector2 DynamicPointA
        {
            get { return _PointA; }
            set { _PointA = value; }
        }

        public DynamicVector2 DynamicPointB
        {
            get { return _PointB; }
            set { _PointB = value; }
        }

        public ParametricSegment2D()
        {
            _PointA = new DynamicVector2(Vector2.zero, () => _Scope);
            _PointB = new DynamicVector2(Vector2.zero, () => _Scope);
        }

        protected void SetScope(DynamicScope scope)
        {
            _Scope = scope;
            _PointA.SetScopeFunc(() => _Scope);
            _PointB.SetScopeFunc(() => _Scope);
        }

        public virtual Vector2 GetMidPoint()
        {
            return _PointA.Value.midpoint(_PointB.Value);
        }

        public virtual Vector2 GetPoint(float time)
        {
            return _PointA.Value + ((_PointB.Value - _PointA.Value) * time);
        }

        public virtual Vector2[] GetPath(int segments)
        {
            return new Vector2[] { _PointA.Value, _PointB.Value };
        }

        public virtual Vector2[] GetPath2D(int segments)
        {
            return new Vector2[] { _PointA.Value, _PointB.Value };
        }

        public virtual Vector3[] GetPath3D(int segments)
        {
            return new Vector3[] { _PointA.Value.ToVector3(), _PointB.Value.ToVector3() };
        }
    }
}
