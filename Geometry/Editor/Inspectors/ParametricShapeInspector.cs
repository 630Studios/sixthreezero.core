﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using SixThreeZero.Core.Geometry;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Core.Geometry
{
    [CustomEditor(typeof(ParametricShape)),System.Serializable]
    public class ParametricShapeInspector: Editor
    {

        



        [MenuItem("Assets/Create/Parametric Shape")]
        public static void CreateParametricShape()
        {
            ScriptableObjectUtility.CreateAsset<ParametricShape>();
        }

        Material mat;
        private void OnEnable()
        {
            var shader = Shader.Find("Hidden/Internal-Colored");
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            mat = new Material(shader);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            ParametricShape shape = (ParametricShape)target;


            List<ParametricBezier2D> segments = shape.Segments;
            ShowProperties(shape);
            ShowSegments(segments);


            Rect rect = GUILayoutUtility.GetRect(10, 500, 200, 200);
            if (Event.current.type == EventType.Repaint)
            {
                Vector3[] path = shape.GetPoints().Select(p => new Vector3(p.x, -p.y,0)).ToArray();
                GUI.BeginClip(rect);
                GL.PushMatrix();
                GL.Clear(true, false, Color.black);
                mat.SetPass(0);

                GL.Begin(GL.LINES);
                GL.Color(Color.black);

                for (int i = 0; i < path.Length -1; i++)
                {
                    
                    GL.Vertex((path[i] * 20) + new Vector3(100,100,0));
                    GL.Vertex((path[i+1] * 20) + new Vector3(100, 100, 0));
                }
                
                GL.End();
                GL.PopMatrix();
                GUI.EndClip();
            }
            Handles.DrawAAPolyLine(shape.GetPoints().Select(p=> new Vector3(p.x, 0, p.y)).ToArray() );
        }

        public void OnDestroy()
        {


        }

        

        protected void OnDisable()
        {
            
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
        }

        public void OnSceneGUI(SceneView view)
        {

            ParametricShape shape = (ParametricShape)target;
            Vector2[] path = shape.GetPath(10);

            Vector3[] path3 = new Vector3[path.Length];

            for (int i = 0; i<path.Length;i++)
                path3[i] = new Vector3(path[i].x, 0, path[i].y) * 10;

            DrawPathInScene(path3, Color.green, Vector3.up);

        }

        private void DrawPathInScene(Vector3[] path, Color color, Vector3 upDirection)
        {

            Color prevColor = Handles.color;
            Handles.color = color;
            Handles.DrawAAPolyLine(path);
            Handles.color = prevColor;
        }

        public static void ShowSegments(List<ParametricBezier2D> segments)
        {
            GUILayout.Label("Segments", EditorStyles.toolbarButton);
            GUILayout.BeginVertical(EditorStyles.helpBox);
            for (int i = 0; i < segments.Count; i++)
            {
                GUILayout.BeginVertical(EditorStyles.helpBox);
                ParametricBezier2D bez = segments[i];
                DynamicVector2 pA = bez.DynamicPointA;
                System.Object pO = (System.Object)pA;
                EditorGUI.BeginChangeCheck();
                DynamicsEditor.DisplayDyanmicFieldEditor(pA.BaseType, ref pO);
                if (EditorGUI.EndChangeCheck())
                {
                    if (i > 0)
                    {
                        segments[i - 1].DynamicPointB.CopyFrom(segments[i].DynamicPointA);
                    }
                }

                DynamicVector2 cA = bez.DynamicControlA;
                System.Object cO = (System.Object)cA;
                EditorGUI.BeginChangeCheck();
                DynamicsEditor.DisplayDyanmicFieldEditor(cA.BaseType, ref cO);
                if (EditorGUI.EndChangeCheck())
                {
                    if (i > 0)
                    {
                        segments[i - 1].DynamicControlB.CopyFrom(segments[i].DynamicControlA);
                    }
                }
                GUILayout.EndVertical();

                if (i == segments.Count - 1)
                {
                    GUILayout.BeginVertical(EditorStyles.helpBox);
                    DynamicVector2 pB = bez.DynamicPointB;
                    System.Object pBO = (System.Object)pB;
                    DynamicsEditor.DisplayDyanmicFieldEditor(pA.BaseType, ref pBO);

                    DynamicVector2 cB = bez.DynamicControlB;
                    System.Object cBO = (System.Object)cB;
                    DynamicsEditor.DisplayDyanmicFieldEditor(cB.BaseType, ref cBO);
                    GUILayout.EndHorizontal();
                }
                
            }
            GUILayout.EndVertical();
        }



        protected static DynamicFieldListInspector _StaticInspector = new DynamicFieldListInspector();
        public static void ShowProperties(ParametricShape shape)
        {
            EditorGUIUtility.labelWidth = 80;
            EditorGUIUtility.fieldWidth = 50;
            _StaticInspector.Target = shape.Properties;

            GUILayout.Label("Properties", EditorStyles.toolbarButton);
            EditorGUI.BeginChangeCheck();
            _StaticInspector.OnInspectorGUI();
            if (EditorGUI.EndChangeCheck())
                EditorUtility.SetDirty(shape);
        }
    }
}
