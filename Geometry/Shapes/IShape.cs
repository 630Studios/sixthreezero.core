﻿
using UnityEngine;

namespace SixThreeZero.Core.Geometry
{
    public interface IShape
    {
        //List<Bezier2D> Segments { get; set; }
        int SegmentCount { get; }
        string name { get; }
        void AddPointToEnd(Vector2 point);
        Rect GetBounds(int segmentation);
        Vector2[] GetControls();
        Vector2[] GetControlsAbsolute();
        Vector2[] GetPath(int segments);
        Vector2[] GetPath2D(int segments);
        Vector3[] GetPath3D(int segments);
        Vector3[] GetPathEditor(int segments);
        Vector2[] GetPoints();
        Vector2[] GetPointsUnique();
        bool IsPathClockwise();
        bool IsPathClosed();
        float Length();
        void MovePointChained(int index, Vector2 amount);
        void RadiusPoint(int pointIndex);
        void RemovePoint(int index);
        void Reverse();
        void SetControlPoints(Vector2[] controls);
        void SetControlPointsAbsolute(Vector2[] controls);
        void SetPathPoint(int index, Vector2 point);
        void SubvidideSegment(int segmentIndex);
        void AddSegment(IBezier2D segment);
        ISpline2D GetSpline();
    }
}