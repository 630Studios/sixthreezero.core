﻿using UnityEngine;

using System.Collections.Generic;
using SixThreeZero.Core.Providers;
using System;

namespace SixThreeZero.Core.Geometry
{

    public class BasicShape : ScriptableObject, IShape, IShapeProvider
    {

        public Spline2D Path = new Spline2D();


        public List<Bezier2D> Segments
        {
            get { return Path.Segments; }
            set { Path.Segments = value; }
        }

        public int SegmentCount
        {
            get { return Path.Segments.Count; }
        }

        public Vector2[] GetPoints()
        {
            return Path.GetPoints();
        }

        public Vector2[] GetPointsUnique()
        {
            return Path.GetPointsUnique();
        }

        public Vector2[] GetControls()
        {
            return Path.GetControls();
        }

        public Vector2[] GetControlsAbsolute()
        {
            return Path.GetControlsAbsolute();
        }

        public Vector2[] GetPath(int segments)
        {
            return Path.GetPath(segments);
        }

        public Vector2[] GetPath2D(int segments)
        {
            return Path.GetPath2D(segments);
        }

        public Vector3[] GetPath3D(int segments)
        {
            return Path.GetPath3D(segments);
        }

        public Vector3[] GetPathEditor(int segments)
        {
            return Path.GetPathEditor(segments);
        }

        public void AddPointToEnd(Vector2 point)
        {
            Path.AddPointToEnd(point);
        }

        public void SetPathPoint(int index, Vector2 point)
        {
            Path.SetPathPoint(index, point);
        }

        public void SetControlPoints(Vector2[] controls)
        {
            Path.SetControlPoints(controls);
        }

        public void SetControlPointsAbsolute(Vector2[] controls)
        {
            Path.SetControlPointsAbsolute(controls);
        }

        public void MovePointChained(int index, Vector2 amount)
        {
            Path.MovePointChained(index, amount);
        }

        public void RemovePoint(int index)
        {
            Path.RemovePoint(index);
        }

        public void Reverse()
        {
            Path.Reverse();
        }

        public bool IsPathClosed()
        {
            return Path.IsPathClosed();
        }

        public bool IsPathClockwise()
        {
            return Path.IsPathClockwise();
        }

        public float Length()
        {
            float length = 0;
            for (int i = 0; i < Path.Segments.Count; i++)
            {
                length += Vector3.Distance(Path.Segments[i].PointA, Path.Segments[i].PointB);
            }
            length += Vector3.Distance(Path.Segments[0].PointA, Path.Segments[Path.Segments.Count-1].PointB);
            return length;
        }

        public void SubvidideSegment(int segmentIndex)
        {
            Path.SubdivideSegment(segmentIndex);
        }

        public void RadiusPoint(int pointIndex)
        {
            Path.RadiusPoint(pointIndex);
        }

        public Rect GetBounds(int segmentation)
        {

            return Path.GetBounds(segmentation);
        }

        public IShape GetShape()
        {
            return this;
        }

        public void AddSegment(IBezier2D segment)
        {
            if (segment.GetType() != typeof(Bezier2D))
                throw new System.Exception("basicShape: AddSegment: segment is not a Bezier2D.");

            Segments.Add((Bezier2D)segment);

        }

        public ISpline2D GetSpline()
        {
            return Path;
        }
    }
}
