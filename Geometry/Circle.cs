﻿using UnityEngine;

namespace SixThreeZero.Core.Geometry
{
    
        public static class Circle
        {
            public static Vector2 getOrigin(Vector2 point1, Vector2 point2, float radius)
            {
                float r2 = (radius * radius);
                float circleEQ = Mathf.Sqrt(((point2.x - point1.x) * (point2.x - point1.x)) + ((point2.y - point1.y) * (point2.y - point1.y)));
                Vector2 midpoint = point1.midpoint(point2);

                Vector2 origin1 = new Vector2();
                origin1.x = midpoint.x + Mathf.Sqrt(r2 - ((circleEQ / 2) * (circleEQ / 2))) * ((point1.y - point2.y) / circleEQ);
                origin1.y = midpoint.y + Mathf.Sqrt(r2 - ((circleEQ / 2) * (circleEQ / 2))) * ((point2.x - point1.x) / circleEQ);

                Vector2 origin2 = new Vector2();
                origin2.x = midpoint.x - Mathf.Sqrt(r2 - ((circleEQ / 2) * (circleEQ / 2))) * ((point1.y - point2.y) / circleEQ);
                origin2.y = midpoint.y - Mathf.Sqrt(r2 - ((circleEQ / 2) * (circleEQ / 2))) * ((point2.x - point1.x) / circleEQ);

                /* Debug.Log("-------------------------");
                 Debug.Log("CIRCLE CENTERS");
                 Debug.Log(origin1);
                 Debug.Log(origin2);
                 Debug.Log(radius);
                 Debug.Log("-------------------------");*/

                return origin1;

            }
            public static Vector2 GetLeftOrigin(Vector2 point1, Vector2 point2, float radius)
            {
                float centerX = LeftCenterX(point1.x, point1.y, point2.x, point2.y, radius);
                float centerY = LeftCenterY(point1.x, point1.y, point2.x, point2.y, radius);
                return new Vector2(centerX, centerY);
            }

            public static Vector2 GetRightOrigin(Vector2 point1, Vector2 point2, float radius)
            {
                float centerX = RightCenterX(point1.x, point1.y, point2.x, point2.y, radius);
                float centerY = RightCenterY(point1.x, point1.y, point2.x, point2.y, radius);
                return new Vector2(centerX, centerY);
            }

            public static float LeftCenterY(float x1, float y1, float x2, float y2, float radius)
            {
                float radsq = radius * radius;
                float q = Mathf.Sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));

                float y3 = (y1 + y2) / 2;

                return y3 + Mathf.Sqrt(radsq - ((q / 2) * (q / 2))) * ((x2 - x1) / q);


            }

            public static float LeftCenterX(float x1, float y1, float x2, float y2, float radius)
            {
                float radsq = radius * radius;
                float q = Mathf.Sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
                float x3 = (x1 + x2) / 2;


                return x3 + Mathf.Sqrt(radsq - ((q / 2) * (q / 2))) * ((y1 - y2) / q);


            }

            public static float RightCenterY(float x1, float y1, float x2, float y2, float radius)
            {
                float radsq = radius * radius;
                float q = Mathf.Sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));

                float y3 = (y1 + y2) / 2;

                return y3 - Mathf.Sqrt(radsq - ((q / 2) * (q / 2))) * ((x2 - x1) / q);


            }

            public static float RightCenterX(float x1, float y1, float x2, float y2, float radius)
            {
                float radsq = radius * radius;
                float q = Mathf.Sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
                float x3 = (x1 + x2) / 2;


                return x3 - Mathf.Sqrt(radsq - ((q / 2) * (q / 2))) * ((y1 - y2) / q);


            }


        }
    
}
