﻿using UnityEngine;


public class GeometryMath
{
    public enum ArcDirection
    {
        Clockwise = 0,
        CounterClockwise = 1,
    }




    /* 2D Math */

    /* circle */
    public static Vector2[] GetPointsOnCircleD(Vector2 center, int radius, float fromAngle, float toAngle, int segments, ArcDirection direction = ArcDirection.Clockwise)
    {
        float totalAngle;

        if (direction == ArcDirection.Clockwise)
            totalAngle = toAngle > fromAngle ? toAngle - fromAngle : (360 + toAngle) - fromAngle;
        else
            totalAngle = toAngle > fromAngle ? -((360 + toAngle) - fromAngle) : -(fromAngle - toAngle);

        Vector2[] points = new Vector2[segments + 1];
        float step = totalAngle / (float)(segments);

        for (int i = 0; i <= segments; i++)
        {
            float currentAngle = (fromAngle + (step * ((float)i)));
            points[i] = GetPointOnCircleD(center, radius, currentAngle);
        }

        return points;
    }
    public static Vector2 GetPointOnCircleD(Vector2 center, int radius, float angleInDegrees)
    {
        return new Vector2(center.x + radius * Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), center.y + radius * Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
    }

    public static Vector2 GetPointOnCircleD(Vector2 center, float radius, float angleInDegrees)
    {
        return new Vector2(center.x + radius * Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), center.y + radius * Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
    }


    /* oval */
    public static Vector2[] GetPointsOnOvalD(Vector2 center, Vector2 radius, float fromAngle, float toAngle, int segments, ArcDirection direction = ArcDirection.Clockwise)
    {
        float totalAngle;

        if (direction == ArcDirection.Clockwise)
            totalAngle = toAngle > fromAngle ? toAngle - fromAngle : (360 + toAngle) - fromAngle;
        else
            totalAngle = toAngle > fromAngle ? -((360 + toAngle) - fromAngle) : -(fromAngle - toAngle);

        Vector2[] points = new Vector2[segments + 1];
        float step = totalAngle / (float)(segments);

        for (int i = 0; i <= segments; i++)
        {
            float currentAngle = (fromAngle + (step * ((float)i)));
            points[i] = GetPointOnOvalD(center, radius, currentAngle);
        }

        return points;
    }
    public static Vector2 GetPointOnOvalD(Vector2 center, Vector2 radius, float angleInDegrees)
    {
        return new Vector2(center.x + radius.x * Mathf.Cos(angleInDegrees * Mathf.Deg2Rad), center.y + radius.y * Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
    }


    public static Vector2[] GetComomplex2DBezierPoints(Vector2[] points, int segmentsPerCurve)
    {
        int numberOfCurves = (points.Length / 2) - 1;
        int numberOfSegments = numberOfCurves * segmentsPerCurve;
        int numberOfPoints = numberOfSegments + 1;
        Vector2[] curvePoints = new Vector2[numberOfPoints];
        int nextPointIndex = 0;
        for (int i = 0; i < points.Length - 2; i += 2)
        {

            Vector2 inverseFirstControl = points[i] + ((points[i + 1] - points[i]) * -1);
            Vector2[] subPoints = GeometryMath.Get2DBezierPoints(points[i], inverseFirstControl, points[i + 3], points[i + 2], segmentsPerCurve);

            /* the first point of every bezier after the first one is the
             * same point as the last point in the last bezier, so we skip saving it to our list of points
             */
            if (i == 0)
            {
                for (int n = 0; n < subPoints.Length; n++)
                {
                    curvePoints[nextPointIndex] = subPoints[n];
                    nextPointIndex++;
                }
            }
            else
            {
                for (int n = 1; n < subPoints.Length; n++)
                {
                    curvePoints[nextPointIndex] = subPoints[n];
                    nextPointIndex++;
                }
            }

        }

        return curvePoints;
    }
    /* bezier */
    public static Vector2[] Get2DBezierPoints(Vector2 start, Vector2 control1, Vector2 control2, Vector2 end, int segments)
    {
        Vector2[] points = new Vector2[2 + segments];

        points[0] = PointOn2DBezier(start, control1, control2, end, 0);
        for (int i = 1; i <= segments; i++)
        {
            points[i] = PointOn2DBezier(start, control1, control2, end, ((float)i) / (float)(segments + 1));
        }
        points[1 + segments] = PointOn2DBezier(start, control1, control2, end, 1);

        return points;
    }
    public static Vector2 PointOn2DBezier(Vector2 start, Vector2 control1, Vector2 control2, Vector2 end, float time)
    {

        return Mathf.Pow((1 - time), 3) * start + 3 * Mathf.Pow((1 - time), 2) * time * control1 + 3 * (1 - time) * time * time * control2 + Mathf.Pow(time, 3) * end;
    }



    public static Rect GetBounds(Vector2[] points)
    {
        float leftMost = points[0].x, topMost = points[0].y, rightMost = points[0].x, bottomMost = points[0].y;

        for (int i = 0; i < points.Length; i++)
        {
            leftMost = points[i].x < leftMost ? points[i].x : leftMost;
            rightMost = points[i].x > rightMost ? points[i].x : rightMost;
            topMost = points[i].y > topMost ? points[i].y : topMost;
            bottomMost = points[i].y < bottomMost ? points[i].y : bottomMost;
        }

        return new Rect(leftMost, bottomMost, rightMost - leftMost, topMost - bottomMost);
    }

    /* 3D Math*/

    /* bezier */
    public static Vector3[] Get3DBezierPoints(Vector3 start, Vector3 control1, Vector3 control2, Vector3 end, int segments)
    {
        Vector3[] points = new Vector3[segments + 1];
        for (int i = 0; i < points.Length; i++)
        {
            points[i] = PointOn3DBezier(start, control1, control2, end, ((float)i) / (float)(segments));
        }

        return points;
    }
    public static Vector3 PointOn3DBezier(Vector3 start, Vector3 control1, Vector3 control2, Vector3 end, float time)
    {
        return Mathf.Pow((1 - time), 3) * start + 3 * Mathf.Pow((1 - time), 2) * time * control1 + 3 * (1 - time) * time * time * control2 + Mathf.Pow(time, 3) * end;
    }


    public static Vector2 RotatePointAroundPoint(Vector2 point, Vector2 around, float angleInDegrees)
    {
        Vector2 newPoint = Vector2.zero;
        newPoint.x = (point.x * Mathf.Cos(angleInDegrees * Mathf.Deg2Rad)) - (point.y * Mathf.Sin(angleInDegrees * Mathf.Deg2Rad));
        newPoint.y = (point.x * Mathf.Sin(angleInDegrees * Mathf.Deg2Rad)) - (point.y * Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        return newPoint;
    }
}

