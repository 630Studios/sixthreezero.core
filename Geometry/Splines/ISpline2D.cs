﻿using UnityEngine;

namespace SixThreeZero.Core.Geometry
{
    public interface ISpline2D
    {
        void AddPointToEnd(Vector2 point);
        void DivideSegment(int index, float time);
        Rect GetBounds(int segmentation);
        Vector2[] GetControls();
        Vector2[] GetControlsAbsolute();
        Vector2[] GetPath(int segments);
        Vector2[] GetPath2D(int segments);
        Vector3[] GetPath3D(int segments);
        Vector3[] GetPathEditor(int segments);
        Vector2[] GetPoints();
        Vector2[] GetPointsUnique();
        bool IsPathClockwise();
        bool IsPathClosed();
        void MovePointChained(int index, Vector2 amount);
        void RadiusPoint(int pointIndex);
        void RemovePoint(int index);
        void Reverse();
        void SetControlPoints(Vector2[] controls);
        void SetControlPointsAbsolute(Vector2[] controls);
        void SetPathPoint(int index, Vector2 point);
        void SubdivideSegment(int index);
        int SegmentCount { get; }
        IBezier2D GetSegment(int index);



    }
}