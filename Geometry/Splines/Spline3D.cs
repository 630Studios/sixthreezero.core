﻿using UnityEngine;
using System.Collections.Generic;




namespace SixThreeZero.Core.Geometry
{

    [System.Serializable]
    public class Spline3D: ISpline<Bezier3D, Vector3>
    {

        [SerializeField]
        private List<Bezier3D> _Segments = new List<Bezier3D>();


        public List<Bezier3D> Segments
        {
            get { return _Segments; }
            set { _Segments = value; }
        }


        public Vector3[] GetPoints()
        {
            List<Vector3> points = new List<Vector3>();
            points.Add(Segments[0].PointA);

            for (int i = 1; i < Segments.Count; i++)
            {
                points.Add(Segments[i].PointA);
                points.Add(Segments[i].PointB);
            }
            return points.ToArray();


        }

        public Vector3[] GetPointsUnique()
        {
            List<Vector3> points = new List<Vector3>();
            points.Add(Segments[0].PointA);
            points.Add(Segments[0].PointB);
            for (int i = 1; i < Segments.Count; i++)
            {
                
                points.Add(Segments[i].PointB);
            }
            return points.ToArray();

        }

        public void SetControlPoints(Vector3[] controls)
        {
            int index = 0;
            for (int i = 0; i < Segments.Count; i++)
            {
                Segments[i].ControlA = controls[index];
                Segments[i].ControlB = controls[index + 1];
                index += 2;
            }
        }

        public Vector3[] GetControls()
        {
            List<Vector3> points = new List<Vector3>();
            points.Add(Segments[0].ControlA);

            for (int i = 1; i < Segments.Count; i++)
            {
                points.Add(Segments[i].ControlA);
                points.Add(Segments[i].ControlB);
            }
            return points.ToArray();
        }

        public Vector3[] GetControlsAbsolute()
        {
            List<Vector3> points = new List<Vector3>();
            points.Add(Segments[0].PointA + Segments[0].ControlA);

            for (int i = 1; i < Segments.Count; i++)
            {
                points.Add(Segments[i].PointA + Segments[i].ControlA);
                points.Add(Segments[i].PointB + Segments[i].ControlB);
            }
            return points.ToArray();
        }

        public void AddLine(Vector3 start, Vector3 end)
        {
            Bezier3D bez = new Bezier3D();
            bez.PointA = start;
            bez.PointB = end;
            Segments.Add(bez);
        }

        public Vector3[] GetPath(int segments)
        {

            List<Vector3> points = new List<Vector3>();
            if (Segments.Count > 0)
            {
                points.Add(Segments[0].PointA);
                points.Add(Segments[0].PointB);

                for (int i = 1; i < Segments.Count; i++)
                {
                    points.Add(Segments[i].PointB);
                }
            }
            return points.ToArray();
        }


        public void SetPathPoint(int index, Vector3 point)
        {
            if (index == 0)
            {
                Segments[0].PointA = point;
            }
            else
            {
                if (index < Segments.Count)
                    Segments[index].PointA = point;

                Segments[index - 1].PointB = point;
            }


        }

        public void DeleteSegment(int index)
        {
            for (int i = Segments.Count - 1; i >= index; i--)
            {
                Segments.RemoveAt(i);
            }
        }

        public void MovePointChained(int index, Vector3 amount)
        {



            while (index < Segments.Count + 1)
            {
                if (index == 0)
                {
                    Segments[0].PointA += amount;
                }
                else
                {
                    if (index < Segments.Count)
                        Segments[index].PointA += amount;

                    Segments[index - 1].PointB += amount;
                }

                index++;
            }
        }

        public Vector2[] GetPath2D(int segments)
        {
            Vector3[] path = GetPath(segments);
            Vector2[] output = new Vector2[path.Length];
            for(int i = 0; i < path.Length;i++)
            {
                output[i] = path[i];
            }
            return output;
        }

        public Vector3[] GetPath3D(int segments)
        {
            return GetPath(segments);
        }

        public Vector3[] GetPathEditor(int segments)
        {
            return GetPath(segments);
        }

        public void AddPointToEnd(Vector3 point)
        {
            Bezier3D newBez = new Bezier3D();
            newBez.PointA = Segments[Segments.Count - 1].PointB;
            newBez.PointB = point;
            Segments.Add(newBez);
        }
    }
}
