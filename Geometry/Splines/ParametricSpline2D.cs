﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Core.Geometry
{

    [System.Serializable]
    public class ParametricSpline2D : ISpline<ParametricBezier2D, Vector2>, ISpline2D
    {

        [SerializeField]
        private List<ParametricBezier2D> _Segments = new List<ParametricBezier2D>();
        private DynamicScope _Scope;

        public DynamicScope Scope
        {
            get { return _Scope; }
            set { SetScope(value); }
        }


        public List<ParametricBezier2D> Segments
        {
            get { return _Segments; }
            set { _Segments = value; }
        }

        public int SegmentCount
        {
            get { return _Segments.Count; }

        }
        public Rect GetBounds(int segmentation)
        {

            Vector2[] path = GetPath(segmentation);
            return path.GetBounds();
        }

        public Vector2[] GetPoints()
        {
            List<Vector2> points = new List<Vector2>();

            for (int i = 0; i < Segments.Count; i++)
            {
                points.Add(Segments[i].PointA);
                points.Add(Segments[i].PointB);
            }
            return points.ToArray();

        }

        public Vector2[] GetPointsUnique()
        {
            if (Segments.Count == 0)
                return new Vector2[0];

            List<Vector2> points = new List<Vector2>();

            points.Add(Segments[0].PointA);
            points.Add(Segments[0].PointB);
            for (int i = 1; i < Segments.Count; i++)
            {

                points.Add(Segments[i].PointB);
            }
            return points.ToArray();

        }

        public Vector2[] GetControls()
        {
            List<Vector2> points = new List<Vector2>();

            for (int i = 0; i < Segments.Count; i++)
            {
                points.Add(Segments[i].ControlA);
                points.Add(Segments[i].ControlB);
            }
            return points.ToArray();
        }

        public Vector2[] GetControlsAbsolute()
        {
            List<Vector2> points = new List<Vector2>();


            for (int i = 0; i < Segments.Count; i++)
            {
                points.Add(Segments[i].PointA + Segments[i].ControlA);
                points.Add(Segments[i].PointB + Segments[i].ControlB);
            }
            return points.ToArray();
        }

        public Vector2[] GetPath(int segments)
        {
            List<Vector2> points = new List<Vector2>();
            if (Segments.Count > 0)
            {
                for (int i = 0; i < Segments.Count; i++)
                {
                    points.AddRange(Segments[i].GetPath(segments));
                }
            }
            return points.ToArray();
        }

        public Vector2[] GetPath2D(int segments)
        {
            return GetPath(segments);
        }


        public Vector3[] GetPath3D(int segments)
        {
            Vector2[] path = GetPath(segments);
            Vector3[] points = new Vector3[path.Length];
            for (int i = 0; i < path.Length; i++)
            {
                points[i] = new Vector3(path[i].x, 0, path[i].y);

            }
            return points;
        }

        public Vector3[] GetPathEditor(int segments)
        {

            List<Vector3> points = new List<Vector3>();
            if (Segments.Count > 0)
            {
                for (int i = 0; i < Segments.Count; i++)
                {
                    Vector2[] newPoints = Segments[i].GetPath(segments);
                    points.AddRange(newPoints.ToV3Array());
                }
            }
            return points.ToArray();
        }


        public void AddPointToEnd(Vector2 point)
        {
            ParametricBezier2D newBez = new ParametricBezier2D();
            newBez.Scope = _Scope;
            newBez.PointA = Segments[Segments.Count - 1].PointB;
            newBez.PointB = point;
            Segments.Add(newBez);
        }

        public void SetPathPoint(int index, Vector2 point)
        {
            if (index == 0)
            {
                Segments[0].PointA = point;
            }
            else
            {
                if (index < Segments.Count)
                    Segments[index].PointA = point;

                Segments[index - 1].PointB = point;
            }


        }

        public void SetControlPoints(Vector2[] controls)
        {
            int index = 0;
            for (int i = 0; i < Segments.Count; i++)
            {
                Segments[i].ControlA = controls[index];
                Segments[i].ControlB = controls[index + 1];
                index += 2;
            }
        }

        public void SetControlPointsAbsolute(Vector2[] controls)
        {
            int index = 0;
            for (int i = 0; i < Segments.Count; i++)
            {
                Segments[i].ControlA = controls[index] - Segments[i].PointA;
                Segments[i].ControlB = controls[index + 1] - Segments[i].PointB;
                index += 2;
            }
        }

        public void MovePointChained(int index, Vector2 amount)
        {
            while (index < Segments.Count + 1)
            {
                if (index == 0)
                {
                    Segments[0].PointA += amount;
                }
                else
                {
                    if (index < Segments.Count)
                        Segments[index].PointA += amount;

                    Segments[index - 1].PointB += amount;
                }

                index++;
            }
        }

        public void RemovePoint(int index)
        {

            if (index == 0)
            {
                Segments.RemoveAt(index);
                return;
            }
            else
            {
                if (index >= Segments.Count)
                {
                    Segments.RemoveAt(Segments.Count - 1);
                    return;
                }
                else
                {
                    ParametricBezier2D pbez = Segments[index - 1];
                    ParametricBezier2D cbez = Segments[index];
                    pbez.PointB = cbez.PointB;
                    pbez.ControlB = cbez.ControlB;
                    Segments.RemoveAt(index);
                }
            }
        }

        public void Reverse()
        {
            throw new NotImplementedException();
        }

        public bool IsPathClosed()
        {
            throw new NotImplementedException();
        }

        public bool IsPathClockwise()
        {
            throw new NotImplementedException();
        }

        public void SubdivideSegment(int index)
        {
            if (index < 0 || index >= _Segments.Count)
                throw new System.Exception("SubdivideSegment: index out of range");

            Vector2 midPoint = _Segments[index].GetMidPoint();
            ParametricBezier2D newBez = new ParametricBezier2D();
            newBez.PointA = midPoint;
            newBez.PointB = Segments[index].PointB;

            _Segments[index].PointB = midPoint;
            _Segments[index].ControlB = Vector2.zero;
            Segments.Insert(index + 1, newBez);
        }

        public void DivideSegment(int index, float time)
        {
            if (index < 0 || index >= _Segments.Count)
                throw new System.Exception("SubdivideSegment: index out of range");

            Vector2 midPoint = _Segments[index].GetPoint(time);
            ParametricBezier2D newBez = new ParametricBezier2D();
            newBez.Scope = _Scope;
            newBez.PointA = midPoint;
            newBez.PointB = Segments[index].PointB;

            _Segments[index].PointB = midPoint;
            _Segments[index].ControlB = Vector2.zero;
            Segments.Insert(index + 1, newBez);
        }

        public void RadiusPoint(int pointIndex)
        {
            int firstSegIndex = pointIndex - 1;
            int segIndex = pointIndex;

            Vector2 point = _Segments[segIndex].PointA;

            DivideSegment(firstSegIndex, .75f);
            DivideSegment(segIndex + 1, .25f);
            RemovePoint(segIndex + 1);

            _Segments[firstSegIndex + 1].ControlA = point - _Segments[firstSegIndex + 1].PointA;
            _Segments[firstSegIndex + 1].ControlB = point - _Segments[firstSegIndex + 1].PointB;

        }

        public void SetScope(DynamicScope scope)
        {
            _Scope = scope;
            for (int i = 0; i < _Segments.Count; i++)
            {
                _Segments[i].Scope = _Scope;
            }
        }

        

        public Vector2 GetPointValue(int index)
        {
            return _Segments[index].PointA;
        }

        public string GetPointVariable(int index)
        {
            return _Segments[index].DynamicPointA.VariableName;
        }

        public string GetPointExpression(int index)
        {
            return _Segments[index].DynamicPointA.Expression;
        }

        public PROPERTY_TYPE PointType(int index)
        {
            return _Segments[index].DynamicPointA.PropertyType;
        }
        public IBezier2D GetSegment(int index)
        {
            return _Segments[index];
        }
    }
}

