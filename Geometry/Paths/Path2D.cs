﻿using UnityEngine;
using System.Collections.Generic;


namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class Path2D: Path<Vector2>
    {


        public Path2D(IEnumerable<Vector2> points)
        {
            _Points.AddRange(points);
        }

        public bool IsPathClosed()
        {
            return _Points.IsPathClosed();
        }

        public bool IsPathClockwise()
        {
            return _Points.IsPathClockwise();
        }

        public void Reverse()
        {
            _Points.Reverse();
        }

    }
}
