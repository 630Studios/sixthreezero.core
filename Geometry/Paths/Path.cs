﻿using UnityEngine;

using System.Collections.Generic;


namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public abstract class Path<T>
    {
        [SerializeField]
        protected List<T> _Points = new List<T>();



        public List<T> Points
        {
            get { return _Points; }
            set { _Points = value; }
        }


        
    }
}
