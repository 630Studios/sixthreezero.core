﻿using UnityEngine;
using System.Collections.Generic;

using System;

namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class Bezier3D : Segment3D, ISegment<Vector3>, IBezier
    {

        [SerializeField]
        protected Vector3 _ControlA;

        [SerializeField] protected Vector3 _ControlB;


        public Vector3 ControlA
        {
            get { return _ControlA; }
            set { _ControlA = value; }
        }

        public Vector3 ControlB
        {
            get { return _ControlB; }
            set { _ControlB = value; }
        }

        public override Vector3 GetMidPoint()
        {
            return GeometryMath.PointOn3DBezier(PointA, PointA + ControlA, PointB + ControlB, PointB, .5f);
        }

        public override Vector3[] GetPath(int segments)
        {
            return GeometryMath.Get3DBezierPoints(PointA, PointA + ControlA, PointB + ControlB, PointB, segments);
        }

        public override Vector2[] GetPath2D(int segments)
        {
            throw new NotImplementedException();
        }

        public override Vector3[] GetPath3D(int segments)
        {
            return GeometryMath.Get3DBezierPoints(PointA, PointA + ControlA, PointB + ControlB, PointB, segments);
        }

        public virtual Vector3 Direction()
        {
            return (PointB - PointA);
        }
    }
}
