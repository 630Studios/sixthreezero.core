﻿using UnityEngine;

namespace SixThreeZero.Core.Geometry
{
    public interface IBezier2D
    {
        Vector2 PointA { get; set; }
        Vector2 PointB { get; set; }
        Vector2 ControlA { get; set; }
        Vector2 ControlB { get; set; }

        Vector2 GetMidPoint();
        Vector2[] GetPath(int segments);
        Vector2[] GetPath2D(int segments);
        Vector3[] GetPath3D(int segments);
        Vector2 GetPoint(float time);
    }
}