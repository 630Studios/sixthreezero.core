﻿using UnityEngine;
using System.Collections.Generic;



namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class Bezier2D : Segment2D, ISegment<Vector2>, IBezier, IBezier2D
    {
        [SerializeField]
        protected Vector2 _ControlA;
        [SerializeField]

        protected Vector2 _ControlB;


        public Vector2 ControlA
        {
            get { return _ControlA; }
            set { _ControlA = value; }
        }

        public Vector2 ControlB
        {
            get { return _ControlB; }
            set { _ControlB = value; }
        }

        public override Vector2 GetMidPoint()
        {
            return GeometryMath.PointOn2DBezier(PointA, PointA + ControlA, PointB + ControlB, PointB, .5f);
        }

        public override Vector2 GetPoint(float time)
        {
            return GeometryMath.PointOn2DBezier(PointA, PointA + ControlA, PointB + ControlB, PointB, time);
        }

        public override Vector2[] GetPath(int segments)
        {
            return GeometryMath.Get2DBezierPoints(PointA, PointA + ControlA, PointB + ControlB, PointB, segments);
        }

        public override Vector2[] GetPath2D(int segments)
        {
            return GeometryMath.Get2DBezierPoints(PointA, PointA + ControlA, PointB + ControlB, PointB, segments);
        }

        public override Vector3[] GetPath3D(int segments)
        {
            return GeometryMath.Get2DBezierPoints(PointA, PointA + ControlA, PointB + ControlB, PointB, segments).ToV3Array();
        }

        
    }
}
