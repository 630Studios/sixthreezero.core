﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class DrawingShapeData: ISerializationCallbackReceiver
    {

            
        public IShape Shape;
        public Color OutlineColor = Color.green;
        public int Segmentation = 16;

        

        public DrawingShapeData(IShape shape)
        {
            Shape = shape;
        } 

        public DrawingShapeData(IShape shape, Color outlineColor)
        {
            Shape = shape;
            OutlineColor = outlineColor;
        }


        protected string TypeStr;
        protected string ShapeStr;

        public void OnBeforeSerialize()
        {
            TypeStr = Shape.GetType().AssemblyQualifiedName;
            ShapeStr = JsonUtility.ToJson(Shape);
        }

        public void OnAfterDeserialize()
        {
            System.Type type = ReflectionHelper.FindTypeByAssemblyQualifiedName(TypeStr);
            Shape = JsonUtility.FromJson(ShapeStr, type) as IShape;
        } 
    }
    
}