﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero.Core.Geometry
{
    [System.Serializable]
    public class DrawingSchematic: ScriptableObject
    {


        public delegate void SchematicChangedEvent(DrawingSchematic schematic);

        [System.Serializable]
        public enum CLIP_METHOD
        {
            ADD = 0,
            REMOVE = 1,
        }


        [SerializeField] protected List<DrawingShapeData> _Shapes = new List<DrawingShapeData>();
        public event SchematicChangedEvent OnChanged;


        public DrawingShapeData[] GetAllShapeData()
        {
            return _Shapes.ToArray();
        }

        public int ShapeCount()
        {
            return _Shapes.Count;
        }

        public void AddShape(BasicShape shape)
        {
            _Shapes.Add(new DrawingShapeData(shape));

            if (OnChanged != null)
                OnChanged(this);
        }

        public void AddShapes(IEnumerable<BasicShape> shapes)
        {
            foreach(BasicShape shape in shapes)
            {
                _Shapes.Add(new DrawingShapeData(shape));
            }

            if (OnChanged != null)
                OnChanged(this);
        }

        public void RemoveShape(int index)
        {
            _Shapes.RemoveAt(index);

            if (OnChanged != null)
                OnChanged(this);
        }


        public void MoveShapeUp(int index)
        {
            if (index <= 0)
                return;

            DrawingShapeData sd = _Shapes[index];
            _Shapes.RemoveAt(index);
            _Shapes.Insert(index - 1, sd);

            if (OnChanged != null)
                OnChanged(this);
        }

        public void MoveShapeDown(int index)
        {
            if (index >= _Shapes.Count)
                return;

            if (OnChanged != null)
                OnChanged(this);
        }


    }
}
