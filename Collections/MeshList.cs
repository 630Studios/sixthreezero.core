﻿using UnityEngine;

namespace SixThreeZero.Core
{
    [System.Serializable]
    public class MeshList : ScriptableObject
    {
        public Mesh[] Meshes;
    }
}
