﻿using System.Collections.Generic;

namespace SixThreeZero.Collections
{
    public class PriorityQueItem<T>  
    {
        public int Priority;
        public T Value;
        

        public PriorityQueItem(int priority, T value)
        {
            Priority = priority;
            Value = value;
        }
    }

    class PriorityQueue<T, K> where T: PriorityQueItem<K>
    {

        public T[] Items;
        private int _Count = 0;
        private int _Size;

        public PriorityQueue(int size)
        {
            _Size = size;
            Items = new T[size];
        }


        public void Push(T item)
        {

            Items[_Count] = item;
            for (int i = _Count; i > 0; i--)
            {
                if (Items[i].Priority > Items[i - 1].Priority)
                {
                    Items[i] = Items[i - 1];
                    Items[i - 1] = item;
                }
                else
                {
                    break;
                }

            }
            _Count++;
            
        }

        public T Pop()
        {
            if (_Count > 0)
            {
                _Count--;
                return Items[_Count];
            }else
            {
                return null;
            }
        }

        public T Peek()
        {
            return Items[_Count - 1];
        }
        
        public void Clear()
        {
            System.Array.Clear(Items, 0, _Size);
        }

        public bool IsInQue(K value)
        {
            for (int i =0; i < _Count; i++)
            {
                if (Items[i].Value.Equals(value))
                    return true;
            }
            return false;
        }
    }
}