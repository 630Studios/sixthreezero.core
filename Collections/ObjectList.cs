﻿using UnityEngine;

namespace SixThreeZero.Core
{
    [System.Serializable]
    public class ObjectList: ScriptableObject
    {
        public GameObject[] Objects;
    }
}
