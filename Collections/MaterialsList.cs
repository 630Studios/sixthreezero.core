﻿using UnityEngine;

namespace SixThreeZero.Core
{


    public class MaterialList : ScriptableObject
    {
        public Material[] Materials;
    }
}
