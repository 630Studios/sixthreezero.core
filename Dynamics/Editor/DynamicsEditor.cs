﻿using UnityEditor;
using UnityEngine;

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero.Core.Dynamics
{
    public static class DynamicsEditor
    {

        [InitializeOnLoadMethod]
        public static void RegisterDynamicInspector()
        {
            CustomClassInspector.CustomHandlers.Add(new CustomClassInspector.CustomHandlerData(IsDynamicField, DisplayDyanmicFieldEditor));
        }


        public static bool IsDynamicField(System.Type type)
        {
            if (typeof(IDynamicField).IsAssignableFrom(type))
                return true;

            return false;
        }
        public static void DisplayDyanmicFieldEditor(System.Type fieldType, ref object value)
        {
            IDynamicField field = value as IDynamicField;

            GUILayout.BeginHorizontal();
            switch (field.PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    System.Object val = field.GetObjValue();
                    CustomClassInspector.ShowDefaultEditor(field.BaseType, ref val);
                    field.SetObjValue(val);
                    break;
                case PROPERTY_TYPE.Variable:
                    if (field.Scope != null)
                    {
                        int selectedVar = 0;
                        string varName = field.VariableName;
                        string[] varnames = field.Scope.variables.Where(v => field.BaseType.IsAssignableFrom(v.Value.Type)).Select(p => p.Key).ToArray();


                        if (varnames.Length > 0)
                        {
                            if (!string.IsNullOrEmpty(varName))
                            {
                                selectedVar = System.Array.IndexOf(varnames, varName);
                            }

                            if (selectedVar == -1)
                                selectedVar = 0;

                            selectedVar = EditorGUILayout.Popup(selectedVar, varnames);

                            if (selectedVar >= 0)
                            {
                                field.VariableName = varnames[selectedVar];
                                System.Object tval = field.Scope.variables[varnames[selectedVar]].Value;
                                CustomClassInspector.ShowDefaultEditor(field.BaseType, ref tval);
                                field.Scope.variables[varnames[selectedVar]].Value = tval;
                            }
                            else
                            {

                            }



                        }
                        else
                        {
                            GUILayout.Label("No Variables in scope.");
                        }

                    }
                    else
                    {
                        GUILayout.Label("No Scope!");
                    }
                    break;
                case PROPERTY_TYPE.Expression:
                    field.Expression = GUILayout.TextField(field.Expression, GUILayout.MinWidth(100));
                    break;
            }


            string[] tNames = field.AcceptableTypeNames();
            int selectedName = System.Array.IndexOf(tNames, field.PropertyType.ToString());

            if (tNames.Length > 1)
            {
                EditorGUI.BeginChangeCheck();
                selectedName = EditorGUILayout.Popup(selectedName, tNames, GUILayout.Width(60));
                if (EditorGUI.EndChangeCheck())
                {
                    field.PropertyType = (PROPERTY_TYPE)System.Enum.Parse(typeof(PROPERTY_TYPE), tNames[selectedName]);
                }
            }

            GUILayout.EndHorizontal();
        }

        public static void DisplayDyanmicFieldEditor(IDynamicField field)
        {


            GUILayout.BeginHorizontal();
            switch (field.PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    System.Object val = field.GetObjValue();
                    CustomClassInspector.ShowDefaultEditor(field.BaseType, ref val);
                    field.SetObjValue(val);
                    break;
                case PROPERTY_TYPE.Variable:
                    if (field.Scope != null)
                    {
                        string[] varnames = field.Scope.variables.Keys.ToArray();
                        EditorGUILayout.Popup(0, varnames);

                        System.Object tval = field.Scope.variables[varnames[0]].Value;
                        CustomClassInspector.ShowDefaultEditor(field.BaseType, ref tval);
                        field.Scope.variables[varnames[0]].Value = tval;

                    }
                    else
                    {
                        GUILayout.Label("No Scope!");
                    }
                    break;
                case PROPERTY_TYPE.Expression:
                    field.Expression = GUILayout.TextField(field.Expression, GUILayout.MinWidth(80));
                    break;
            }

            string[] tNames = field.AcceptableTypeNames();
            int selectedName = System.Array.IndexOf(tNames, field.PropertyType.ToString());

            if (tNames.Length > 1)
            {
                EditorGUI.BeginChangeCheck();
                selectedName = EditorGUILayout.Popup(selectedName, tNames, GUILayout.Width(60));
                if (EditorGUI.EndChangeCheck())
                {
                    field.PropertyType = (PROPERTY_TYPE)System.Enum.Parse(typeof(PROPERTY_TYPE), tNames[selectedName]);
                }
            }

            GUILayout.EndHorizontal();
        }
    }
}
