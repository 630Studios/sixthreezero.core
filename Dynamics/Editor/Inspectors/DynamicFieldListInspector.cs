﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SixThreeZero.Core.Dynamics
{
    
    [System.Serializable]
    public class DynamicFieldListInspector
    {

        protected DynamicFieldList _Target;

        protected string _TxtNewProprtyName = "";
        protected int _SelectedPropertyType;
        protected string[] _TypeNames;
        protected string _RemovePropertyName;

        public Vector2 ListScrollPos = Vector2.zero;
        public DynamicFieldList Target
        {
            get { return _Target;}
            set { _Target = value; }
        }

        public void SetTarget(ref DynamicFieldList list)
        {
            _Target = list;
        }

        public delegate string test();

        public DynamicFieldListInspector()
        {
            
            
            RefreshTypeNames();
            
        }

        public DynamicFieldListInspector(ref DynamicFieldList list)
        {
            _Target = list;
            RefreshTypeNames();
        }

        public void RefreshTypeNames()
        {
             _TypeNames = DynamicField.GetFieldTypeNames();
        }

        public void OnInspectorGUI()
        {

            if (_Target == null)
                return;

            int count = _Target.Count();
            string[] fieldNames = _Target.Names();
            GUILayout.BeginVertical(EditorStyles.helpBox);
            if (count > 0)
            {
                ListScrollPos = GUILayout.BeginScrollView(ListScrollPos, GUILayout.MinHeight(100));
                for (int i = 0; i < count; i++)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(fieldNames[i], GUILayout.Width(EditorGUIUtility.labelWidth));
                    DisplayDyanmicFieldEditor(_Target.GetField(fieldNames[i]));
                    if (GUILayout.Button("X", EditorStyles.miniButton, GUILayout.Width(24)))
                    {
                        _RemovePropertyName = fieldNames[i];
                    }
                    GUILayout.EndHorizontal();
                }
                GUILayout.EndScrollView();
            }else
            {
                GUILayout.Label("There are no parameters on this objects.");
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Label("Add Property", EditorStyles.centeredGreyMiniLabel);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Name: ", GUILayout.Width(80));
            _TxtNewProprtyName = EditorGUILayout.TextField(_TxtNewProprtyName, EditorStyles.textField);
            
            _SelectedPropertyType = EditorGUILayout.Popup(_SelectedPropertyType, _TypeNames);
            if (GUILayout.Button("Add", EditorStyles.miniButton, GUILayout.Width(50)))
            {
                System.Type dType = DynamicField.GetFieldTypeByName(_TypeNames[_SelectedPropertyType]);
                IDynamicField dField = System.Activator.CreateInstance(dType) as IDynamicField;
                dField.RestrictedPropertyTypes = PROPERTY_TYPE_FLAGS.Variable | PROPERTY_TYPE_FLAGS.Expression;
                _Target.Add(_TxtNewProprtyName, dField);
                GUI.changed = true;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();


            if (!string.IsNullOrEmpty(_RemovePropertyName))
            {
                _Target.Remove(_RemovePropertyName);
                _RemovePropertyName = "";
                GUI.changed = true;
            }

        }


        public static void DisplayDyanmicFieldEditor(IDynamicField field)
        {
            
            System.Type fieldType = field.BaseType;
            GUILayout.BeginHorizontal();
            switch (field.PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    System.Object val = field.GetObjValue();
                    CustomClassInspector.ShowDefaultEditor(field.BaseType, ref val);
                    field.SetObjValue(val);
                    break;
                case PROPERTY_TYPE.Variable:
                    if (field.Scope != null)
                    {
                        string[] varnames = field.Scope.variables.Keys.ToArray();
                        if (varnames.Length > 0)
                        {
                            EditorGUILayout.Popup(0, varnames);

                            System.Object tval = field.Scope.variables[varnames[0]].Value;
                            CustomClassInspector.ShowDefaultEditor(field.BaseType, ref tval);
                            field.Scope.variables[varnames[0]].Value = tval;
                        }
                        else
                        {
                            GUILayout.Label("No Variables in scope.");
                        }

                    }
                    else
                    {
                        GUILayout.Label("No Scope!");
                    }
                    break;
                case PROPERTY_TYPE.Expression:
                    field.Expression = GUILayout.TextField(field.Expression, GUILayout.MinWidth(100));
                    break;
            }


            string[] tNames = field.AcceptableTypeNames();
            int selectedName = System.Array.IndexOf(tNames, field.PropertyType.ToString());

            if (tNames.Length > 1)
            {
                EditorGUI.BeginChangeCheck();
                selectedName = EditorGUILayout.Popup(selectedName, tNames, GUILayout.Width(60));
                if (EditorGUI.EndChangeCheck())
                {
                    field.PropertyType = (PROPERTY_TYPE)System.Enum.Parse(typeof(PROPERTY_TYPE), tNames[selectedName]);
                }
            }

            GUILayout.EndHorizontal();
        }


        public void Destroy()
        {
            //_Target = null;
        }


    }




}
