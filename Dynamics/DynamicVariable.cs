﻿namespace SixThreeZero.Core.Dynamics
{

    public class DynamicVariable
    {
        public delegate System.Object GetValueDel();
        public delegate void SetValueDel(System.Object value);

        protected string _Name;
        protected System.Type _Type;
        protected GetValueDel _GetValueCallback;
        protected SetValueDel _SetValueCallback;

        public string Name
        {
            get
            {
                return _Name;
            }

            set
            {
                _Name = value;
            }
        }

        public System.Type Type
        {
            get
            {
                return _Type;
            }

        }

        public System.Object Value
        {
            get
            {
                return _GetValueCallback();
            }

            set
            {
                _SetValueCallback(value);
            }
        }

        public DynamicVariable(string name, System.Type type, GetValueDel getValueCallback, SetValueDel setValueCallback)
        {
            _Name = name;
            _Type = type;
            _GetValueCallback = getValueCallback;
            _SetValueCallback = setValueCallback;
        }
    }

}