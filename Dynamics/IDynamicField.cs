﻿

//using SixThreeZero.Scripting;
namespace SixThreeZero.Core.Dynamics
{
    public interface IDynamicField
    {
        PROPERTY_TYPE PropertyType { get; set; }
        PROPERTY_TYPE_FLAGS RestrictedPropertyTypes { get; set; }
        System.Type BaseType { get; }
        System.Object GetObjValue();
        void SetObjValue(System.Object value);
        DynamicScope Scope { get; set; }
        string VariableName { get; set; }
        string Expression { get; set; }
        string[] AcceptableTypeNames();
        void CopyFrom(IDynamicField target);
    }
}