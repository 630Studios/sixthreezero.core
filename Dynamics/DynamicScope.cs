﻿
using System.Collections.Generic;
namespace SixThreeZero.Core.Dynamics
{

    public class DynamicScope
    {
        protected List<DynamicScope> _childScopes;

        public Dictionary<string, DynamicVariable> variables = new Dictionary<string, DynamicVariable>();
        public Dictionary<string, System.Reflection.MethodInfo> functions = new Dictionary<string, System.Reflection.MethodInfo>();
        public Dictionary<string, System.Type> RegisteredTypes = new Dictionary<string, System.Type>();



        public bool HasChildScope(DynamicScope scope)
        {
            if (_childScopes.Contains(scope))
                return true;

            return false;
        }

        public bool AddChildScope(DynamicScope scope)
        {
            if (_childScopes.Contains(scope))
                return false;

            _childScopes.Add(scope);
            return true;
        }

        public void RemoveChildScope(DynamicScope scope)
        {
            if (_childScopes.Contains(scope))
                _childScopes.Remove(scope);

        }


        public void RegisterType(System.Type type)
        {
            if (RegisteredTypes.ContainsKey(type.Name))
            {
                //throw new System.Exception("Attempting to register type. Pre-exisiting type registered with same name(" + type.FullName + " - " + RegisteredTypes[type.Name].FullName);
            }
            else
            {
                RegisteredTypes[type.Name] = type;
            }
        }

        public void RegisterEntireAssembly(System.Reflection.Assembly assembly)
        {
            System.Type[] types = assembly.GetExportedTypes();

            for (int i = 0; i < types.Length; i++)
            {
                RegisterType(types[i]);
            }

        }

        public bool HasVariable(string name)
        {
            return variables.ContainsKey(name);
        }
    }
}

