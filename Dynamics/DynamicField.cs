﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using SixThreeZero.Scripting;


namespace SixThreeZero.Core.Dynamics
{
    [System.Serializable]
    public enum PROPERTY_TYPE
    {
        Value = 1,
        Variable = 2,
        Expression = 4,
    }

    [System.Serializable, System.Flags]
    public enum PROPERTY_TYPE_FLAGS
    {
        None = 0,
        Value = 1,
        Variable = 2,
        Expression = 4,
    }


    




    public delegate DynamicScope GetScopeDel();


    [System.Serializable]
    public static class DynamicField
    {

        public static System.Type[] GetFieldTypes()
        {
            return ReflectionHelper.FindClassesWithInterface(typeof(IDynamicField)).Where(p => !p.IsGenericType).ToArray();
        }

        public static string[] GetFieldTypeNames()
        {
            return GetFieldTypes().Select(p => p.Name).ToArray();
        }


        public static System.Type GetFieldTypeByName(string name)
        {
            return ReflectionHelper.FindClassesWithInterface(typeof(IDynamicField)).Where(p => !p.IsGenericType && p.Name == name).FirstOrDefault();
        }
    }

    [System.Serializable]
    public class DynamicField<T> : IDynamicField
    {


        private Tokenizer _Tokenizer;
        private LanguageController _Settings;

        [SerializeField]
        protected PROPERTY_TYPE _PropertyType = PROPERTY_TYPE.Value;
        [SerializeField]
        protected PROPERTY_TYPE_FLAGS _RestrictedPropertyTypes = PROPERTY_TYPE_FLAGS.None;
        [SerializeField]
        protected T _Value;

        [SerializeField]
        protected string _VariableName;
        [SerializeField]
        protected string _Expression;

        [System.NonSerialized]
        protected GetScopeDel _Scope;

        public PROPERTY_TYPE PropertyType
        {
            get { return _PropertyType; }
            set { SetPropertyType(value); }
        }

        public PROPERTY_TYPE_FLAGS RestrictedPropertyTypes
        {
            get { return _RestrictedPropertyTypes; }
            set { _RestrictedPropertyTypes = value; }
        }

        public System.Type BaseType
        {
            get { return typeof(T); }

        }

        public DynamicScope Scope
        {
            get
            {
                if (_Scope != null)
                    return _Scope();

                return null;
            }
            set
            {
                DynamicScope s = value;
                _Scope = () => s;
            }
        }

        public string Expression
        {
            get { return _Expression; }
            set { _Expression = value; }
        }


        public DynamicField()
        {
            _Settings = new LanguageController();
            _Settings.LoadDeafults();
            _Tokenizer = new Tokenizer(_Settings);
            SetPropertyType(PROPERTY_TYPE.Value);
            _Value = default(T);

        }

        public DynamicField(T value)
        {
            _Settings = new LanguageController();
            _Tokenizer = new Tokenizer(_Settings);
            _Settings.LoadDeafults();
            SetPropertyType(PROPERTY_TYPE.Value);
            _Value = value;
        }

        public DynamicField(T value, GetScopeDel getScopeFunc)
        {
            _Settings = new LanguageController();
            _Settings.LoadDeafults();
            _Tokenizer = new Tokenizer(_Settings);
            SetPropertyType(PROPERTY_TYPE.Value);
            _Value = value;
            _Scope = getScopeFunc;
        }

        public DynamicField(T value, PROPERTY_TYPE_FLAGS restrictedTypes, GetScopeDel getScopeFunc)
        {
            _Settings = new LanguageController();
            _Settings.LoadDeafults();
            _Tokenizer = new Tokenizer(_Settings);
            _RestrictedPropertyTypes = restrictedTypes;
            SetPropertyType(PROPERTY_TYPE.Value);
            _Value = value;
            _Scope = getScopeFunc;
        }

        public DynamicField(PROPERTY_TYPE propertyType, string value)
        {

            switch (propertyType)
            {
                case PROPERTY_TYPE.Expression:
                    _Expression = value;
                    break;
                case PROPERTY_TYPE.Variable:
                    _VariableName = value;
                    break;
                case PROPERTY_TYPE.Value:
                    break;
            }
            SetPropertyType(propertyType);
        }


        public T Value
        {
            get
            {
                return GetValue();
            }
            set
            {
                SetValue(value);
            }
        }

        public T GetValue()
        {

            switch (_PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    return _Value;

                case PROPERTY_TYPE.Variable:
                    if (_Scope() != null)
                    {
                        if (_Scope().HasVariable(_VariableName))
                            return (T)_Scope().variables[_VariableName].Value;
                        else
                            throw new System.Exception("Vairable : " + _VariableName + " was not found in the scope.");
                    }
                    else
                    {
                        throw new System.Exception("Vairable : No Scope");
                    }
                case PROPERTY_TYPE.Expression:
                    List<string> tokens = _Tokenizer.tokenizeString(_Expression);
                    
                    ITokenObject rootToken = TokenObject.CreateToken(_Settings, _Scope(), null, tokens);
                    return (T)rootToken.Execute();
            }

            return default(T);
        }

        public System.Object GetObjValue()
        {

            switch (_PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    return (System.Object)_Value;

                case PROPERTY_TYPE.Variable:
                    if (_Scope().HasVariable(_VariableName))
                        return _Scope().variables[_VariableName].Value;
                    else
                        throw new System.Exception("Vairable : " + _VariableName + " was not found in the scope.");
                case PROPERTY_TYPE.Expression:
                    List<string> tokens = _Tokenizer.tokenizeString(_Expression);
                    
                    ITokenObject rootToken = TokenObject.CreateToken(_Settings, _Scope(), null, tokens);
                    return (T)rootToken.Execute();
                    

            }

            return null;
        }

        public void SetValue(T value)
        {
            switch (_PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    _Value = value;
                    break;

                case PROPERTY_TYPE.Variable:
                    if (_Scope().HasVariable(_VariableName))
                        _Scope().variables[_VariableName].Value = value;
                    else
                        throw new System.Exception("Vairable : " + _VariableName + " was not found in the scope.");
                    break;
                case PROPERTY_TYPE.Expression:

                    break;

            }
        }

        public void SetObjValue(System.Object value)
        {
            switch (_PropertyType)
            {
                case PROPERTY_TYPE.Value:
                    _Value = (T)value;
                    break;

                case PROPERTY_TYPE.Variable:
                    if (_Scope().HasVariable(_VariableName))
                        _Scope().variables[_VariableName].Value = value;
                    else
                        throw new System.Exception("Vairable : " + _VariableName + " was not found in the scope.");
                    break;
                case PROPERTY_TYPE.Expression:

                    break;

            }
        }

        public string VariableName
        {
            get { return _VariableName; }
            set { _VariableName = value; }
        }


        public void SetScopeFunc(GetScopeDel getScopeFunc)
        {
            _Scope = getScopeFunc;
        }

        public void SetPropertyType(PROPERTY_TYPE propertyType)
        {
            if (_RestrictedPropertyTypes == PROPERTY_TYPE_FLAGS.None)
            {
                _PropertyType = propertyType;
            }
            else
            {
                if ((((int)propertyType) & ((int)_RestrictedPropertyTypes)) == 0)
                {
                    _PropertyType = propertyType;
                }
            }
        }

        protected List<string> typeNames = new List<string>();
        public string[] AcceptableTypeNames()
        {
            if (typeNames == null)
                typeNames = new List<string>();

            typeNames.Clear();

            foreach (PROPERTY_TYPE pType in System.Enum.GetValues(typeof(PROPERTY_TYPE)))
            {
                if ((((int)pType) & ((int)_RestrictedPropertyTypes)) == 0)
                {
                    typeNames.Add(pType.ToString());
                }
            }

            return typeNames.ToArray();
        }

        public void CopyFrom(IDynamicField target)
        {
            if (target.BaseType != this.BaseType)
                throw new System.Exception("Can not copy Dynamic Field data from different base types.");

            this._PropertyType = target.PropertyType;
            this._Value = (T)target.GetObjValue();
            this._VariableName = target.VariableName;
            this._Expression = target.Expression;

        }

    }
}
