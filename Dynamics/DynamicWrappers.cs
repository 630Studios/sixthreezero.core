﻿using UnityEngine;
using SixThreeZero.Core.Geometry;

namespace SixThreeZero.Core.Dynamics
{
    [System.Serializable]
    public class DynamicInt : DynamicField<int>, IDynamicField
    {
        public DynamicInt() : base() { }
        public DynamicInt(int value) : base(value) { }
        public DynamicInt(int value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicFloat : DynamicField<float>, IDynamicField
    {
        public DynamicFloat() : base() { }
        public DynamicFloat(float value) : base(value) { }
        public DynamicFloat(float value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicColor : DynamicField<Color>, IDynamicField
    {
        public DynamicColor() : base() { }
        public DynamicColor(Color value) : base(value) { }
        public DynamicColor(Color value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicColor32 : DynamicField<Color32>, IDynamicField
    {
        public DynamicColor32() : base() { }
        public DynamicColor32(Color32 value) : base(value) { }
        public DynamicColor32(Color32 value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicVector2 : DynamicField<Vector2>, IDynamicField
    {
        public DynamicVector2() : base() { }
        public DynamicVector2(Vector2 value) : base(value) { }
        public DynamicVector2(Vector2 value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicVector3 : DynamicField<Vector3>, IDynamicField
    {
        public DynamicVector3() : base() { }
        public DynamicVector3(Vector3 value) : base(value) { }
        public DynamicVector3(Vector3 value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicVector4 : DynamicField<Vector4>, IDynamicField
    {
        public DynamicVector4() : base() { }
        public DynamicVector4(Vector4 value) : base(value) { }
        public DynamicVector4(Vector4 value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }


    [System.Serializable]
    public class DynamicBool : DynamicField<bool>, IDynamicField
    {
        public DynamicBool() : base() { }
        public DynamicBool(bool value) : base(value) { }
        public DynamicBool(bool value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicString : DynamicField<string>, IDynamicField
    {
        public DynamicString() : base() { }
        public DynamicString(string value) : base(value) { }
        public DynamicString(string value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicGO : DynamicField<GameObject>, IDynamicField
    {
        public DynamicGO() : base() { }
        public DynamicGO(GameObject value) : base(value) { }
        public DynamicGO(GameObject value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicShape : DynamicField<BasicShape>, IDynamicField
    {
        public DynamicShape() : base() { }
        public DynamicShape(BasicShape value) : base(value) { }
        public DynamicShape(BasicShape value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }

    [System.Serializable]
    public class DynamicParametricShape : DynamicField<ParametricShape>, IDynamicField
    {
        public DynamicParametricShape() : base() { }
        public DynamicParametricShape(ParametricShape value) : base(value) { }
        public DynamicParametricShape(ParametricShape value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }


}
