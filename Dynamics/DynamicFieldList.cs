﻿
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace SixThreeZero.Core.Dynamics
{
    [System.Serializable]
    public class DynamicFieldList : ISerializationCallbackReceiver
    {
        [System.NonSerialized] private Dictionary<string, IDynamicField> _Fields = new Dictionary<string, IDynamicField>();

        [SerializeField] protected string[] _FieldNames;
        [SerializeField] protected string[] _TypeNames;
        [SerializeField] protected string[] _Data;

        public void OnBeforeSerialize()
        {
            _FieldNames = _Fields.Keys.ToArray();
            _TypeNames = _FieldNames.Select(p => _Fields[p].GetType().AssemblyQualifiedName).ToArray();
            _Data = _FieldNames.Select(p => JsonUtility.ToJson(_Fields[p])).ToArray();
            
        }


        public void OnAfterDeserialize()
        {
        }

        public void OnEnable()
        {
            _Fields.Clear();
            if (_Data != null)
            {
                for (int i = 0; i < _TypeNames.Length; i++)
                {
                    System.Type targetType = ReflectionHelper.FindTypeByAssemblyQualifiedName(_TypeNames[i]);

                    if (targetType == null)
                        throw new System.Exception("Invalid Dyanmic Field Type");

                    AddOrUpdate(_FieldNames[i], JsonUtility.FromJson(_Data[i], targetType) as IDynamicField);
                    
                }
            }
             
        }

        public void Add(string name, IDynamicField field)
        {
            if (field == null)
                throw new System.Exception("DynamicFieldList: field is null");

            if (_Fields.ContainsKey(name))
                throw new System.Exception("DynamicFieldList: " + name + " already exists in the field dictionary.");

            _Fields.Add(name, field);

        }

        public void AddOrUpdate(string name, IDynamicField field)
        {
            if (_Fields.ContainsKey(name))
            {
                IDynamicField target = _Fields[name];
                if (target != field)
                {
                    target.SetObjValue(field.GetObjValue());
                }
            }
            else
            {
                _Fields.Add(name, field);
            }
        }

        public IDynamicField GetField(string name)
        {
            return _Fields[name];
        }
        public void Remove(string name)
        {
            if (_Fields.ContainsKey(name))
                _Fields.Remove(name);
        }

        public void Clear()
        {
            _Fields.Clear();
        }

        public int Count()
        {
            return _Fields.Count;
        }

        public string[] Names()
        {
            return _Fields.Keys.ToArray();
        }


    }

}