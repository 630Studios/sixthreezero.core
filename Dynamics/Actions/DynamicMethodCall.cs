﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SixThreeZero.Core;

namespace SixThreeZero.Core.Dynamics
{
    public class DynamicMethodCall: DynamicAction
    {

        protected MethodReference _TargetMethod;

        public MethodReference TargetMethod
        {
            get { return _TargetMethod; }
        }



        public DynamicMethodCall(MethodReference reference)
        {
            
        }



        public void SetMethod(MethodReference reference)
        {
            _TargetMethod = reference;

            
        }
    }
}
