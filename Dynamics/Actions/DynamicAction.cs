﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero.Core.Dynamics
{
    [System.Serializable]
    public class DynamicAction
    {

        public string Name { get { return "DynamicAction"; } }

        public void Invoke()
        {

        }
    }
}
