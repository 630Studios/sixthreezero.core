
# SixThreeZero.Core
The SixThreeZero.Core library is intended to provide common core functionality for game and application development in Unity. The library provides new basic class types and utilities covering a wide range of things from a collection of serialized classes, basic geometry math and classes, spatial analysis, improved UI library, dynamic properties, mesh manipulation, on the fly C# scripting and much more. 

# Documentation
>"Here lies code, it does things"

I will attempt to work on some form of comprhensive documentation at some point, for now though there is random bits of useful information scattered through out the code.
 
[630Studios]

#License - MIT
Copyright 2018 Jason Penick

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#Important Note
This library collection makes use of the clipper and poly2tri libraries. They are subject to their own license agreements and have been included here simply to wrap them in a namespace so as not to create issues with other unity assets that make use of them.

[630Studios]: <https://30Studios.com>