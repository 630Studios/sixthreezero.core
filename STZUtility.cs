﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero
{
    public static class STZUtility
    {

        private static string _RootPath_Unity;
        private static string _RootPath_OS;
        private static string _LibraryPath_Unity;
        private static string _LibraryPath_unity;




        public static string RootPath_Unity
        {
            get
            {
                if (string.IsNullOrEmpty(_RootPath_Unity))
                    SetupRootPaths();

                return _RootPath_Unity;
            }
        }

        public static string RootPath_OS
        {
            get
            {
                if (string.IsNullOrEmpty(_RootPath_OS))
                    SetupRootPaths();

                return _RootPath_OS;
            }
        }

        public static string LibraryPath_Unity
        {
            get
            {
                return _RootPath_Unity + "/Shared Library";
            }
        }

        public static string LibraryPath_OS
        {
            get
            {
                return _RootPath_Unity + "/Shared Library";
            }
        }

        public static string ResourcesPath_Unity
        {
            get
            {
                return _RootPath_Unity + "/Resources";
            }
        }

        public static string ResourcesPath_OS
        {
            get
            {
                return _RootPath_Unity + "/Resources";
            }
        }

        public static string LibraryShadersPath_Unity
        {
            get
            {
                return _RootPath_Unity + "/Shared Library/Shaders";
            }
        }

        public static string LibraryShadersPath_OS
        {
            get
            {
                return _RootPath_Unity + "/Shared Library/Shaders";
            }
        }

        public static string OSPathToUnityPath(string path)
        {
            string appDataPath = Application.dataPath.Replace("\\", "/");
            string outpath = path.Replace("\\", "/");
            outpath = outpath.Replace(appDataPath, "");
            

            return outpath;
        }

        public static string UIImagePath_Unity
        {
            get
            {
                return LibraryPath_Unity + "/UI Images";
            }
        }

        public static string UIImagePath_OS
        {
            get
            {
                return LibraryPath_OS + "/UI Images";
            }
        }

        [InitializeOnLoadMethod]
        private static void SetupRootPaths()
        {
            string dpath = Application.dataPath;
            string[] directories = System.IO.Directory.GetDirectories(dpath, "630Studios", System.IO.SearchOption.AllDirectories);

            if (directories.Length != 1)
                throw new System.Exception("Unable to location 630Studios root directory.");

            _RootPath_OS = directories[0].Replace("\\", "/");
            _RootPath_Unity = ("Assets" + directories[0].Replace(dpath, "")).Replace("\\", "/");
            

        }


    }
}
