﻿using System;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
public class SceneControllerAttribute : Attribute
{
    public System.Type TargetType;

    public SceneControllerAttribute(System.Type targetType)
    {
        TargetType = targetType;
    }
}