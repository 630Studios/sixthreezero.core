﻿using UnityEngine;


namespace SixThreeZero.Core
{
    [System.Serializable]
    public class ByteMapArray
    {
        [SerializeField]private byte[] data;
        [SerializeField]private int _SizeX;
        [SerializeField]private int _SizeY;


        public int SizeX {get{return _SizeX;} }
        public int SizeY { get { return _SizeY; } }

        public byte this[int i]
        {
            get
            {
                return data[i];
            }
            set
            {
                data[i] = value;
            }
        }

        public byte this[int x, int y]
        {
            get
            {
                return data[(y * _SizeX) + x];
            }
            set
            {
                data[(y * _SizeX) + x] = value;
            }
        }


        public ByteMapArray(int sizeX, int sizeY)
        {
            this._SizeX = sizeX;
            this._SizeY = sizeY;
            int size = (sizeX * sizeY);
            this.data = new byte[size];
            for (int x = 0; x < size; x++)
                    this.data[x] = 0;

        }
    }


   
}
