﻿/*
 * Filename: MeshInspecter.cs
 * Created By: Jason Penick
 * Date: 7/14/2018
 * 
 * Description:
 * Allows for rotation, positional shifting, and alignment of the actual mesh and
 * is particularly useful for Assets from the asset store where you do
 * always get a .blend or fbx file that you can edit in another program.
 * 
 * Rotation allows for an optional pivot point for the rotation.
 * Su
 */

using UnityEngine;
using UnityEditor;
using System.Linq;

namespace SixThreeZero.Core.Inspectors
{
    [CustomEditor(typeof(Mesh))]
    public class MeshInspector : Editor
    {

        private Vector3 rotateAmount;
        private Vector3 pivotPos;
        private Vector3 shiftAmount;

        public override void OnInspectorGUI()
        {
            
            bool updateStuff = false;

            Mesh mesh = (Mesh)target;
            EditorGUIUtility.labelWidth = 80;
            EditorGUILayout.IntField("Sub Meshs:", mesh.subMeshCount);
            EditorGUILayout.BoundsField("Bounds:", mesh.bounds);
            GUILayout.Label("Vert Count: " + mesh.vertexCount);
            GUILayout.Label("Triangles Count: " + mesh.triangles.Length / 3);
            GUILayout.Label("UV Count: " + mesh.uv.Length);
            GUILayout.Label("Normal Count: " + mesh.normals.Length);
            GUI.enabled = true;
            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Label("Rotate Mesh", EditorStyles.toolbarButton);
            GUILayout.BeginHorizontal();
            pivotPos = EditorGUILayout.Vector3Field("Pivot: ", pivotPos);
            GUILayout.Space(60);
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            rotateAmount = EditorGUILayout.Vector3Field("Rotate: ", rotateAmount);
            if (GUILayout.Button("Rotate", EditorStyles.miniButton, GUILayout.Width(60)))
            {
                
                Quaternion newRotation = new Quaternion();
                newRotation.eulerAngles = rotateAmount;

                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = newRotation * verts[i];

                mesh.vertices = verts;
                updateStuff = true;
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Label("Shift Mesh", EditorStyles.toolbarButton);
            GUILayout.BeginHorizontal();
            shiftAmount = EditorGUILayout.Vector3Field("Shift: ", shiftAmount);
            if (GUILayout.Button("Shift", EditorStyles.miniButton, GUILayout.Width(60)))
            {

                
                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += shiftAmount;

                mesh.vertices = verts;
                updateStuff = true;

            }

            GUILayout.EndHorizontal();

            Vector3 center = mesh.bounds.center;

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Align Front Left", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(mesh.bounds.extents.x, 0, -mesh.bounds.extents.z);

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Front", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(0, 0, -mesh.bounds.extents.z);

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Front Right", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(-mesh.bounds.extents.x, 0, -mesh.bounds.extents.z);

                mesh.vertices = verts;
                updateStuff = true;

            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Align Left", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(mesh.bounds.extents.x, 0, 0);

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Center", EditorStyles.miniButton, GUILayout.Width(140)))
            {

                
                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -mesh.bounds.center;

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Right", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(-mesh.bounds.extents.x, 0, 0);

                mesh.vertices = verts;
                updateStuff = true;

            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Align Back Left", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(mesh.bounds.extents.x, 0, mesh.bounds.extents.z);

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Back", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(0, 0, mesh.bounds.extents.z);

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Back Right", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(-mesh.bounds.extents.x, 0, mesh.bounds.extents.z);

                mesh.vertices = verts;
                updateStuff = true;

            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Align Top", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(0, -mesh.bounds.extents.y, 0);

                mesh.vertices = verts;
                updateStuff = true;

            }
            if (GUILayout.Button("Align Bottom", EditorStyles.miniButton, GUILayout.Width(140)))
            {


                Vector3[] verts = mesh.vertices;

                for (int i = 0; i < verts.Length; i++)
                    verts[i] = verts[i] += -center + new Vector3(0, mesh.bounds.extents.y, 0);

                mesh.vertices = verts;
                updateStuff = true;

            }
        


            if (updateStuff)
            {
                shiftAmount = Vector3.zero;
                rotateAmount = Vector3.zero;
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();
                mesh.RecalculateTangents();
                EditorUtility.SetDirty(mesh);
                GUI.FocusControl("empty control");
                GUIUtility.hotControl = 0;
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        public override bool HasPreviewGUI()
        {
            return true;
        }

       /* private Material myMaterial;
        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {

            if (myMaterial == null)
                myMaterial = new Material(Shader.Find("Diffuse"));

            Mesh myMesh = (Mesh)target;
            PreviewRenderUtility pu = new PreviewRenderUtility();
            pu.BeginPreview(r, background);
            pu.camera.transform.position += Vector3.up;
            pu.camera.transform.LookAt(Vector3.zero);
            pu.DrawMesh(myMesh, Matrix4x4.identity, myMaterial, 0);
            pu.Render();
            pu.EndAndDrawPreview(r);
            
            //Texture texture = pu.EndPreview();
            //GUI.DrawTexture(r, texture, ScaleMode.StretchToFill, false);
        }

        public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
        {
            base.OnInteractivePreviewGUI(r, background);

        }*/
    }
}

