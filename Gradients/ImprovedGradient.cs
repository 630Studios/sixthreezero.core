﻿using UnityEngine;
using System.Collections.Generic;
using SixThreeZero.Core;
using System.Linq;
using System;

namespace SixThreeZero.Core
{

    [System.Serializable]
    public class ImprovedGradient
    {
        [System.Serializable]
        public struct ImprovedGradientColorKey
        {
            public Color color;
            public float time;

            
            public ImprovedGradientColorKey(Color col, float time)
            {
                this.color = col;
                this.time = time;
            }
        }

        [System.Serializable]
        public struct ImprovedGradientAlphaKey
        {
            public float alpha;
            public float time;
            
            public ImprovedGradientAlphaKey(float alpha, float time)
            {
                this.alpha = alpha;
                this.time = time;
            }
        }

        [SerializeField]public List<ImprovedGradientColorKey> Colors = new List<ImprovedGradientColorKey>();
        [SerializeField]public List<ImprovedGradientAlphaKey> Alphas = new List<ImprovedGradientAlphaKey>();

        [System.NonSerialized] private Texture2D _PreviewTexture;

        public Texture2D PreviewTexture
        {
            get {
                if (_PreviewTexture == null)
                {
                    _PreviewTexture = new Texture2D(256, 1, TextureFormat.ARGB32, false);
                    _PreviewTexture.hideFlags = HideFlags.HideAndDontSave;
                    UpdatePreviewTexture();
                }

                return _PreviewTexture;
            }
        }

        public ImprovedGradient()
        {
            Colors.Add(new ImprovedGradientColorKey(Color.white, 0));
            Colors.Add(new ImprovedGradientColorKey(Color.white, 1));
            Alphas.Add(new ImprovedGradientAlphaKey(1, 0));
            Alphas.Add(new ImprovedGradientAlphaKey(1, 1));
            
        }

        public ImprovedGradient(ImprovedGradient target)
        {

            for (int i = 0; i < target.Colors.Count;i++)
            {
                Colors.Add(new ImprovedGradientColorKey(target.Colors[i].color, target.Colors[i].time));
            }

            for (int i = 0; i < target.Alphas.Count; i++)
            {
                Alphas.Add(new ImprovedGradientAlphaKey(target.Alphas[i].alpha, target.Alphas[i].time));
            }
            UpdatePreviewTexture();
        }

        public Color Evaluate(float t)
        {
            Color returnColor = Color.white;
            Colors = Colors.OrderBy(p => p.time).ToList();

            for (int i = 0; i < (Colors.Count - 1); i++)
            {
                if (t >= Colors[i].time   && t < Colors[i+1].time)
                {
                    float nt = t - Colors[i].time;
                    float endT = Colors[i + 1].time - Colors[i].time;
                    float val = nt / endT;
                    returnColor = Color.Lerp(Colors[i].color, Colors[i + 1].color, val);
                    break;
                }
            }
            returnColor.a = 1;

            Alphas = Alphas.OrderBy(p => p.time).ToList();

            for (int i = 0; i < (Alphas.Count - 1); i++)
            {
                if (t >= Alphas[i].time && t < Alphas[i + 1].time)
                {
                    float nt = t - Alphas[i].time;
                    float endT = Alphas[i + 1].time - Alphas[i].time;
                    float val = nt / endT;
                    
                    returnColor.a =  Mathf.Lerp(Alphas[i].alpha, Alphas[i + 1].alpha, val);
                    break;
                }
            }
            return returnColor;
        }

        public void UpdatePreviewTexture()
        {
            if (_PreviewTexture == null)
            {
                _PreviewTexture = new Texture2D(256, 1, TextureFormat.ARGB32, false);
                _PreviewTexture.hideFlags = HideFlags.HideAndDontSave;
            }

            float width = _PreviewTexture.width;
            float t = 0;
            for (int i = 0; i < _PreviewTexture.width; i++)
            {
                t = i / width;
                _PreviewTexture.SetPixel(i, 0, Evaluate(t));
            }
            _PreviewTexture.Apply();
        }

        public Texture2D GetTexture(int width, int height)
        {
            Texture2D output = new Texture2D(width, 1, TextureFormat.ARGB32, false);

            
            float t = 0;
            for (int i = 0; i < width; i++)
            {
                t = i / width;
                Color c = Evaluate(t);
                for (int n = 0; n < height; n++)
                    output.SetPixel(i, n, c);
            }
            output.Apply();
            return output;
        }

        public Color[] GetColors(int width)
        {
            
            Color[] output = new Color[width];

            float t = 0;
            for (int i = 0; i < width; i++)
            {
                t = ((float)i) / ((float)width);
                output[i] = Evaluate(t);
            }
            
            return output;
        }


    }

}
