﻿using UnityEngine;
using UnityEditor;
using SixThreeZero.Core;

namespace SixThreeZero.Core
{
    public class ImprovedGradientEditorWindow: EditorWindow
    {
        [InitializeOnLoadMethod]
        public static void RegisterEditor()
        {
            CustomClassInspector.CustomHandlers.Add(new CustomClassInspector.CustomHandlerData(IsGradient, DisplayGradientEditorUI));
        }

        public static bool IsGradient(System.Type type)
        {
            if (typeof(ImprovedGradient).IsAssignableFrom(type))
                return true;

            return false;
        }

        public static void DisplayGradientEditorUI(System.Type fieldType, ref object value)
        {
            ImprovedGradient grad = (ImprovedGradient)value;
            Display(grad);
        }



        public static void Display(ImprovedGradient gradient)
        {
            GUILayout.BeginHorizontal();
            Rect r = GUILayoutUtility.GetRect(100, 14);
            if (Event.current.type == EventType.Repaint)
                GUI.DrawTexture(r, gradient.PreviewTexture);

            if (GUILayout.Button(new GUIContent("Edit"), EditorStyles.miniButton))
            {
                ShowEditor(gradient);
            }
            GUILayout.EndHorizontal();

        }
         


        public ImprovedGradient _TargetGradient = null;

        public static void ShowEditor(ImprovedGradient gradient)
        {
            ImprovedGradientEditorWindow window = EditorWindow.GetWindow<ImprovedGradientEditorWindow>("Gradient Editor", true);
            window._TargetGradient = gradient;
        }


        private Color newColor;
        private float newTime;
        private float newAlpha;
        private float newAlphaTime;
        private int removeColorID = -1;
        private int removeAlphaID = -1;
        private Vector2 ColorScrollPos = Vector2.zero;
        private Vector2 AlphaScrollPos = Vector2.zero;


        public void OnGUI()
        {
            EditorGUIUtility.labelWidth = 60;
            EditorGUIUtility.fieldWidth = 30;
            if (_TargetGradient == null)
                GUILayout.Label("Please select a gradient.", EditorStyles.centeredGreyMiniLabel);

            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Label("Preview", EditorStyles.centeredGreyMiniLabel);
            Rect r = GUILayoutUtility.GetRect(100, 14);
            if (Event.current.type == EventType.Repaint)
                GUI.DrawTexture(r, _TargetGradient.PreviewTexture, ScaleMode.StretchToFill);

            if (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag)
            {
                if (r.Contains(Event.current.mousePosition))
                {
                    float val = Mathf.Clamp(Event.current.mousePosition.x / (r.xMin + r.width), 0,1);
                    val = snapValue(val, .01f);
                    newTime = val;
                    newAlphaTime = val;
                    Repaint();
                    Event.current.Use();
                }
            }
            GUILayout.EndVertical(); 

            
            GUILayout.BeginHorizontal();
            
            GUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.ExpandWidth(true));
            GUILayout.Button("Color Keys", EditorStyles.toolbarButton);
            ColorScrollPos = GUILayout.BeginScrollView(ColorScrollPos);
            EditorGUI.BeginChangeCheck();
            for (int i = 0; i < _TargetGradient.Colors.Count; i++)
            {
                ImprovedGradient.ImprovedGradientColorKey key = _TargetGradient.Colors[i];
                GUILayout.BeginHorizontal(EditorStyles.helpBox);
                key.color = EditorGUILayout.ColorField(new GUIContent("Color:"), key.color, true, true, true, new ColorPickerHDRConfig(0,10,0,10));
                key.time = Mathf.Clamp(EditorGUILayout.DelayedFloatField(new GUIContent("Time: "), key.time),0,1);
                if(GUILayout.Button("X", EditorStyles.miniButton))
                {
                    removeColorID = i;
                }
                GUILayout.EndHorizontal();
                _TargetGradient.Colors[i] = key;
            }
            if (EditorGUI.EndChangeCheck())
            {
                _TargetGradient.UpdatePreviewTexture();
            }
            GUILayout.EndScrollView();

            GUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.ExpandWidth(true));
            GUILayout.Button("Alpha Keys", EditorStyles.toolbarButton);
            AlphaScrollPos =GUILayout.BeginScrollView(AlphaScrollPos);
            EditorGUI.BeginChangeCheck();
            for (int i = 0; i < _TargetGradient.Alphas.Count; i++)
            {
                ImprovedGradient.ImprovedGradientAlphaKey key = _TargetGradient.Alphas[i];
                GUILayout.BeginHorizontal();
                key.alpha = Mathf.Clamp(EditorGUILayout.DelayedFloatField(new GUIContent("Alpha:"), key.alpha),0,1);
                key.time = Mathf.Clamp(EditorGUILayout.DelayedFloatField(new GUIContent("Time: "), key.time),0,1);
                if (GUILayout.Button("X", EditorStyles.miniButton))
                {
                    removeAlphaID = i;
                }
                GUILayout.EndHorizontal();
                _TargetGradient.Alphas[i] = key;
            }
            if (EditorGUI.EndChangeCheck())
            {
                _TargetGradient.UpdatePreviewTexture();
            }
            GUILayout.EndScrollView();

            GUILayout.EndVertical();

            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Button("Add Color", EditorStyles.toolbarButton);
            newColor = EditorGUILayout.ColorField(new GUIContent("New Color: "), newColor, true, true, true, new ColorPickerHDRConfig(0, 10, 0, 10));
            newTime = EditorGUILayout.FloatField("Time: ", newTime);
            
            if (GUILayout.Button("Add"))
            {
                _TargetGradient.Colors.Add(new ImprovedGradient.ImprovedGradientColorKey(newColor, newTime));
                _TargetGradient.UpdatePreviewTexture();
            }
            GUILayout.EndVertical();
            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Button("Add Alpha", EditorStyles.toolbarButton);
            newAlpha = EditorGUILayout.FloatField("New Alpha: ", newAlpha);
            newAlphaTime = EditorGUILayout.FloatField("Time: ", newAlphaTime);
            
            if (GUILayout.Button("Add"))
            {
                _TargetGradient.Alphas.Add(new ImprovedGradient.ImprovedGradientAlphaKey(newAlpha, newAlphaTime));
                _TargetGradient.UpdatePreviewTexture();
            }
            GUILayout.EndVertical();
            GUILayout.EndVertical();

            GUILayout.EndHorizontal();




            if (removeAlphaID != -1)
            {
                _TargetGradient.Alphas.RemoveAt(removeAlphaID);
                removeAlphaID = -1;
                _TargetGradient.UpdatePreviewTexture();
                Repaint();
            }

            if (removeColorID != -1)
            {
                _TargetGradient.Colors.RemoveAt(removeColorID);
                _TargetGradient.UpdatePreviewTexture();
                removeColorID = -1;
                Repaint();
            }




        }
        private static float snapValue(float value, float snapAmount)
        {
            float result;

            if (snapAmount > 0)
                result = Mathf.Round(value / snapAmount) * snapAmount;
            else
                result = value;

            return result;

        }

        public void OnLostFocus()
        {
            //Close();
        }
    }
}
