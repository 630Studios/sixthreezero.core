﻿using System;
using UnityEngine;

namespace SixThreeZero.Core
{
    [System.Serializable]
    public struct IntVector2
    {

        [SerializeField]
        public int X;
        [SerializeField]
        public int Y;

        private static readonly IntVector2 VectorZero = new IntVector2(0, 0);
        private static readonly IntVector2 VectorLeft = new IntVector2(-1, 0);
        private static readonly IntVector2 VectorRight = new IntVector2(1, 0);
        private static readonly IntVector2 VectorUp = new IntVector2(0, 1);
        private static readonly IntVector2 VectorDown = new IntVector2(0, -1);
        private static readonly IntVector2 VectorOne = new IntVector2(1, 1);
        private static readonly IntVector2 VectorNegativeOne = new IntVector2(-1, -1);

        public IntVector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static IntVector2 Zero()
        {
            return VectorZero;
        }

        public static IntVector2 Left { get { return VectorLeft; } }
        public static IntVector2 Right { get { return VectorRight; } }
        public static IntVector2 Up { get { return VectorUp; } }
        public static IntVector2 Down { get { return VectorDown; } }
        public static IntVector2 One { get { return VectorOne; } }
        public static IntVector2 NegativeOne { get { return VectorNegativeOne; } }


        public static IntVector2 operator +(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.X + b.X, a.Y + b.Y);
        }

        public static IntVector2 operator -(IntVector2 a, IntVector2 b)
        {
            return new IntVector2(a.X - b.X, a.Y - b.Y);
        }

        public static IntVector2 operator -(IntVector2 a)
        {
            return new IntVector2(-a.X, -a.Y);
        }

        public static IntVector2 operator *(IntVector2 a, int d)
        {
            return new IntVector2(a.X * d, a.Y * d);
        }

        public static IntVector2 operator *(int d, IntVector2 a)
        {
            return new IntVector2(a.X * d, a.Y * d);
        }

        public static IntVector2 operator *(IntVector2 a, float d)
        {
            return new IntVector2((int)(a.X * d), (int)(a.Y * d));
        }

        public static IntVector2 operator *(float d, IntVector2 a)
        {
            return new IntVector2((int)(a.X * d), (int)(a.Y * d));
        }

        public static IntVector2 operator /(IntVector2 a, float d)
        {
            return new IntVector2((int)(a.X / d), (int)(a.Y / d));
        }

        public static IntVector2 operator /(IntVector2 a, int d)
        {
            return new IntVector2((int)(a.X / d), (int)(a.Y / d));
        }

        public static bool operator ==(IntVector2 a, IntVector2 b)
        {
            return (a.X == b.X && a.Y == b.Y);
        }

        public static bool operator !=(IntVector2 a, IntVector2 b)
        {
            return !(a.X == b.X && a.Y == b.Y);
        }

        public static implicit operator IntVector2(Vector3 v)
        {
            return new IntVector2((int)v.x, (int)v.y);
        }

        public static implicit operator IntVector2(Vector2 v)
        {
            return new IntVector2((int)v.x, (int)v.y);
        }

        public static implicit operator Vector3(IntVector2 v)
        {
            return new Vector3(v.X, v.Y, 0f);
        }

        public static implicit operator Vector2(IntVector2 v)
        {
            return new Vector2(v.X, v.Y);
        }

        public override bool Equals(object other)
        {
            bool result;
            if (!(other is IntVector2))
            {
                result = false;
            }
            else
            {
                IntVector2 vector = (IntVector2)other;
                result = (this.X.Equals(vector.X) && this.Y.Equals(vector.Y));
            }
            return result;
        }

        public override int GetHashCode()
        {
            return this.X.GetHashCode() ^ this.Y.GetHashCode() << 2;
        }

        public override string ToString()
        {
            return "(" + X.ToString() + "," + Y.ToString() + ")";
        }


        public int Distance(IntVector2 target)
        {
            int distance1 = Mathf.Abs(target.X - X);
            int distance2 = Mathf.Abs(target.Y - Y);

            return (int)(Mathf.Sqrt( (distance1 * distance1) + (distance2 * distance2)));
        }

        
    }
}

