﻿using UnityEngine;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SixThreeZero.Core
{
    [System.Serializable]
    public class MethodReference : MemberReference, ISerializationCallbackReceiver
    {


        [SerializeField, HideInInspector]
        protected SystemType[] _ParameterTypes;
        protected MethodInfo _TargetMethod;


        public SystemType[] ParameterTypes
        {
            get { return _ParameterTypes; }
        }

        public MethodInfo TargetMethod
        {
            get { return _TargetMethod; }
        }



        public MethodReference(ClassInstanceType instanceType, System.Type type, string name, System.Type[] parameterTypes) : base(instanceType, type, name)
        {


            this._ReferenceType = ClassMemberTypes.Method;
            this._TargetType = new SystemType(type);
            this._ParameterTypes = new SystemType[parameterTypes.Length];
            for (int i = 0; i < parameterTypes.Length; i++)
                this._ParameterTypes[i] = new SystemType(parameterTypes[i]);


            InitializeMethod();

        }

        private void InitializeMethod()
        {
            MethodInfo method;
            System.Type[] parameterTypes = _ParameterTypes.Select(p => p.Type).ToArray();

            if (_InstanceType == ClassInstanceType.Static)
                method = _TargetType.Type.GetPublicStaticMethod(_Name, parameterTypes);
            else
                method = _TargetType.Type.GetPublicInstaceMethod(_Name, parameterTypes);


            if (method == null)
                throw new System.Exception("MethodReference: Unable to find method with matching signature");

            this._TargetMethod = method;
        }

        public void Invoke()
        {
            if (_TargetMethod == null)
                throw new System.Exception("Invoke() TargetMethod is null");

            if (_InstanceType == ClassInstanceType.Instance)
                throw new System.Exception("Invoke() called on Instance Method");

            _TargetMethod.Invoke(null, null);
        }

        public void Invoke(System.Object[] values)
        {
            if (_TargetMethod == null)
                throw new System.Exception("Invoke() TargetMethod is null");

            if (_InstanceType == ClassInstanceType.Instance)
                throw new System.Exception("Invoke() called on Instance Method");

            _TargetMethod.Invoke(null, values);
        }

        public void Invoke(System.Object referenceObject, System.Object[] values)
        {
            if (_TargetMethod == null)
                throw new System.Exception("Invoke() TargetMethod is null");

            _TargetMethod.Invoke(referenceObject, values);
        }

        public void OnAfterDeserialize()
        {
            InitializeMethod();
        }

        public void OnBeforeSerialize()
        {
            
        }

        public T Value<T>()
        {
            if (_TargetMethod == null)
                throw new System.Exception("Value() TargetMethod is null");

            if (_InstanceType == ClassInstanceType.Instance)
                throw new System.Exception("Value() called on Instance Method");

            return (T)_TargetMethod.Invoke(null, null);
        }

        public T Value<T>(System.Object[] values)
        {
            if (_TargetMethod == null)
                throw new System.Exception("Value(values[]) TargetMethod is null");

            if (_InstanceType == ClassInstanceType.Instance)
                throw new System.Exception("Value(values[]) called on Instance Method");

            

            return (T)_TargetMethod.Invoke(null, values);
        }

        public T Value<T>(System.Object referenceObject, System.Object[] values)
        {

            if (_TargetMethod == null)
                throw new System.Exception("Value(ref, values[]) TargetMethod is null");


            return (T)_TargetMethod.Invoke(referenceObject, values);
        }
    }
}