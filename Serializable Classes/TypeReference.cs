﻿using UnityEngine;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

/*[System.Serializable]
public class DyanmicProperty
{
    [System.Serializable]
    public enum ValueType
    {
        Object = 0,
        UnityObject = 1,
        Int = 2,
        Float = 3,
        String = 4,


    }
    
    private string _Name;
    private SystemType _TypeReference;
    private ValueType _ValueType;

    [SerializeField, HideInInspector] private UnityEngine.Object _UnityObject;
    [SerializeField, HideInInspector] private int _IntValue;
    [SerializeField, HideInInspector] private float _FloatValue;
    [SerializeField, HideInInspector] private string _StringValue;
    [SerializeField, HideInInspector] private byte _ByteValue;
    [SerializeField, HideInInspector] private byte _Vector2Value;
    [SerializeField, HideInInspector] private byte _Vector3Value;
    [SerializeField, HideInInspector] private byte _RectValue;
}*/


[System.Serializable]
public class SystemType : ISerializationCallbackReceiver
{
    private System.Type _Type = null;
    [SerializeField, HideInInspector] private bool _IsNull = true;
    [SerializeField, HideInInspector] private string _AssemblyName;
    [SerializeField, HideInInspector] private string _AssemblyQualifiedName;
    [SerializeField, HideInInspector] private string _Namespace;
    [SerializeField, HideInInspector] private string _Fullname;
    [SerializeField, HideInInspector] private string _Name;

    public System.Type Type { get { return _Type; } set { SetType(value); } }
    public bool IsNull { get { return _IsNull; } }
    public string AssemblyQualifiedName { get { return _AssemblyQualifiedName; } }
    public string AssemblyName { get { return _AssemblyName; } }
    public string Name { get { return _Name; } }
    public string Fullname { get { return _Fullname; } }
    public string Namespace { get { return _Namespace; } }
    
    

    public SystemType()
    {

    }

    public SystemType(System.Type type)
    {
        SetType(type);
    }

    private void SetToNull()
    {
        _Type = null;
        _IsNull = true;
        _AssemblyName = null;
        _AssemblyQualifiedName = null;
        _Namespace = null;
        _Fullname = null;
        _Name = null;
    }

    private void SetType(System.Type type)
    {
        if (type == null)
        {
            SetToNull();
            return;
        }
        _IsNull = false;
        _AssemblyName = type.Assembly.FullName;
        _AssemblyQualifiedName = type.AssemblyQualifiedName;
        _Namespace = type.Namespace;
        _Fullname = type.FullName;
        _Name = type.Name;
    }

    public void OnBeforeSerialize()
    {

    }

    public void OnAfterDeserialize()
    {
        if (_IsNull)
            return;

        if (!string.IsNullOrEmpty(_AssemblyName))
        {
            Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();

            Assembly target = assemblies.FirstOrDefault(p => p.FullName == _AssemblyName);

            if (target != null)
            {
                System.Type targetType = target.GetType(_Fullname);
                if (targetType == null)
                {
                    throw new System.Exception("TypeReference: After Deserialize: Assembly Found: Type fullname not found.");
                }else
                {
                    SetType(targetType);
                    return;
                }
            }else
            {
                throw new System.Exception("TypeReference: After Deserialize: Type has Assemly name, but could not be found in appDomain assemblies.");
            }
        }

    }

    private static Assembly[] CachedAssemblyArray;
    private static string[] CachedAssemblyNames;
    
    public static Assembly[] GetAssemblies(bool refresh = false)
    {
        if (CachedAssemblyArray == null || refresh == true)
            CachedAssemblyArray = System.AppDomain.CurrentDomain.GetAssemblies();

        return CachedAssemblyArray;
    }


    public static string[] GetAssemblyNames(bool refresh = false)
    {
        if (CachedAssemblyNames == null || refresh == true)
            CachedAssemblyNames = System.AppDomain.CurrentDomain.GetAssemblies().Select(p=>p.FullName.Split(',')[0]).ToArray();

        return CachedAssemblyNames;
    }

    public static System.Type[] GetAssemblyTypes(Assembly assembly, string nameFilter = "")
    {
        
            return assembly.GetTypes().Where(t => t.IsPublic && t.Name.StartsWith(nameFilter)).ToArray();

    }
}



#if UNITY_EDITOR
public class TypeReferenceView
{
    private static System.Type[] types;
    private static string[] names = new string[] { };


    private static Assembly SelectedAssembly;
    private static int SelectedAssemblyID = 0;
    private static int SelectedTypeID = 0;


    private static string NameFilter = "";
    private static bool showEditor = false;
    

    public static void OnGUI(SystemType typeRef)
    {

        GUILayout.BeginHorizontal();
        GUILayout.Label("Type:", GUILayout.Width(100));
        if (typeRef.IsNull)
        {
            EditorGUILayout.LabelField(typeRef.IsNull.ToString(), EditorStyles.textField);
        }else
        {
            EditorGUILayout.LabelField(typeRef.Name, EditorStyles.textField);
        }

        if (GUILayout.Button(showEditor ? "Close" : "Edit", EditorStyles.miniButton, GUILayout.Width(50)))
            showEditor = !showEditor;

        GUILayout.EndHorizontal();

        if (!showEditor)
            return;
        
        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Label("Type Selector", EditorStyles.toolbarButton);
        /*Assembly controls*/
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Assembly:", GUILayout.Width(100));
        EditorGUI.BeginChangeCheck();
        SelectedAssemblyID = EditorGUILayout.Popup(SelectedAssemblyID, SystemType.GetAssemblyNames());
        if (EditorGUI.EndChangeCheck() || SelectedAssembly == null)
        {
            SelectedAssembly = SystemType.GetAssemblies()[SelectedAssemblyID];
            types = SystemType.GetAssemblyTypes(SelectedAssembly, NameFilter);
            names = types.Select(p => p.FullName).ToArray();
        }
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Name Filter:", GUILayout.Width(100));

        EditorGUI.BeginChangeCheck();
        NameFilter = EditorGUILayout.TextField(NameFilter);
        if (EditorGUI.EndChangeCheck())
        {
            types = SystemType.GetAssemblyTypes(SelectedAssembly, NameFilter);
            names = types.Select(p => p.FullName).ToArray();
        }

        GUILayout.EndHorizontal();

        

        GUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Results:", GUILayout.Width(100));
        SelectedTypeID = EditorGUILayout.Popup( SelectedTypeID, names);
        if (GUILayout.Button("Set", EditorStyles.miniButton, GUILayout.Width(50)))
        {
            typeRef.Type = types[SelectedTypeID];
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        


        
    }


}

#endif