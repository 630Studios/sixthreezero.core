﻿using UnityEngine;
using System.Collections.Generic;


namespace SixThreeZero.Core
{
    [System.Serializable]
    public enum ClassMemberTypes
    {
        Field = 0,
        Property = 1,
        Method = 2,
    }

    [System.Serializable]
    public enum ClassInstanceType
    {
        Static = 0,
        Instance = 1,
    }

    [System.Serializable]
    public class MemberReference
    {
        

        [SerializeField, HideInInspector] protected ClassMemberTypes _ReferenceType;
        [SerializeField, HideInInspector] protected ClassInstanceType _InstanceType;
        [SerializeField, HideInInspector] protected SystemType _TargetType;
        [SerializeField, HideInInspector] protected string _Name;

        public string Name { get { return _Name; } }
        public ClassMemberTypes ReferenceType { get { return _ReferenceType; } }
        public ClassInstanceType InstanceType { get { return _InstanceType; } }
        public SystemType TargetType { get { return _TargetType; } }

        public MemberReference(ClassInstanceType instanceType, System.Type type, string name)
        {
            _InstanceType = instanceType;
            _TargetType = new SystemType(type);
            _Name = name;
        }
    }
}
