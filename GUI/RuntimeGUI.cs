﻿using UnityEngine;



public static class RuntimeGUI
{
    public delegate bool CharValidationFunc(char value);
    public delegate bool TextValidationFunc(string value);



    private static string _curveEditorOwner;
    private static bool _showCurveField = false;
    private static Rect _CurveEditorRect = new Rect(0, 0, 300, 300);

    public static void HandleCommandKeys()
    {
        
    }


    public static bool CurveField(string name, Rect rect, AnimationCurve value)
    {
        int controlID = GUIUtility.GetControlID(name.GetHashCode(), FocusType.Passive, rect);

        /*if (GUIUtility.hotControl == 0)
        {
            _curveEditorOwner = "";
            _showCurveField = false;
        }*/
        GUI.Box(rect, "", GUI.skin.box);
        Rect animRect = rect;
        animRect.y = Screen.height - rect.y;

        Drawing.DrawAnimationCurve(animRect, value);

        if (Event.current.isMouse)
        {
            if (rect.Contains(Event.current.mousePosition))
            {
                if (Event.current.type == EventType.MouseUp)
                {
                    _showCurveField = !_showCurveField;
                    _curveEditorOwner = name;
                    GUIUtility.hotControl = controlID;
                    Debug.Log("Clicked");
                }
            }else
            {
                Debug.Log(Event.current.mousePosition);
            }
        }

        
        if (_curveEditorOwner == name)
        {
            if (_showCurveField == true)
                _CurveEditorRect = GUI.Window(name.GetHashCode(), _CurveEditorRect, (id)=>DoAnimationCurveWindow(id, value), "Curve Editor");
                
            return _showCurveField;
        }

        return false;
    }

    // newScreenPos = scren.height - CurrentScreePos 
    //currentCreenPos = newScreenPos - screenHeight;
    private static void DoAnimationCurveWindow(int windowID, AnimationCurve curve)
    {
        
        Rect animRect = _CurveEditorRect;
        animRect.height = 100;
        animRect.y =  Screen.height - _CurveEditorRect.y + 30;
        animRect.x += 10;
        animRect.width -= 20;
        
        

        for (int i = 0; i < curve.keys.Length; i++)
        {

        }
            
        


        //int controlID = GUIUtility.GetControlID(windowID, FocusType.Passive, _CurveEditorRect);
        
        GUILayout.BeginVertical(GUI.skin.box, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
        GUILayout.Label("Hello World");
        GUILayout.EndVertical();
        Drawing.DrawAnimationCurve(animRect, curve);
        GUI.DragWindow();
    }









    


    /*
     * ValidateField - GUI Equivelant Requires a Rect.
     */
    public static string ValidationField(string name, Rect rect, string value, CharValidationFunc charValidationFunction, TextValidationFunc validationFunction, string defaultValue)
    {
        return ValidationField(name, rect, value, GUI.skin.textField, charValidationFunction, validationFunction, defaultValue);
    }
    public static string ValidationField(string name, Rect rect, string value, GUIStyle style, CharValidationFunc charValidationFunction, TextValidationFunc validationFunction, string defaultValue)
    {

        bool hadFocus = false;

        if (validationFunction != null)
            if (validationFunction.Invoke(value) == false)
                value = defaultValue;


        if (Event.current.keyCode == KeyCode.Escape)
        {
            Debug.Log("Here");
            GUI.FocusControl(null);
            GUIUtility.hotControl = -1;
            return value;
        }

        //GUIContent content = new GUIContent(value);
        hadFocus = (GUI.GetNameOfFocusedControl() == name);

        if (Event.current.type == EventType.KeyDown && hadFocus)
        {



            char character = Event.current.character;
            if (char.IsLetterOrDigit(character) || char.IsSymbol(character) || char.IsPunctuation(character) || char.IsWhiteSpace(character))
            {
                if (charValidationFunction != null)
                {
                    if (charValidationFunction.Invoke(Event.current.character) == false)
                    {
                        Event.current.Use();
                    }
                }
            }



        }


        
        GUI.SetNextControlName(name);
        string newValue = GUI.TextField(rect, value, style);

        if (newValue != value)
        { 
            if (hadFocus && GUI.GetNameOfFocusedControl() != name)
            {
                if (validationFunction == null)
                    return newValue;

                if (validationFunction.Invoke(newValue) == false)
                    return defaultValue;
            }
            return newValue;
        }
        else
        {
            return value;
        }

        
    }

   /*
    * ValidateField - GUILayout Equivelant.
    */
    public static string ValidationField(string name,string value, CharValidationFunc charValidationFunction, TextValidationFunc validationFunction, string defaultValue)
    {
        return ValidationField(name, value, GUI.skin.textField, charValidationFunction, validationFunction, defaultValue);
    }

    public static string ValidationField(string name, string value, GUIStyle style, CharValidationFunc charValidationFunction, TextValidationFunc validationFunction, string defaultValue)
    {
        
        bool hadFocus = false;

        if (validationFunction != null)
            if (validationFunction.Invoke(value) == false)
                value = defaultValue;


        if (Event.current.keyCode == KeyCode.Escape)
        {
            Debug.Log("Here");
            GUI.FocusControl(null);
            GUIUtility.hotControl = -1;
            return value;
        }

        //GUIContent content = new GUIContent(value);
        hadFocus = (GUI.GetNameOfFocusedControl() == name);

        if (Event.current.type == EventType.KeyDown && hadFocus)
        {


            char character = Event.current.character;
            if (char.IsLetterOrDigit(character) || char.IsSymbol(character) || char.IsPunctuation(character) || char.IsWhiteSpace(character))
            {
                if (charValidationFunction != null)
                {
                    if (charValidationFunction.Invoke(Event.current.character) == false)
                    {
                        Event.current.Use();
                    }
                }
            }
            

            
        }


        GUI.SetNextControlName(name);
        string newValue = GUILayout.TextField( value, style);

        if (newValue != value)
        {
            if (hadFocus && GUI.GetNameOfFocusedControl() != name)
            {
                if (validationFunction == null)
                    return newValue;

                if (validationFunction.Invoke(newValue) == false)
                    return defaultValue;
            }
            return newValue;
        }
        else
        {
            return value;
        }


    }


    /* Int Slider */
    public static int HoroizontalIntSlider(string name, Rect rect, int minValue, int value, int maxValue, GUIStyle style)
    {
        return (int)GUI.HorizontalSlider(rect, value, minValue, maxValue);
    }

    public static int VerticalIntSlider(string name, Rect rect, int minValue, int value, int maxValue, GUIStyle style)
    {
        return (int)GUI.VerticalSlider(rect, value, minValue, maxValue);
    }

    /*Vector3 Fields*/
    public static Vector4 Vector4Field(string name, Rect rect, Vector4 value)
    {
        return Vector4Field(name, rect, value, GUI.skin.textField);
    }

    public static Vector4 Vector4Field(string name, Rect rect, Vector4 value, GUIStyle style)
    {
        Rect firstFieldRect = new Rect(rect.x, rect.y, rect.width / 3, rect.height);
        Rect secondFieldRect = new Rect(rect.width / 4, rect.y, rect.width / 3, rect.height);
        Rect thirdFieldRect = new Rect((rect.width / 4) * 2, rect.y, rect.width / 3, rect.height);
        Rect fourthFieldRect = new Rect((rect.width / 4) * 3, rect.y, rect.width / 3, rect.height);

        float value1 = FloatField(name + "_x", firstFieldRect, value.x, style);
        float value2 = FloatField(name + "_y", secondFieldRect, value.y, style);
        float value3 = FloatField(name + "_z", thirdFieldRect, value.z, style);
        float value4 = FloatField(name + "_w", fourthFieldRect, value.w, style);
        return new Vector4(value1, value2, value3, value4);
    }


    /*Vector3 Fields*/
    public static Vector3 Vector3Field(string name, Rect rect, Vector3 value)
    {
        return Vector3Field(name, rect, value, GUI.skin.textField);
    }

    public static Vector3 Vector3Field(string name, Rect rect, Vector3 value, GUIStyle style)
    {
        Rect firstFieldRect = new Rect(rect.x, rect.y, rect.width / 3, rect.height);
        Rect secondFieldRect = new Rect(rect.width / 3, rect.y, rect.width / 3, rect.height);
        Rect thirdFieldRect = new Rect((rect.width / 3) * 2, rect.y, rect.width / 3, rect.height);

        float value1 = FloatField(name + "_x", firstFieldRect, value.x, style);
        float value2 = FloatField(name + "_y", secondFieldRect, value.y, style);
        float value3 = FloatField(name + "_z", thirdFieldRect, value.z, style);
        return new Vector3(value1, value2, value3);
    }


    /* Vector2 Fields */
    public static Vector2 Vector2Field(string name, Rect rect, Vector2 value)
    {
        return Vector2Field(name, rect, value, GUI.skin.textField);
    }

    public static Vector2 Vector2Field(string name, Rect rect, Vector2 value, GUIStyle style)
    {
        Rect firstFieldRect = new Rect(rect.x, rect.y, rect.width/2, rect.height);
        Rect secondFieldRect = new Rect(rect.width / 2, rect.y, rect.width / 2, rect.height);

        float value1 = FloatField(name + "_x", firstFieldRect, value.x, style);
        float value2 = FloatField(name + "_y", secondFieldRect, value.y, style);
        return new Vector2(value1, value2);
    }

    /* Float Fields */
    public static float FloatField(string name, Rect rect, float value)
    {
        return FloatField(name, rect, value, GUI.skin.textField);
    }

    public static float FloatField(string name, Rect rect, float value, GUIStyle style)
    {
        string floatValue = value.ToString();
        if (floatValue.IndexOf('.') == -1)
            floatValue += ".0";

        string returnString = ValidationField(name, rect, floatValue, style, isValidFloatChar, IsValidFloat, "0");
        float returnValue;

        if (float.TryParse(returnString, out returnValue))
            return returnValue;

        return 0;
        
    }

    public static float FloatField(string name, float value)
    {
        return FloatField(name, value, GUI.skin.textField);
    }

    public static float FloatField(string name, float value, GUIStyle style)
    {
        string floatValue = value.ToString();
        if (floatValue.IndexOf('.') == -1)
            floatValue += ".0";

        string returnString = ValidationField(name, floatValue, style, isValidFloatChar, IsValidFloat, "0");
        float returnValue;

        if (float.TryParse(returnString, out returnValue))
            return returnValue;

        return 0;

    }

    private static bool isValidFloatChar(char c)
    {
        bool outcome = (char.IsNumber(c) || (c == '.'));
        return outcome;
    }

    private static bool IsValidFloat(string value)
    {
        value += "0";
        float fValue;
        bool outcome = float.TryParse(value, out fValue);
        return outcome;
    }







    /* Int Fields */
    public static int IntField(string name, Rect rect, int value)
    {
        return IntField(name, rect, value, GUI.skin.textField);
    }

    public static int IntField(string name, Rect rect, int value, GUIStyle style)
    {
        string intValue = value.ToString();

        string returnString = ValidationField(name, rect, intValue, style, isValidIntChar, IsValidInt, "0");
        int returnValue;

        if (int.TryParse(returnString, out returnValue))
            return returnValue;

        return 0;
    }

    private static bool isValidIntChar(char c)
    {
        return (char.IsNumber(c) || (c == '-'));
        
    }

    private static bool IsValidInt(string value)
    {
        value += "0";
        int intValue;
        bool outcome = int.TryParse(value, out intValue);
        return outcome;
    }






    



}

