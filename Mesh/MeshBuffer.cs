﻿using UnityEngine;
using System.Collections.Generic;

namespace SixThreeZero.Core
{
    [System.Serializable]
    public class MeshBuffer
    {

        public List<Vector3> Vertexs = new List<Vector3>();
        public int SubMeshCount;
        public List<List<int>> Triangles = new List<List<int>>();
        public List<Material> Materials = new List<Material>();
        public List<Vector3> Normals = new List<Vector3>();
        public List<Vector4> Tangents = new List<Vector4>();
        public List<Vector4> UV0 = new List<Vector4>();
        public List<Vector4> UV1 = new List<Vector4>();
        public List<Vector4> UV2 = new List<Vector4>();
        public List<Vector4> UV3 = new List<Vector4>();
        public List<Color> Colors = new List<Color>();


        [System.NonSerialized] private List<Vector3> _TVertexs = new List<Vector3>();
        [System.NonSerialized] private int _TSubMeshCount;
        [System.NonSerialized] private List<List<int>> _TTriangles = new List<List<int>>();
        [System.NonSerialized] private List<Material> _TMaterials = new List<Material>();
        [System.NonSerialized] private List<Vector3> _TNormals = new List<Vector3>();
        [System.NonSerialized] private List<Vector4> _TTangents = new List<Vector4>();
        [System.NonSerialized] private List<Vector4> _TUV0 = new List<Vector4>();
        [System.NonSerialized] private List<Vector4> _TUV1 = new List<Vector4>();
        [System.NonSerialized] private List<Vector4> _TUV2 = new List<Vector4>();
        [System.NonSerialized] private List<Vector4> _TUV3 = new List<Vector4>();
        [System.NonSerialized] private List<Color> _TColors = new List<Color>();

        public MeshBuffer()
        {

        }

        public void Reset()
        {
            SubMeshCount = 0;
            Vertexs.Clear();
            Triangles.Clear();
            Normals.Clear();
            Tangents.Clear();
            UV0.Clear();
            UV1.Clear();
            UV2.Clear();
            UV3.Clear();
            Materials.Clear();
            Colors.Clear();
        }

        private void ResetTemps()
        {
            _TSubMeshCount = 0;
            _TVertexs.Clear();
            _TTriangles.Clear();
            _TNormals.Clear();
            _TTangents.Clear();
            _TUV0.Clear();
            _TUV1.Clear();
            _TUV2.Clear();
            _TUV3.Clear();
            _TMaterials.Clear();
            _TColors.Clear();
        }

        public MeshBuffer(Mesh mesh)
        {

            FromMesh(mesh);
        }

        public void FromMesh(Mesh mesh)
        {
            Reset();
            
            this.SubMeshCount = mesh.subMeshCount;
            mesh.GetVertices(this.Vertexs);
            for (int i = 0; i < this.SubMeshCount; i++)
            {
                Triangles.Add(new List<int>());
                //Triangles[i] = new List<int>();
                mesh.GetTriangles(Triangles[i], i);
            }

            mesh.GetTangents(Tangents);
            while (this.Tangents.Count < this.Vertexs.Count)
                this.Tangents.Add(Vector4.zero);

            mesh.GetNormals(Normals);
            while (this.Normals.Count < this.Vertexs.Count)
                this.Normals.Add(Vector3.zero);

            mesh.GetUVs(0, UV0);
            while (this.UV0.Count < this.Vertexs.Count)
                this.UV0.Add(Vector4.zero);

            mesh.GetUVs(1, UV1);
            while (this.UV1.Count < this.Vertexs.Count)
                this.UV1.Add(Vector4.zero);

            mesh.GetUVs(2, UV2);
            while (this.UV2.Count < this.Vertexs.Count)
                this.UV2.Add(Vector4.zero);

            mesh.GetUVs(3, UV3);
            while (this.UV3.Count < this.Vertexs.Count)
                this.UV3.Add(Vector4.zero);

            mesh.GetColors(Colors);
            while (this.Colors.Count < this.Vertexs.Count)
                this.Colors.Add(Color.white);
        }

        public void BuildToMesh(Mesh mesh)
        {
            mesh.subMeshCount = SubMeshCount;
            mesh.SetVertices(this.Vertexs);
            mesh.SetNormals(this.Normals);
            mesh.SetTangents(this.Tangents);
            mesh.SetUVs(0, UV0);
            mesh.SetUVs(1, UV1);
            mesh.SetUVs(2, UV2);
            mesh.SetUVs(3, UV3);
            mesh.SetColors(Colors);
            

           
            for (int i = 0; i < SubMeshCount; i++)
            {
                mesh.SetTriangles(Triangles[i], i);
            }
            mesh.RecalculateBounds();
        }

        public void AddMesh(Mesh mesh)
        {
            ResetTemps();
            this._TSubMeshCount = mesh.subMeshCount;
            mesh.GetVertices(this._TVertexs);
            for (int i = 0; i < this._TSubMeshCount; i++)
            {
                _TTriangles[i] = new List<int>();
                mesh.GetTriangles(_TTriangles[i], i);
            }

        
            mesh.GetTangents(_TTangents);
            while (this._TTangents.Count < this._TVertexs.Count)
                this._TTangents.Add(Vector4.zero);

            mesh.GetNormals(_TNormals);
            while (this._TNormals.Count < this._TVertexs.Count)
                this._TNormals.Add(Vector3.zero);

            mesh.GetUVs(0, _TUV0);
            while (this._TUV0.Count < this._TVertexs.Count)
                this._TUV0.Add(Vector4.zero);

            mesh.GetUVs(1, _TUV1);
            while (this._TUV1.Count < this._TVertexs.Count)
                this._TUV1.Add(Vector4.zero);

            mesh.GetUVs(2, _TUV2);
            while (this._TUV2.Count < this._TVertexs.Count)
                this._TUV2.Add(Vector4.zero);

            mesh.GetUVs(3, _TUV3);
            while (this._TUV3.Count < this._TVertexs.Count)
                this._TUV3.Add(Vector4.zero);

            mesh.GetColors(_TColors);
            while (this._TColors.Count < this._TVertexs.Count)
                this._TColors.Add(Color.white);

            int vertCount = Vertexs.Count;
            this.SubMeshCount += _TSubMeshCount;
            this.Vertexs.AddRange(_TVertexs);
            this.Normals.AddRange(_TNormals);
            this.Tangents.AddRange(_TTangents);
            this.UV0.AddRange(_TUV0);
            this.UV1.AddRange(_TUV1);
            this.UV2.AddRange(_TUV2);
            this.UV3.AddRange(_TUV3);
            this.Colors.AddRange(_TColors);

            int oTriangleCount = Triangles.Count;

            for (int i = 0; i < _TSubMeshCount; i++)
            {
                Triangles.Add(new List<int>());
                for (int n = 0; n < _TTriangles[i].Count; n++)
                {
                    _TTriangles[i][n] += vertCount;
   
                }
                
                Triangles[oTriangleCount + i].AddRange(_TTriangles[i]);
            }
            ResetTemps();

        }

        public void AddMesh(MeshBuffer meshBuffer)
        {
            ResetTemps();
            this._TSubMeshCount = meshBuffer.SubMeshCount;
            
            for (int i = 0; i < meshBuffer.SubMeshCount; i++)
            {
                _TTriangles.Add( new List<int>());
                _TTriangles[i].AddRange(meshBuffer.Triangles[i]);
            }
            


            int vertCount = Vertexs.Count;
            this.SubMeshCount += meshBuffer.SubMeshCount;
            this.Vertexs.AddRange(meshBuffer.Vertexs);
            this.Normals.AddRange(meshBuffer.Normals);
            this.Tangents.AddRange(meshBuffer.Tangents);
            this.UV0.AddRange(meshBuffer.UV0);
            this.UV1.AddRange(meshBuffer.UV1);
            this.UV2.AddRange(meshBuffer.UV2);
            this.UV3.AddRange(meshBuffer.UV3);
            this.Colors.AddRange(meshBuffer.Colors);
            

            int oTriangleCount = Triangles.Count;

            for (int i = 0; i < _TSubMeshCount; i++)
            {
                Triangles.Add(new List<int>());
                for (int n = 0; n < _TTriangles[i].Count; n++)
                {
                    _TTriangles[i][n] += vertCount;
                    
                }
                Triangles[oTriangleCount + i].AddRange(_TTriangles[i]);
            }
            ResetTemps();

        }

        public void Flatten()
        {
            SubMeshCount = 1;
            _TTriangles.Clear();
            _TTriangles.Add(new List<int>());
            for (int i = 0; i < Triangles.Count; i++)
                _TTriangles[0].AddRange(Triangles[i]);

            Triangles.Clear();
            Triangles.Add(new List<int>());
            Triangles[0].AddRange(_TTriangles[0]);

        }

        public Bounds GetBounds()
        {

            float tX, lX, tY, lY, tZ, lZ;

            if (Vertexs.Count == 0)
            {
                return new Bounds(Vector3.zero, Vector3.zero);
            }
            else
            {
                tX = lX = Vertexs[0].x;
                tY = lY = Vertexs[0].y;
                tZ = lZ = Vertexs[0].z;
            }

            for (int i = 1; i < Vertexs.Count; i++)
            {
                Vector3 vert = Vertexs[i];
                if (vert.x > tX)
                    tX = vert.x;
                if (vert.x < lX)
                    lX = vert.x;

                if (vert.y > tY)
                    tY = vert.y;
                if (vert.y < lY)
                    lY = vert.y;

                if (vert.z > tZ)
                    tZ = vert.z;
                if (vert.z < lZ)
                    lZ = vert.z;
            }

            float mX = lX + ((tX - lX) / 2f);
            float mY = lY + ((tY - lY) / 2f);
            float mZ = lZ + ((tZ - lZ) / 2f);
            float sX = Mathf.Abs(tX - lX);
            float sY = Mathf.Abs(tY - lY);
            float sZ = Mathf.Abs(tZ - lZ);


            return new Bounds(new Vector3(mX, mY, mZ), new Vector3(sX, sY, sZ));
        }
    }

}
