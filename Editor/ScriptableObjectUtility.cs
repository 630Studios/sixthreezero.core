﻿using UnityEngine;
using UnityEditor;
using System.IO;

public static class ScriptableObjectUtility
{
   

    public static T CreateAsset<T>() where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string path = EditorUtility.SaveFilePanelInProject("Save new " + typeof(T).Name, "new " + typeof(T).Name, "asset", "Save new obejct", AssetDatabase.GetAssetPath(Selection.activeObject));

        if (path == "")
        {
            EditorUtility.DisplayDialog("Invalid file path", "Invalid file path", "Click Here, obviously.");
            return null;
        }
        else if (Path.GetExtension(path) != ".asset")
        {
            EditorUtility.DisplayDialog("Invalid file extension", "Invalid file extension", "Do I have a choice?");
            return null;
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path);

        AssetDatabase.CreateAsset(asset, assetPathAndName);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        AssetDatabase.ImportAsset(assetPathAndName);
        
        return asset;
    }
}