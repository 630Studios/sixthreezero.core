﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SixThreeZero.Core.Geometry;
namespace SixThreeZero.Core.Providers
{
    public interface IShapeProvider
    {

        IShape GetShape();

    }
}

