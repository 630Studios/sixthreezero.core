﻿using UnityEngine;
using System.Collections.Generic;

using System.Linq;

using SixThreeZero.Core.Providers;

namespace SixThreeZero.Core.Providers
{
    public static class ProviderUtility
    {

        private static List<System.Type> _Providers = new List<System.Type>();
        private static string[] _ProviderNames;


        public static System.Type[] Providers
        {
            get
            {
                if (_Providers.Count > 1)
                    RefreshProviderTypes();

                return _Providers.ToArray();
            }
        }

        public static string[] ProviderNames
        {
            get { return _ProviderNames; }
        }

        public static void RefreshProviderTypes()
        {
            _Providers.Clear();
            _Providers.AddRange(ReflectionHelper.FindClassesWithInterface(typeof(IProvider)));
            _ProviderNames = _Providers.Select(p => p.Name).ToArray();
        }


    }
}
