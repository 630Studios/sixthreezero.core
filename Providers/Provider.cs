﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Core.Providers
{

    [System.Serializable]
    public class Provider
    {
        protected string _UniqueID = "";
        protected SystemType _ProviderType;
        protected DynamicBool _Enabled = new DynamicBool(true);
        protected DynamicScope _Scope = new DynamicScope();

        public SystemType ProviderType
        {
            get { return _ProviderType; }
        }

        public bool Enabled
        {
            get { return _Enabled.Value; }
            set { _Enabled.Value = value; }
        }

        public K Get<K>()
        {
            return default(K);
        }

        public DynamicScope Scope
        {
            get { return _Scope; }
            set { _Scope = value; }
        }
            
    }

    [System.Serializable]
    public class Provider<T> :Provider
    {

        public T Get()
        {
            return default(T);
        }




    }
}

