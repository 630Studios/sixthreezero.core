﻿using SixThreeZero.Core.Dynamics;


namespace SixThreeZero.Core.Providers
{
    public interface IProvider
    {
        System.Type ProviderType { get; }
        string UniqueID { get; }
        bool Enabled { get; set; }
        DynamicScope Scope { get; set; }
    }
}

