﻿using SixThreeZero.Core.Dynamics;
using SixThreeZero.Core.Geometry;

namespace SixThreeZero.Core.Providers
{
    [System.Serializable]
    public class DynamicShapeProvider : DynamicField<IShapeProvider>, IDynamicField
    {
        public DynamicShapeProvider() : base() { }
        public DynamicShapeProvider(IShapeProvider value) : base(value) { }
        public DynamicShapeProvider(IShapeProvider value, GetScopeDel getScopeFunc) : base(value, getScopeFunc) { }
    }
}