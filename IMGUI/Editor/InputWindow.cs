﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

namespace SixThreeZero.Core.IMGUI
{
    public class IntInputWindow : InputWindow<int>
    {

        public static void ShowWindow(string Title, string message, string acceptStr, string cancelStr, AcceptCallbackDel acceptCallback, UnityAction cancelCallback, ValidationCallbackDel validationCallback)
        {

            IntInputWindow window = EditorWindow.CreateInstance<IntInputWindow>();
            window.titleContent = new GUIContent(Title);
            float height = EditorStyles.helpBox.CalcHeight(new GUIContent(message), 300);

            window.minSize = new Vector2(300, height + 60);
            window.maxSize = new Vector2(300, height + 60);
            window._Message = message;
            window._AcceptStr = acceptStr;
            window._CancelStr = cancelStr;
            window._AcceptCallback = acceptCallback;
            window._CancelCallback = cancelCallback;
            window.Show();
        }
    }

    public class FloatInputWindow : InputWindow<float>
    {

        public static void ShowWindow(string Title, string message, string acceptStr, string cancelStr, AcceptCallbackDel acceptCallback, UnityAction cancelCallback, ValidationCallbackDel validationCallback)
        {

            FloatInputWindow window = EditorWindow.CreateInstance<FloatInputWindow>();
            window.titleContent = new GUIContent(Title);
            float height = EditorStyles.helpBox.CalcHeight(new GUIContent(message), 300);

            window.minSize = new Vector2(300, height + 60);
            window.maxSize = new Vector2(300, height + 60);
            window._Message = message;
            window._AcceptStr = acceptStr;
            window._CancelStr = cancelStr;
            window._AcceptCallback = acceptCallback;
            window._CancelCallback = cancelCallback;
            window.Show();
        }
    }
    public class InputWindow<T>: EditorWindow
    {
        public delegate void AcceptCallbackDel(T value);
        public delegate bool ValidationCallbackDel(T value);


        protected T _Value;

        public T Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        public string _Message;
        public AcceptCallbackDel _AcceptCallback;
        public UnityAction _CancelCallback;
        public ValidationCallbackDel _ValidationCallback;
        public string _AcceptStr;
        public string _CancelStr;

        public void Update()
        {
            
        }


        public void OnGUI()
        {
            GUILayout.BeginVertical();
            GUILayout.Label(_Message, EditorStyles.helpBox);
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Input: ", GUILayout.Width(50));
            System.Object val = _Value;
            CustomClassInspector.ShowDefaultEditor(typeof(T), ref val);
            _Value = (T)val;
            GUILayout.EndHorizontal();
            
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(_CancelStr))
            {
                if (_CancelCallback != null)
                {
                    _CancelCallback.Invoke();
                    
                }
                Close();
            }
            if (GUILayout.Button(_AcceptStr))
            {
                if (_ValidationCallback != null)
                {
                    bool outcome = _ValidationCallback(_Value);
                    if (outcome)
                    {
                        if (_AcceptCallback != null)
                            _AcceptCallback(_Value);
                        Close();
                    }
                }else
                {
                    if (_AcceptCallback != null)
                        _AcceptCallback(_Value);
                    Close();
                }
            }
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

        }

        public void OnEnable()
        {
            
        }

        public void OnDisable()
        {
            
        }

        public void OnLostFocus()
        {
            Focus();
        }

    }
}
