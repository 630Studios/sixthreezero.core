﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;



public static class CustomClassInspector
{

    public delegate bool CustomTypeHandlerValidation(System.Type type);
    public delegate void CustomTypeHandlerCallback(System.Type type, ref object value);

    public class CustomHandlerData
    {
        public CustomTypeHandlerValidation ValidationFunc;
        public CustomTypeHandlerCallback Callback;
        public CustomHandlerData(CustomTypeHandlerValidation validationFunc, CustomTypeHandlerCallback callbackFunc)
        {
            this.ValidationFunc = validationFunc;
            this.Callback = callbackFunc;
        }
    }

    public static List<CustomHandlerData> CustomHandlers = new List<CustomHandlerData>();


    public static void DrawEditor(System.Object target)
    {

        System.Type targetType = target.GetType();

        FieldInfo[] fields = targetType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);

        fields = fields.Where(p => (p.IsPublic  || System.Attribute.GetCustomAttribute(p, typeof(SerializeField)) != null) && System.Attribute.GetCustomAttribute(p, typeof(HideInInspector)) == null).ToArray();


        for (int i = 0; i < fields.Length;i++)
        {
            System.Type fieldType = fields[i].FieldType;

            System.Object value = fields[i].GetValue(target);
            GUILayout.BeginHorizontal();
            string prettyName = fields[i].Name.STZSplitCamelCase();
            GUILayout.Label(new GUIContent(prettyName, prettyName), GUILayout.Width(EditorGUIUtility.labelWidth));
            ShowDefaultEditor(fieldType, ref value);
            fields[i].SetValue(target, value);
            GUILayout.EndHorizontal();

        
        }
    }

    

    public static void ShowDefaultEditor(System.Type type, ref object value)
    {

        if (type == null)
        {
            Debug.Log("Null Type");
            return;
        }



        //Debug.Log(type.Name);

        if (type.IsGenericType)
        {
            System.Type genDef;
            if (!type.IsGenericTypeDefinition)
                genDef = type.GetGenericTypeDefinition();
            else
                genDef = type;

            if (value == null)
                value = System.Activator.CreateInstance(type);

            System.Type[] listType = type.GetGenericArguments();

            if (genDef == typeof(List<>))
            {
                displayListEditor(listType[0], value as IList);
                return;
            }

            if (genDef == typeof(Dictionary<,>))
            {

                displayDictionaryEditor(listType[0], listType[1], value as IDictionary);
                return;
            }


        }

        if (type.IsEnum)
        {
            if (value == null)
                value = System.Activator.CreateInstance(type);

            value = (System.Enum)EditorGUILayout.EnumPopup("", (System.Enum)value);
            return;
        }

        for (int i = 0; i < CustomHandlers.Count;i++)
        {
            if (CustomHandlers[i].ValidationFunc(type))
            {
                CustomHandlers[i].Callback(type, ref value);
                return;
            }
            
        }
        
        
        switch (type.Name)
        {
            case "bool":
            case "Boolean":
                bool boolValue = value == null ? default(bool) : (bool)value;
                value = EditorGUILayout.Toggle(boolValue);
                break;

            case "Byte":
                byte byteValue = value == null ? default(byte) : (byte)value;
                value = (byte)EditorGUILayout.IntField(byteValue);
                break;

            case "Sbyte":
                sbyte sbyteValue = value == null ? default(sbyte) : (sbyte)value;
                value = (byte)EditorGUILayout.IntField(sbyteValue);
                break;

            /*case "char":
                char charValue = value == null ? default(char) : (char)value;
                value = (byte)EditorGUILayout.IntField(charValue);
                break;*/
            case "Decimal":
                decimal decValue = value == null ? default(decimal) : (decimal)value;
                value = (decimal)EditorGUILayout.DoubleField((double)decValue);
                break;

            case "Double":
                double doubleVal = value == null ? default(double) : (double)value;
                value = EditorGUILayout.DoubleField(doubleVal);
                break;
            case "float":
            case "Single":
                float floatVal = value == null ? default(float) : (float)value;
                value = EditorGUILayout.FloatField(floatVal);
                break;

            case "int":
            case "Int32":
                int intValue = value == null ? default(int) : (int)value;
                value = EditorGUILayout.IntField(intValue);
                break;

            case "uint":
            case "UInt32":
                uint uintValue = value == null ? default(uint) : (uint)value;
                value = (uint)EditorGUILayout.IntField((int)uintValue);
                break;
            case "long":
            case "Int64":
                long longValue = value == null ? default(long) : (long)value;
                value = EditorGUILayout.LongField(longValue);
                break;
            case "ulong":
            case "UInt64":
                ulong ulongValue = value == null ? default(ulong) : (ulong)value;
                value = (ulong)EditorGUILayout.LongField((long)ulongValue);
                break;
            /*case "object":

                break;*/
            case "short":
            case "Int16":

                break;
            case "ushort":
            case "UInt16":

                break;
            case "string":
            case "String":
                string strValue = value == null ? "" : (string)value;
                value = EditorGUILayout.TextField("", strValue);
                break;
            case "Color":
                Color colorValue = value == null ? default(Color) : (Color)value;
                value = (Color)EditorGUILayout.ColorField((Color)colorValue);
                break;
            case "Color32":
                Color32 color32Value = value == null ? default(Color32) : (Color32)value;
                value = (Color32)EditorGUILayout.ColorField((Color32)color32Value);
                break;
            case "Vector2":
                Vector2 vec2Value = value == null ? default(Vector2) : (Vector2)value;
                value = EditorGUILayout.Vector2Field("", vec2Value);
                break;
            case "Vector3":
                Vector3 vec3Val = value == null ? default(Vector3) : (Vector3)value;
                value = EditorGUILayout.Vector3Field("", vec3Val);
                break;
            case "Vector4":
                Vector4 vec4Val = value == null ? default(Vector4) : (Vector4)value;
                value = EditorGUILayout.Vector4Field("", vec4Val);
                break;

            case "AnimationCurve":
                AnimationCurve curveValue = value == null ? new AnimationCurve() : (AnimationCurve)value;
                value = EditorGUILayout.CurveField(curveValue);
                break;

            /*case "GUIDataSource":
                GUIDataSource gds = (GUIDataSource) value;

                break;*/


            default:

                if (typeof(UnityEngine.Object).IsAssignableFrom(type))
                {
                    value = EditorGUILayout.ObjectField((Object)value, type, true);
                }

                break;

        }

    }

    private static void displayListEditor(System.Type listType, IList value)
    {

        if (value.Count == 0)
        {
            GUILayout.Label("No Items in list");
        }
        else
        {
            for (int i = 0; i < value.Count; i++)
            {

                var refItem = value[i];
                ShowDefaultEditor(listType, ref refItem);
                value[i] = refItem;
            }
        }
        GUILayout.BeginVertical();

        if (GUILayout.Button("Add Entry"))
        {
            if (listType == typeof(string))
                value.Add("");
            else
                value.Add(System.Activator.CreateInstance(listType));
        }
        GUILayout.EndVertical();
        return;
    }

    private static void displayDictionaryEditor(System.Type KeyType, System.Type valueType, IDictionary value)
    {

        if (value.Keys.Count == 0)
        {
            GUILayout.Label("No Items in Dictionary", EditorStyles.centeredGreyMiniLabel);
        }
        else
        {
            for (int i = 0; i < value.Keys.Count; i++)
            {
                GUILayout.BeginHorizontal();
                var refItem = value[i];
                foreach (var key in value.Keys)
                {
                    GUILayout.Label(key.ToString());
                    var item = value[key];
                    ShowDefaultEditor(valueType, ref item);
                    value[key] = item;
                }
                GUILayout.EndHorizontal();

            }
        }


        return;
    }


}





