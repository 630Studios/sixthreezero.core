﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero.Core.IMGUI
{

    public delegate bool ValidationFunc();
    public delegate Color ColorFunc();
    public delegate bool PanelValidationFunc(GUIPanel panel);
    public delegate float FloatFunc();
    public delegate void MouseEvent(GUIPanel panel, Vector2 localPosition);
    public delegate void PanelFunc(GUIPanel panel);
    public delegate GUIStyle StyleFunc();
    public class GUIPanel
    {
        public delegate void DisplayPanelFunc(GUIPanel panel);
        public delegate string StringFunc();


        public enum LAYOUT_DIRECTION
        {
            Horizontal = 0,
            Vertical = 1,
            Scrollview = 2,
            AbsoluteHorizontal = 3,
            AbsoluteVertical = 4,
        }



        protected ImprovedEditorWindow _Window;

        public StringFunc Name = () => "New Panel";
        public bool ShowHeader = false;
        public bool CanResizeWidth = false;
        public bool CanResizeHeight = false;
        public Vector2 ScrollPos = Vector2.zero;
        public ColorFunc GetHeaderColor = () => Color.grey;
        public StyleFunc GetHeaderStyle = () => EditorStyles.toolbarButton;

        public FloatFunc GetLeftFunc = () => 0;
        public FloatFunc GetTopFunc = () => 0;
        public FloatFunc GetHeightFunc = () => 100;
        public FloatFunc GetWidthFunc = () => 100;
        public PanelValidationFunc IsVisibleFunc = (p) => true;
        public LAYOUT_DIRECTION LayoutDirection = LAYOUT_DIRECTION.Horizontal;
        public event DisplayPanelFunc ContentFunc = null;
        public event DisplayPanelFunc PreContentFunc = null;

        public event DisplayPanelFunc OnMouseEnter;
        public event DisplayPanelFunc OnMouseExit;
        public event MouseEvent OnMouseMove;
        public event MouseEvent OnMouseDown;
        public event MouseEvent OnMouseUp;
        public event MouseEvent OnMouseDrag;

        public event DisplayPanelFunc OnPanelUpdate;

        [System.NonSerialized]
        public List<EditorMenuItem> TopLevelMenus = new List<EditorMenuItem>();
        public List<GUIPanel> Children = new List<GUIPanel>();

        public GUIStyle PanelStyle;

        protected float _CachedLeft = 0;
        protected float _CachedTop = 0;
        protected float _CachedWidth = 0;
        protected float _CachedHeight = 0;
        protected Rect _CachedRect = Rect.zero;

        protected float _LastHeight = 0;
        protected float _LastWidth = 0;

        protected bool _IsResizingWidth = false;
        protected bool _IsResizingHeight = false;

        public bool IsDraggable = false;
        protected bool _IsDragging = false;

        private bool _WasMouseIn = false;

        public float CachedLeft { get { return _CachedLeft; } }
        public float CachedTop { get { return _CachedTop; } }
        public float CachedWidth
        {
            get { return _CachedWidth; }
        }
        public float CachedHeight
        {
            get { return _CachedHeight; }
        }

        public Rect CachedRect { get { return _CachedRect; } }



        public GUIPanel(ImprovedEditorWindow window)
        {
            _Window = window;
            PanelStyle = new GUIStyle(EditorStyles.objectFieldThumb);
            PanelStyle.margin = new RectOffset(0, 0, 0, 0);
            PanelStyle.padding = new RectOffset(0, 0, 0, 0);
        }




        public virtual void Display()
        {
            if (!IsVisibleFunc(this))
                return;

            _CachedLeft = GetLeftFunc();
            _CachedTop = GetTopFunc();
            _CachedWidth = GetWidthFunc();
            _CachedHeight = GetHeightFunc();
            _CachedRect = new Rect(_CachedLeft, _CachedTop, _CachedWidth, _CachedHeight);

            CheckForResize();
            CheckMouseEvents();



            switch (LayoutDirection)
            {
                case LAYOUT_DIRECTION.Horizontal:
                    GUILayout.BeginVertical(PanelStyle, GUILayout.Width(_CachedWidth), GUILayout.Height(_CachedHeight));
                    if (ShowHeader)
                        DrawHeader(Name());

                    DrawTopToolbar();

                    GUILayout.BeginHorizontal();

                    if (PreContentFunc != null)
                        PreContentFunc(this);

                    if (ContentFunc != null)
                        ContentFunc(this);

                    DisplayChildren();

                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    RecordPosition();
                    break;
                case LAYOUT_DIRECTION.Vertical:
                    GUILayout.BeginVertical(PanelStyle, GUILayout.Width(_CachedWidth), GUILayout.Height(_CachedHeight));
                    if (ShowHeader)
                        DrawHeader(Name());
                    DrawTopToolbar();

                    GUILayout.BeginVertical();
                    if (PreContentFunc != null)
                        PreContentFunc(this);

                    if (ContentFunc != null)
                        ContentFunc(this);

                    DisplayChildren();
                    GUILayout.EndVertical();
                    GUILayout.EndVertical();
                    RecordPosition();
                    break;
                case LAYOUT_DIRECTION.Scrollview:
                    GUILayout.BeginVertical(PanelStyle, GUILayout.Width(_CachedWidth), GUILayout.Height(_CachedHeight));
                    if (ShowHeader)
                        DrawHeader(Name());
                    DrawTopToolbar();

                    ScrollPos = GUILayout.BeginScrollView(ScrollPos, GUILayout.Width(_CachedWidth), GUILayout.Height(_CachedHeight));

                    if (PreContentFunc != null)
                        PreContentFunc(this);

                    if (ContentFunc != null)
                        ContentFunc(this);

                    DisplayChildren();

                    GUILayout.EndScrollView();
                    GUILayout.EndVertical();
                    RecordPosition();
                    break;
                case LAYOUT_DIRECTION.AbsoluteHorizontal:
                    GUILayout.BeginArea(new Rect(GetLeftFunc(), GetTopFunc(), GetWidthFunc(), GetHeightFunc()));
                    GUILayout.BeginVertical(PanelStyle, GUILayout.Width(_CachedWidth), GUILayout.Height(_CachedHeight));
                    if (ShowHeader)
                        DrawHeader(Name());

                    DrawTopToolbar();

                    GUILayout.BeginHorizontal();
                    if (PreContentFunc != null)
                        PreContentFunc(this);

                    if (ContentFunc != null)
                        ContentFunc(this);

                    DisplayChildren();

                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    GUILayout.EndArea();

                    break;
                case LAYOUT_DIRECTION.AbsoluteVertical:
                    GUILayout.BeginArea(new Rect(GetLeftFunc(), GetTopFunc(), GetWidthFunc(), GetHeightFunc()));
                    GUILayout.BeginVertical(PanelStyle, GUILayout.Width(_CachedWidth), GUILayout.Height(_CachedHeight));
                    if (ShowHeader)
                        DrawHeader(Name());

                    DrawTopToolbar();

                    GUILayout.BeginVertical();
                    if (PreContentFunc != null)
                        PreContentFunc(this);

                    if (ContentFunc != null)
                        ContentFunc(this);

                    DisplayChildren();
                    GUILayout.EndVertical();
                    GUILayout.EndVertical();
                    GUILayout.EndArea();
                    ;
                    break;

            }





        }




        private void CheckMouseEvents()
        {
            if (Event.current.isMouse)
            {
                Rect lastRect = new Rect(_CachedLeft, _CachedTop, _CachedWidth, _CachedHeight);
                EventType eType = Event.current.type;

                Vector2 mousePos = Event.current.mousePosition;
                bool containsMouse = lastRect.Contains(mousePos);


                if (eType == EventType.mouseMove && !_IsDragging)
                {
                    if ((_WasMouseIn == false) && containsMouse)
                    {
                        _WasMouseIn = true;

                        if (OnMouseEnter != null)
                            OnMouseEnter(this);

                        return;
                    }

                    if ((_WasMouseIn == true) && !containsMouse)
                    {
                        _WasMouseIn = false;

                        if (OnMouseExit != null)
                            OnMouseExit(this);

                        return;
                    }

                    if ((_WasMouseIn == true) && containsMouse)
                    {

                        if (OnMouseMove != null)
                            OnMouseMove(this, mousePos - new Vector2(_CachedLeft, _CachedTop));

                        return;
                    }

                    return;
                }

                if (eType == EventType.MouseDown && containsMouse)
                {
                    if (OnMouseDown != null)
                    {
                        OnMouseDown(this, mousePos - new Vector2(_CachedLeft, _CachedTop));

                    }
                    return;
                }

                if (eType == EventType.MouseUp && containsMouse)
                {
                    if (OnMouseUp != null)
                    {
                        OnMouseUp(this, mousePos - new Vector2(_CachedLeft, _CachedTop));

                    }
                    return;
                }

                if (eType == EventType.MouseDrag)
                {

                    if (_IsDragging)
                    {
                        float left = GetLeftFunc();
                        float top = GetTopFunc();
                        left += Event.current.delta.x;
                        top += Event.current.delta.y;
                        GetLeftFunc = () => left;
                        GetTopFunc = () => top;
                        _Window.Repaint();
                        DispatchUpdate();

                    }
                    else
                    {
                        if (OnMouseDrag != null && containsMouse)
                        {
                            OnMouseDrag(this, mousePos - new Vector2(_CachedLeft, _CachedTop));

                        }
                    }

                    return;
                }

            }

        }
        private void CheckForResize()
        {

            if (Event.current.type == EventType.MouseDown)
            {
                bool nearWidth = (Mathf.Abs(Event.current.mousePosition.x - (_CachedLeft + _LastWidth)) < 6);
                bool nearHeight = (Mathf.Abs(Event.current.mousePosition.y - (_CachedTop + _LastHeight)) < 6);

                if ((nearWidth || nearHeight))
                {
                    if (nearWidth && CanResizeWidth)
                        _IsResizingWidth = true;

                    if (nearHeight && CanResizeHeight)
                        _IsResizingHeight = true;

                    Event.current.Use();

                    _Window.Repaint();
                    return;
                }
                return;
            }



            if (Event.current.type == EventType.MouseUp)
            {
                _IsResizingHeight = false;
                _IsResizingWidth = false;

                return;
            }

            if (Event.current.type == EventType.MouseDrag)
            {
                bool useEvent = false;
                if (_IsResizingWidth)
                {
                    float xWidth = Event.current.mousePosition.x;
                    xWidth = Mathf.Max(xWidth - _CachedLeft, 20);
                    GetWidthFunc = () => xWidth;
                    useEvent = true;
                }

                if (_IsResizingHeight)
                {
                    float yHeight = Event.current.mousePosition.y;
                    yHeight = Mathf.Max(yHeight - _CachedTop, 20);
                    GetHeightFunc = () => yHeight;
                    useEvent = true;
                }

                if (useEvent)
                    Event.current.Use();
            }
        }

        private void RecordPosition()
        {

            if (Event.current.type == EventType.Repaint)
            {
                Rect lastRect = GUILayoutUtility.GetLastRect();
                float left = lastRect.x;
                float top = lastRect.y;
                _LastHeight = lastRect.height;
                _LastWidth = lastRect.width;
                GetLeftFunc = () => left;
                GetTopFunc = () => top;
            }
        }

        private void RecordAbsolutePositions()
        {
            if (Event.current.type == EventType.repaint)
            {
                Rect lastRect = GUILayoutUtility.GetLastRect();
                _LastHeight = lastRect.height;
                _LastWidth = lastRect.width;
            }
        }

        public virtual void DisplayChildren()
        {
            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].Display();
            }
        }

        protected virtual void DrawTopToolbar()
        {
            if (TopLevelMenus.Count < 1)
                return;

            GUILayout.BeginHorizontal(EditorStyles.toolbar, GUILayout.Width(_CachedWidth));
            for (int i = 0; i < TopLevelMenus.Count; i++)
            {
                if (TopLevelMenus[i].Name == "<->")
                {
                    GUILayout.FlexibleSpace();
                    continue;
                }
                if (TopLevelMenus[i].HandleDisplay != null)
                {
                    TopLevelMenus[i].HandleDisplay(this);
                    continue;
                }

                if (GUILayout.Button(new GUIContent(TopLevelMenus[i].Name, TopLevelMenus[i].icon), EditorStyles.toolbarButton))
                {
                    if (TopLevelMenus[i].Children.Count > 0)
                    {
                        EditorMenuItem topItem = TopLevelMenus[i];
                        GenericMenu menu = new GenericMenu();
                        for (int n = 0; n < topItem.Children.Count; n++)
                        {

                            EditorMenuItem item = topItem.Children[n];
                            if (item.Name == "<->")
                            {
                                menu.AddSeparator("");
                            }
                            else
                            {
                                menu.AddItem(new GUIContent(item.Name, item.icon), item.IsActive(), () => item.action());
                            }
                        }
                        menu.ShowAsContext();
                    }
                    else
                    {
                        if (TopLevelMenus[i].action != null)
                            TopLevelMenus[i].action();
                    }
                }
            }
            GUILayout.EndHorizontal();
        }

        protected void DrawHeader(string title)
        {
            GUI.backgroundColor = GetHeaderColor();
            GUILayout.Label(title, EditorStyles.toolbarButton, GUILayout.ExpandWidth(true));
            GUI.backgroundColor = Color.white;



            if (IsDraggable && Event.current.isMouse)
            {


                switch (Event.current.type)
                {
                    case EventType.MouseDown:
                        Rect hRect = GUILayoutUtility.GetLastRect();
                        if (hRect.Contains(Event.current.mousePosition))
                        {
                            _IsDragging = true;
                            Event.current.Use();
                        }
                        break;

                    case EventType.MouseUp:

                        if (_IsDragging) 
                            _IsDragging = false;

                        break;

                }
            }

        }


        

        public void DrawHeader(string title, ref bool toggleVal)
        {

            

            

            GUI.backgroundColor = GetHeaderColor();
            if (GUILayout.Button(title, GetHeaderStyle(), GUILayout.ExpandWidth(true)))
                toggleVal = !toggleVal;
            GUI.backgroundColor = Color.white;



            if (IsDraggable && Event.current.isMouse)
            {


                switch (Event.current.type)
                {
                    case EventType.MouseDown:
                        Rect hRect = GUILayoutUtility.GetLastRect();
                        if (hRect.Contains(Event.current.mousePosition))
                        {
                            _IsDragging = true;
                            Event.current.Use();
                        }
                        break;

                    case EventType.MouseUp:

                        if (_IsDragging)
                            _IsDragging = false;

                        break;

                }
            }

        }


        private void DispatchUpdate()
        {
            if (OnPanelUpdate != null)
                OnPanelUpdate(this);
        }

        

    }

    public class GridPanel : GUIPanel
    {
        [System.Serializable]
        public struct GridPanelGridInfo
        {
            public string Name;
            public FloatFunc CellWidth;
            public FloatFunc CellHeight;
            public ColorFunc GridColor;

            public GridPanelGridInfo(string name, FloatFunc cellWidth, FloatFunc cellHeight, ColorFunc color)
            {
                this.Name = name;
                this.CellHeight = cellHeight;
                this.CellWidth = cellWidth;
                this.GridColor = color;
            }
        }

        public List<GridPanelGridInfo> Grids = new List<GridPanelGridInfo>();
        public FloatFunc Zoom = () => 1;



        public GridPanel(ImprovedEditorWindow window) : base(window)
        {
            ContentFunc += DrawGraphGrids;
        }


        public Vector2 Origin()
        {

            return new Vector2(_CachedWidth / 2, _CachedHeight / 2);

        }



        public void DrawGraphGrids(GUIPanel panel)
        {
            if (Event.current.type != EventType.Repaint)
                return;

            Vector2 origin = Origin();
            float zoom = Zoom();
            float gridWidth = _CachedWidth;
            float gridHeight = _CachedHeight;
            //gridSizeX = (int)gridSizeX * _Scale);
            //gridSizeY = (int)gridSizeY * _Scale);
            for (int q = 0; q < Grids.Count; q++)
            {
                float gridSizeX = Grids[q].CellWidth();
                float gridSizeY = Grids[q].CellHeight();
                gridSizeX = (int)(gridSizeX * zoom);
                gridSizeY = (int)(gridSizeY * zoom);
                Color color = Grids[q].GridColor();

                float gridLinesX = (gridWidth / gridSizeX) / 2;
                float gridLinesY = (gridHeight / gridSizeY) / 2;

                for (int i = 1; i < gridLinesX; i++)
                {
                    Handles.color = color;
                    Handles.DrawLine(new Vector3(origin.x - (i * gridSizeX), 0, 0), new Vector3(origin.x - (i * gridSizeY), gridHeight, 0));
                    Handles.DrawLine(new Vector3(origin.x + (i * gridSizeX), 0, 0), new Vector3(origin.x + (i * gridSizeY), gridHeight, 0));
                }

                for (int i = 1; i < gridLinesY; i++)
                {
                    Handles.color = color;
                    Handles.DrawLine(new Vector3(0, origin.y - (i * gridSizeX), 0), new Vector3(gridWidth, origin.y - (i * gridSizeY), 0));
                    Handles.DrawLine(new Vector3(0, origin.y + (i * gridSizeX), 0), new Vector3(gridWidth, origin.y + (i * gridSizeY), 0));
                }



            }

            Handles.color = Color.yellow;
            Handles.DrawLine(new Vector3(origin.x, 0, 0), new Vector3(origin.x, gridHeight, 0));
            Handles.DrawLine(new Vector3(0, origin.y, 0), new Vector3(gridWidth, origin.y, 0));
        }

    }



    public class EditorMenuItem
    {

        public string Name = "New Item";
        public Texture2D icon = null;
        public ValidationFunc Enabled = () => true;
        public ValidationFunc IsActive = () => false;
        public UnityAction action = null;
        public List<EditorMenuItem> Children = new List<EditorMenuItem>();
        public PanelFunc HandleDisplay;

        public static EditorMenuItem FlexSpace()
        {
            return new EditorMenuItem() { Name = "<->" };
        }
    }


    public class ImprovedEditorWindow : EditorWindow
    {

        //public delegate void KeyboardEvent(KeyCode code);

        [System.Serializable]
        public class KeyboardEvent : UnityEvent<KeyCode> { };









        [System.NonSerialized]
        protected static bool _IsRecoveringFromReload = false;
        [System.NonSerialized]
        public List<GUIPanel> Panels = new List<GUIPanel>();


        public KeyboardEvent OnKeyDown = new KeyboardEvent();

        private bool _ConstantUpdate = false;
        public bool ConstantUpdate
        {
            get { return _ConstantUpdate; }
            set
            {
                _ConstantUpdate = value;
            }
        }

        protected Vector2 _CurrentMousePosition = Vector2.zero;

        public virtual void Awake()
        {
            Initialize();
        }

        public virtual void OnDestroy()
        {

        }

        public virtual void Update()
        {

            if (ConstantUpdate)
                Repaint();
        }

        public virtual void OnGUI()
        {

            if (_IsRecoveringFromReload)
                HandleReloadRecovery();

            if (Event.current.isMouse)
                _CurrentMousePosition = Event.current.mousePosition;

            for (int i = 0; i < Panels.Count; i++)
            {
                Panels[i].Display();
            }


            if (Event.current.type == EventType.KeyDown)
            {
                if (OnKeyDown != null)
                    OnKeyDown.Invoke(Event.current.keyCode);
            }
        }

        [UnityEditor.Callbacks.DidReloadScripts]
        public static void HandleScriptReload()
        {

            _IsRecoveringFromReload = true;

        }


        public virtual void Initialize()
        {
            Panels.Clear();
        }

        public virtual void HandleReloadRecovery()
        {
            _IsRecoveringFromReload = false;
            Initialize();
        }





    }
}
