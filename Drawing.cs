﻿using UnityEngine;
using System.Collections.Generic;
public static class Drawing
{
    
    public static Material LineMaterial = new Material(Shader.Find("Sprites/Default"));
    public static Color LineColor = Color.white;

    public static bool DrawInWorldSpace = false;
    public static Camera TargetCamera;

    /* 2D Ddawing
     * 
     * 
     * Public
     * ---------------
     * Draw2DBezier
     * Draw2DLine
     * 
     * Private
     * ---------------
     * Get2DBezierPoints
     * GetPointOn2DBezier
     * 
     */


    public static void DrawAnimationCurve(Rect viewRect, AnimationCurve value, bool unifiedSCale = true)
    {

        int samplePoints = (int)(viewRect.width / 3);
        //int samplePerKey = (samplePoints / value.keys.Length);
        //int totalPoints = samplePerKey * value.keys.Length;
        //List<Vector2> sPoints = new List<Vector2>();
        Vector2[] points = new Vector2[samplePoints+1];

        Vector2 scale = viewRect.size;

        /*for (int i = 0; i < value.keys.Length - 1; i++)
        {
            float timeStart = value.keys[i].time;
            float timeDistance = value.keys[i + 1].time - timeStart;
            float timePercent = timeDistance / 1f;
            int realSampleCount = (int)(timePercent * totalPoints);    
            for (int n = 1; n <= realSampleCount;n++)
            {
                float sampleTime = ((float)n) / ((float)realSampleCount);

                float currentTime = timeStart + sampleTime;
                sPoints.Add( new Vector2(currentTime, value.Evaluate(currentTime)));
            }
        }
        points = sPoints.ToArray();*/
        for (int i = 0; i <= samplePoints; i++)
        {
                points[i] = (new Vector2( ((float)i) / (float)samplePoints, value.Evaluate(((float)i) / (float)samplePoints)));
        }

        Rect originalBounds = GeometryMath.GetBounds(points);
        float scaledBottom = originalBounds.y * viewRect.width;

        Vector2 originalCenter = originalBounds.center;
        //Vector2 scaledCenter = originalCenter * viewRect.width;

        
        

        for (int i = 0; i < points.Length; i++)
        {

            points[i] *= viewRect.width;

            points[i].y -= scaledBottom + viewRect.height;
            points[i] += viewRect.position;

            

        }
        Draw2DLine(points, Color.green);
    }

    

    /* Circles */
    public static void Draw2DCircleOutline(Vector2 center, int radius, int segments)
    {
        Draw2DCircleOutline(center, radius, segments, LineColor);
    }
    public static void Draw2DCircleOutline(Vector2 center, int radius, int segments, Color color)
    {
        Draw2DLine(GeometryMath.GetPointsOnCircleD(center, radius, 0, 360, segments), color);
    }


    /* Complex Beziers */
    public static void DrawComplex2DBezier(Vector2[] points, int segmentsPerCurve)
    {
        DrawComplex2DBezier(points, segmentsPerCurve, LineColor);
    }
    /// <summary>
    /// Draws a set of connected bezier curvers as defined by points. Points expects the values to be entered in pairs SegmentPoint, then ControlPoint.
    /// Curvers are defined by bezier(index, index+1, index+3, index+2). Where index, and index+2 are SegmentPoint, and index+1 and index+2 are ControlPoints
    /// for their previous segmentPoint.
    /// </summary>
    /// <param name="points">An array of points. Points should be entered in pairs. SegmentPoint, ControlPoint.</param>
    /// <param name="segmentsPerCurve">How many segments each bezier curve should be comprised of.</param>
    /// <param name="color">what color to draw the curve in.</param>
    public static void DrawComplex2DBezier(Vector2[] points, int segmentsPerCurve, Color color)
    {


        Draw2DLine(GeometryMath.GetComomplex2DBezierPoints(points, segmentsPerCurve), color);

    }

    /* Single Bezier */
    public static void Draw2DBezier(Vector2 start, Vector2 control1, Vector2 control2, Vector2 end, int segments)
    {
        Draw2DLine(GeometryMath.Get2DBezierPoints(start, control1, control2, end, segments), LineColor, false);
    }
    public static void Draw2DBezier(Vector2 start, Vector2 control1, Vector2 control2, Vector2 end, int segments, Color color)
    {
        Draw2DLine(GeometryMath.Get2DBezierPoints(start, control1, control2, end, segments), color, false);
    }

    /* Lines */
    public static void Draw2DLine(Vector2[] points, bool close = false)
    {
        Draw2DLine(points, LineColor, close);
    }
    public static void Draw2DLine(Vector2[] points, Color color, bool close = false)
    {
        Begin2DLineDrawing(color, GL.LINE_STRIP);
        for (int i = 0; i < points.Length; i++)
            GL.Vertex(points[i]);

        if (close)
            GL.Vertex(points[0]);
        End2DLineDrawing();
    }

    public static void Draw2DTriangles(Vector2[] points, Color color)
    {
        for (int i = 0; i < points.Length; i++)
        {

        }
    }





    /* 2D Begin & End functions */
    private static void Begin2DLineDrawing(Color color, int mode)
    {
        if (Event.current.type == EventType.Repaint && LineMaterial != null)
        {
            
            GL.PushMatrix();
            LineMaterial.SetPass(0);
            if (DrawInWorldSpace)
            {
                GL.LoadProjectionMatrix(TargetCamera.projectionMatrix);
                GL.modelview = TargetCamera.worldToCameraMatrix;
            }
            else
            {
                GL.LoadPixelMatrix();
            }
            GL.Color(color);
            GL.Begin(mode);

        }
    }
    private static void End2DLineDrawing()
    {
        if (Event.current.type == EventType.Repaint && LineMaterial != null)
        {
            GL.End();
            GL.PopMatrix();
        }
    }




    /* 3D Drawing
     * Draw3DBezier
     * Draw3DLine
     * 
     * 
     * Get3DBezierPoints
     * PointOn3DBezier
     * Begin3DLineDrawing
     * End3DLineDrawing
     */


    public static void Draw2DPointIn3D(Vector2[] points, Vector3 position, Quaternion rotation, Vector3 scale, Color color)
    {
        Vector3[] newPoints = new Vector3[points.Length];
        Matrix4x4 matrix = Matrix4x4.TRS(position, rotation, scale);

        for (int i = 0; i < points.Length;i++)
        {
            newPoints[i] = matrix.MultiplyPoint3x4(new Vector3(points[i].x, points[i].y, 0));
        }

        Draw3DLine(newPoints, color);
    }
    /*Complex Beziers*/
    public static void DrawComplex3DBezier(Vector3[] points, int segmentsPerCurve, Color color)
    {
        int numberOfCurves = (points.Length / 2) - 1;
        int numberOfSegments = numberOfCurves * segmentsPerCurve;
        int numberOfPoints = numberOfSegments + 1;
        Vector3[] curvePoints = new Vector3[numberOfPoints];
        int nextPointIndex = 0;
        for (int i = 0; i < points.Length - 2; i += 2)
        {

            Vector3 inverseFirstControl = points[i] + ((points[i + 1] - points[i]) * -1);
            Vector3[] subPoints = GeometryMath.Get3DBezierPoints(points[i], inverseFirstControl, points[i + 3], points[i + 2], segmentsPerCurve);

            /* the first point of every bezier after the first one is the
             * same point as the last point in the last bezier, so we skip saving it to our list of points
             */
            if (i == 0)
            {
                for (int n = 0; n < subPoints.Length; n++)
                {
                    curvePoints[nextPointIndex] = subPoints[n];
                    nextPointIndex++;
                }
            }
            else
            {
                for (int n = 1; n < subPoints.Length; n++)
                {
                    curvePoints[nextPointIndex] = subPoints[n];
                    nextPointIndex++;
                }
            }

        }

        Draw3DLine(curvePoints, color);

    }

    /* Single Bezier*/
    public static void Draw3DBezier(Vector3 start, Vector3 control1, Vector3 control2, Vector3 end, int segments)
    {
        Draw3DLine(GeometryMath.Get3DBezierPoints(start, control1, control2, end, segments), LineColor, false);
    }
    public static void Draw3DBezier(Vector3 start, Vector3 control1, Vector3 control2, Vector3 end, int segments, Color color)
    {
        Draw3DLine(GeometryMath.Get3DBezierPoints(start, control1, control2, end, segments), color, false);
    }

    /* Line */
    public static void Draw3DLine(Vector3[] points, bool close = false)
    {
        Draw3DLine(points, LineColor, close);
    }
    public static void Draw3DLine(Vector3[] points, Color color, bool close = false)
    {
        Begin3DLineDrawing(color, GL.LINE_STRIP);
        for (int i = 0; i < points.Length; i++)
            GL.Vertex3(points[i].x, points[i].y, points[i].z);

        if (close)
            GL.Vertex3(points[0].x, points[0].y, points[0].z);
        End3DLineDrawing();
    }



    /*3D begin and end functions*/
    private static void Begin3DLineDrawing(Color color, int mode)
    {
        
        GL.PushMatrix();
        LineMaterial.SetPass(0);
        GL.LoadProjectionMatrix(TargetCamera.projectionMatrix);
        GL.modelview = TargetCamera.worldToCameraMatrix;
        GL.Color(color);
        GL.Begin(mode);
    }
    private static void End3DLineDrawing()
    {

        GL.End();
        GL.PopMatrix();
    }

}

