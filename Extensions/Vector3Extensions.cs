﻿using UnityEngine;


public static class Vector3Extensions
{

    /*
     *  approx extensions for all vector2 and vector3 formats. Since vectors use floats, you can have "indentical" points that
     *  are not numerically equivelant. Approx checks to see if points are "indentical" withing a given variance.
     */
    public static bool approx(this Vector3 vec1, Vector3 vec2, float variance = 0.0001f)
    {
        return (Mathf.Abs(vec1.x - vec2.x) < variance) && (Mathf.Abs(vec1.y - vec2.y) < variance) && Mathf.Abs(vec1.z - vec2.z) < variance;
    }

    public static Vector3 midpoint(this Vector3 vec1, Vector3 vec2)
    {

        return new Vector3((vec1.x + vec2.x) / 2f, (vec1.y + vec2.y) / 2f, (vec1.z + vec2.z) / 2f);
    }


}


