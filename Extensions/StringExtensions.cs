﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using SixThreeZero;


public static class StringExtensions
{


    public static string STZSplitCamelCase(this string input)
    {
        return System.Text.RegularExpressions.Regex.Replace(input, "([A-Z])", " $1").Trim();
    }

}