﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero.Core.Extensions
{
    public static class ReflectionExtensions
    {
        public static bool IsNullable(this System.Type type)
        {
            return
                type != null &&
                type.IsGenericType &&
                type.GetGenericTypeDefinition() == typeof(System.Nullable<>);
        }

        public static bool IsNumeric(this System.Type type)
        {
            if (type == null || type.IsEnum)
                return false;

            if (IsNullable(type))
                return IsNumeric(System.Nullable.GetUnderlyingType(type));

            switch (System.Type.GetTypeCode(type))
            {
                case System.TypeCode.Byte:
                case System.TypeCode.Decimal:
                case System.TypeCode.Double:
                case System.TypeCode.Int16:
                case System.TypeCode.Int32:
                case System.TypeCode.Int64:
                case System.TypeCode.SByte:
                case System.TypeCode.Single:
                case System.TypeCode.UInt16:
                case System.TypeCode.UInt32:
                case System.TypeCode.UInt64:
                    return true;
                default:
                    return false;
            }
        }
    }
}
