﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class Vector2Extensions
{

    /*
    *  approx extensions for all vector2 and vector3 formats. Since vectors use floats, you can have "indentical" points that
    *  are not numerically equivelant. Approx checks to see if points are "indentical" withing a given variance.
    */

    public static bool approx(this Vector2 vec1, Vector2 vec2, float variance = 0.0001f)
    {
        return (Mathf.Abs(vec1.x - vec2.x) < variance) && (Mathf.Abs(vec1.y - vec2.y) < variance);
    }


    /*
     * midpoint extenions for vector2 and vector3 allowing you to quickly find the midpoint between any two points
     */
    public static Vector2 midpoint(this Vector2 vec1, Vector2 vec2)
    {

        return new Vector2((vec1.x + vec2.x) / 2f, (vec1.y + vec2.y) / 2f);
    }

    public static Vector3 ToVector3(this Vector2 vec1)
    {

        return new Vector3(vec1.x, vec1.y, 0);
    }

    public static void Offset(this Vector2[] vecList, Vector2 offset)
    {
        for (int i = 0; i < vecList.Length; i++)
        {
            vecList[i] += offset;
        }

    }

    public static Vector3[] ToV3Array(this Vector2[] vecList)
    {
        Vector3[] newArray = new Vector3[vecList.Length];
        for (int i = 0; i < vecList.Length; i++)
        {
            newArray[i] = new Vector3(vecList[i].x, vecList[i].y, 0);
        }
        return newArray;
    }



    public static bool IsPathClosed(this Vector2[] vecList)
    {
        if (vecList.Length < 3)
            return false;

        Vector2 p1 = vecList[0];
        Vector2 endP = vecList[vecList.Length - 1];

        if (p1.approx(endP)) 
            return true;

        return false;
    }

    public static bool IsPathClosed(this List<Vector2> vecList)
    {
        if (vecList.Count < 3)
            return false;

        Vector2 p1 = vecList[0];
        Vector2 endP = vecList[vecList.Count - 1];

        if (p1.approx(endP))
            return true;

        return false;
    }

    public static bool IsPathClockwise(this List<Vector2> vecList)
    {


        float pTotal = 0;

        if (vecList.IsPathClosed())
        {
            for (int i = 0; i < vecList.Count - 1; i++)
            {
                pTotal += ((vecList[i + 1].x - vecList[i].x) * (vecList[i + 1].y + vecList[i].y));
            }

            if (pTotal > 0)
                return true;

            return false;

        }
        else
        {
            for (int i = 0; i < vecList.Count; i++)
            {
                int nI = (i + 1) % vecList.Count;
                pTotal += ((vecList[nI].x - vecList[i].x) * (vecList[nI].y + vecList[i].y));
            }

            if (pTotal > 0)
                return true;

            return false;
        }

    }

    public static bool IsPathClockwise(this Vector2[] vecList)
    {


        float pTotal = 0;

        if (vecList.IsPathClosed())
        {
            for (int i = 0; i < vecList.Length - 1; i++)
            {
                pTotal += ((vecList[i + 1].x - vecList[i].x) * (vecList[i + 1].y + vecList[i].y));
            }

            if (pTotal > 0)
                return true;

            return false;

        }
        else
        {
            for (int i = 0; i < vecList.Length; i++)
            {
                int nI = (i + 1) % vecList.Length;
                pTotal += ((vecList[nI].x - vecList[i].x) * (vecList[nI].y + vecList[i].y));
            }

            if (pTotal > 0)
                return true;

            return false;
        }

    }

    public static Vector2[] Reverse(this Vector2[] vecList)
    {
        int vecLength = vecList.Length;
        Vector2[] newList = new Vector2[vecLength];

        for (int i = 0; i < vecLength; i++)
        {
            newList[i] = vecList[vecLength - (i + 1)];
        }

        return newList;
    }

    public static Vector2 Rotate(this Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public static Rect GetBounds(this Vector2[] path)
    {
        float x = path.OrderBy(p => p.x).FirstOrDefault().x;
        float x2 = path.OrderBy(p => p.x).LastOrDefault().x;

        float y = path.OrderBy(p => p.y).FirstOrDefault().y;
        float y2 = path.OrderBy(p => p.y).LastOrDefault().y;

        return new Rect(x, y, x2 - x, y2 - y);
    }


}

