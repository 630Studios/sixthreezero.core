﻿using UnityEngine;
using System.Linq;



public static class TextureExtensions
{

    public static Texture2D CreateMaskFromAlpha(this Texture2D source, float cutoff = 1f)
    {
        Texture2D Output = new Texture2D(source.width, source.height, TextureFormat.ARGB32, false);

        CreateMaskFromAlpha(source, Output, cutoff);
        return Output;
    }  

    public static void CreateMaskFromAlpha(this Texture2D source, Texture2D output, float cutoff = 1f)
    {
        output.Resize(source.width, source.height, TextureFormat.ARGB32, false);

        /* black white
         * distance from white
         * distance from black
         * avearge*/ 

        Color[] sColors = source.GetPixels(); 
        for (int i = 0; i < sColors.Length; i++)
        {
            Color c = sColors[i];
            if (c.a < .5f)
            {
                sColors[i] =  new Color(0, 0, 1, 0);
            }
            else
            {
                sColors[i] = new Color(1, 1, 0, 1);
            }
        }


        output.SetPixels(sColors);
        output.Apply();
        return;
    }
}

