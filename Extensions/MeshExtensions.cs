﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class MeshExtensions
{

    public static void CopyFrom(this Mesh mesh, Mesh target)
    {
        mesh.Clear();
        mesh.subMeshCount = target.subMeshCount;
        mesh.vertices = target.vertices;
        mesh.colors = target.colors;

        for (int i = 0; i < mesh.subMeshCount; i++)
        {
            mesh.SetTriangles(target.GetTriangles(i), i);
        }
        

        mesh.normals = target.normals;
        mesh.uv = target.uv;
        mesh.uv2 = target.uv2;
        mesh.uv3 = target.uv3;
        mesh.uv4 = target.uv4;
        mesh.tangents = target.tangents;
    }


    public static void AddMesh(this Mesh mesh, Mesh target)
    {



        List<Vector4> v4Zero = new List<Vector4>();

        List<Vector4> uv0 = new List<Vector4>();
        List<Vector4> uv1 = new List<Vector4>();
        List<Vector4> uv2 = new List<Vector4>();
        List<Vector4> uv3 = new List<Vector4>();

        List<Vector3> normals = new List<Vector3>();
        List<Vector4> tangents = new List<Vector4>();
        List<Vector3> verts = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<List<int>> faces = new List<List<int>>();



        int vertCount = mesh.vertexCount;
        int SubMeshCount = mesh.subMeshCount;

        for (int i = 0; i < SubMeshCount; i++)
            faces.Add(new List<int>());

        while (v4Zero.Count < vertCount)
            v4Zero.Add(Vector4.zero);




        mesh.GetVertices(verts);
        mesh.GetNormals(normals);

        mesh.GetTangents(tangents);
        if (tangents.Count == 0)
            tangents.AddRange(v4Zero);




        mesh.GetUVs(0, uv0);

        if (uv0.Count == 0)
            uv0.AddRange(v4Zero);

        mesh.GetUVs(1, uv1);
        if (uv1.Count == 0)
            uv1.AddRange(v4Zero);

        mesh.GetUVs(2, uv2);
        if (uv2.Count == 0)
            uv2.AddRange(v4Zero);

        mesh.GetUVs(3, uv3);
        if (uv3.Count == 0)
            uv3.AddRange(v4Zero);

        mesh.GetColors(colors);
        while (colors.Count < vertCount)
            colors.Add(Color.white);

        for (int i = 0; i < SubMeshCount; i++)
        {
            faces[i].AddRange(mesh.GetTriangles(i));
        }


        List<Vector4> tv4Zero = new List<Vector4>();

        List<Vector4> tuv0 = new List<Vector4>();
        List<Vector4> tuv1 = new List<Vector4>();
        List<Vector4> tuv2 = new List<Vector4>();
        List<Vector4> tuv3 = new List<Vector4>();

        List<Vector3> tnormals = new List<Vector3>();
        List<Vector4> ttangents = new List<Vector4>();
        List<Vector3> tverts = new List<Vector3>();
        List<Color> tcolors = new List<Color>();
        List<List<int>> tfaces = new List<List<int>>();

        int tvertCount = target.vertexCount;
        int tSubMeshCount = target.subMeshCount;

        for (int i = 0; i < tSubMeshCount; i++)
            tfaces.Add(new List<int>());

        while (tv4Zero.Count < tvertCount)
            tv4Zero.Add(Vector4.zero);

        target.GetVertices(tverts);
        target.GetNormals(tnormals);

        target.GetTangents(ttangents);
        if (ttangents.Count == 0)
            ttangents.AddRange(tv4Zero);




        target.GetUVs(0, tuv0);

        if (tuv0.Count == 0)
            tuv0.AddRange(tv4Zero);

        target.GetUVs(1, tuv1);
        if (tuv1.Count == 0)
            tuv1.AddRange(tv4Zero);

        target.GetUVs(2, tuv2);
        if (tuv2.Count == 0)
            tuv2.AddRange(tv4Zero);

        target.GetUVs(3, tuv3);
        if (tuv3.Count == 0)
            tuv3.AddRange(tv4Zero);

        target.GetColors(tcolors);
        while (tcolors.Count < tvertCount)
            tcolors.Add(Color.white);

        for (int i = 0; i < tSubMeshCount; i++)
        {
            tfaces[i].AddRange(target.GetTriangles(i));
        }


        verts.AddRange(tverts);
        normals.AddRange(tnormals);
        tangents.AddRange(ttangents);
        uv0.AddRange(tuv0);
        uv1.AddRange(tuv1);
        uv2.AddRange(tuv2);
        uv3.AddRange(tuv3);
        colors.AddRange(tcolors);

        for (int n = 0; n < tfaces.Count; n++)
        {
            int tindex = faces.Count;
            faces.Add(new List<int>());
            for (int i = 0; i < tfaces[n].Count; i++)
            {
                faces[tindex].Add(vertCount + tfaces[n][i]);
            }
        }

        
        
        

        mesh.subMeshCount = faces.Count;
        mesh.SetVertices(verts);
        mesh.SetNormals(normals);
        mesh.SetTangents(tangents);
        for (int i = 0; i< faces.Count; i++)
            mesh.SetTriangles(faces[i], i);
        mesh.SetUVs(0, uv0);
        mesh.SetUVs(1, uv1);
        mesh.SetUVs(2, uv2);
        mesh.SetUVs(3, uv3);
        mesh.SetColors(colors);


        
        
    }

    public static void AddMesh(this Mesh mesh, Mesh target, Matrix4x4 matrix)
    {



        List<Vector4> v4Zero = new List<Vector4>();

        List<Vector4> uv0 = new List<Vector4>();
        List<Vector4> uv1 = new List<Vector4>();
        List<Vector4> uv2 = new List<Vector4>();
        List<Vector4> uv3 = new List<Vector4>();

        List<Vector3> normals = new List<Vector3>();
        List<Vector4> tangents = new List<Vector4>();
        List<Vector3> verts = new List<Vector3>();
        List<Color> colors = new List<Color>();
        List<int> faces = new List<int>();

         

        int vertCount = mesh.vertexCount;
        int SubMeshCount = mesh.subMeshCount;

        while (v4Zero.Count < vertCount)
            v4Zero.Add(Vector4.zero);




        mesh.GetVertices(verts);
        mesh.GetNormals(normals);

        mesh.GetTangents(tangents);
        if (tangents.Count == 0)
            tangents.AddRange(v4Zero);




        mesh.GetUVs(0, uv0);

        if (uv0.Count == 0)
            uv0.AddRange(v4Zero);

        mesh.GetUVs(1, uv1);
        if (uv1.Count == 0)
            uv1.AddRange(v4Zero);

        mesh.GetUVs(2, uv2);
        if (uv2.Count == 0)
            uv2.AddRange(v4Zero);

        mesh.GetUVs(3, uv3);
        if (uv3.Count == 0)
            uv3.AddRange(v4Zero);

        mesh.GetColors(colors);
        while (colors.Count < vertCount)
            colors.Add(Color.white);

        for (int i = 0; i < SubMeshCount; i++)
        {
            faces.AddRange(mesh.GetTriangles(i));
        }


        List<Vector4> tv4Zero = new List<Vector4>();

        List<Vector4> tuv0 = new List<Vector4>();
        List<Vector4> tuv1 = new List<Vector4>();
        List<Vector4> tuv2 = new List<Vector4>();
        List<Vector4> tuv3 = new List<Vector4>();

        List<Vector3> tnormals = new List<Vector3>();
        List<Vector4> ttangents = new List<Vector4>();
        List<Vector3> tverts = new List<Vector3>();
        List<Color> tcolors = new List<Color>();
        List<int> tfaces = new List<int>();

        int tvertCount = target.vertexCount;
        int tSubMeshCount = target.subMeshCount;

        while (tv4Zero.Count < tvertCount)
            tv4Zero.Add(Vector4.zero);

        target.GetVertices(tverts);
        target.GetNormals(tnormals);

        target.GetTangents(ttangents);
        if (ttangents.Count == 0)
            ttangents.AddRange(tv4Zero);




        target.GetUVs(0, tuv0);

        if (tuv0.Count == 0)
            tuv0.AddRange(tv4Zero);

        target.GetUVs(1, tuv1);
        if (tuv1.Count == 0)
            tuv1.AddRange(tv4Zero);

        target.GetUVs(2, tuv2);
        if (tuv2.Count == 0)
            tuv2.AddRange(tv4Zero);

        target.GetUVs(3, tuv3);
        if (tuv3.Count == 0)
            tuv3.AddRange(tv4Zero);

        target.GetColors(tcolors);
        while (tcolors.Count < tvertCount)
            tcolors.Add(Color.white);

        for (int i = 0; i < tSubMeshCount; i++)
        {
            tfaces.AddRange(target.GetTriangles(i));
        }

        for (int i = 0; i < tvertCount; i++)
        {
            tverts[i] = matrix.MultiplyPoint(tverts[i]);
            tnormals[i] = matrix.MultiplyVector(tnormals[i]);
            ttangents[i] = matrix.MultiplyVector(ttangents[i]);
        }



        verts.AddRange(tverts);
        normals.AddRange(tnormals);
        tangents.AddRange(ttangents);
        uv0.AddRange(tuv0);
        uv1.AddRange(tuv1);
        uv2.AddRange(tuv2);
        uv3.AddRange(tuv3);
        colors.AddRange(tcolors);

        
        for (int i = 0; i < tfaces.Count; i++)
        {
            faces.Add(vertCount + tfaces[i]);
        }





        mesh.subMeshCount = 1;
        mesh.SetVertices(verts);
        mesh.SetNormals(normals);
        mesh.SetTangents(tangents);
        mesh.SetTriangles(faces, 0);
        mesh.SetUVs(0, uv0);
        mesh.SetUVs(1, uv1);
        mesh.SetUVs(2, uv2);
        mesh.SetUVs(3, uv3);
        mesh.SetColors(colors);

        mesh.RecalculateBounds();


    }


    public static Mesh SubmeshToMesh(this Mesh mesh, int index)
    {
        int subMeshCount = mesh.subMeshCount;

        Mesh newMesh = new Mesh();
        bool hasNormal = false;
        bool hasUV = false;
        bool hasUV2 = false;
        bool hasUV3 = false;
        bool hasUV4 = false;
        bool hasColor = false;
        bool hasTangents = false;


        Vector3[] originalVerts = mesh.vertices;
        Vector3[] originalNormals = mesh.normals;
        Vector4[] originalTangents = mesh.tangents;

        
        int[] triangleVerts = mesh.GetTriangles(index);

        List<Vector4> originalUV = new List<Vector4>();
        mesh.GetUVs(0, originalUV);

        List<Vector4> originalUV2 = new List<Vector4>();
        mesh.GetUVs(1, originalUV2);

        List<Vector4> originalUV3 = new List<Vector4>();
        mesh.GetUVs(2, originalUV3);

        List<Vector4> originalUV4 = new List<Vector4>();
        mesh.GetUVs(3, originalUV4);

        Color32[] colors = mesh.colors32;

        if (originalNormals.Length > 0)
            hasNormal = true;

        if (originalUV.Count > 0)
            hasUV = true;

        if (originalUV2.Count > 0)
            hasUV = true;

        if (originalUV3.Count > 0)
            hasUV = true;

        if (originalUV4.Count > 0)
            hasUV = true;

        if (colors != null)
            if (colors.Length > 0)
                hasColor = true;

        if (originalTangents.Length > 0)
            hasTangents = true;

        Vector3[] newVerts = new Vector3[triangleVerts.Length];
        int[] newTrianglesVerts = new int[triangleVerts.Length];
        Vector3[] newNormals = new Vector3[triangleVerts.Length];
        Vector4[] newTangents = new Vector4[triangleVerts.Length];

        List<Vector4> newUV = new List<Vector4>();
        List<Vector4> newUV2 = new List<Vector4>();
        List<Vector4> newUV3 = new List<Vector4>();
        List<Vector4> newUV4 = new List<Vector4>();
        Color32[] newColors = new Color32[triangleVerts.Length];
        

        
        

        for (int i = 0; i < triangleVerts.Length; i++)
        {
            newVerts[i] = originalVerts[triangleVerts[i]];
            newTrianglesVerts[i] = i;

            if (hasNormal)
                newNormals[i] = originalNormals[i];

            if (hasUV)
                newUV.Add(originalUV[i]);

            if (hasUV2)
                newUV2.Add(originalUV[i]);

            if (hasUV3)
                newUV3.Add(originalUV[i]);

            if (hasUV4)
                newUV4.Add(originalUV[i]);

            if (hasColor)
                newColors[i] = colors[i];

            if (hasTangents)
                newTangents[i] = newTangents[i];
        }
        newMesh.vertices = newVerts;
        newMesh.triangles = newTrianglesVerts;

        if (hasNormal)
            newMesh.normals = newNormals;

        if (hasUV)
            mesh.SetUVs(0,newUV);

        if (hasUV2)
            mesh.SetUVs(1, newUV);

        if (hasUV3)
            mesh.SetUVs(2, newUV);

        if (hasUV4)
            mesh.SetUVs(3, newUV);

        if (hasColor)
            newColors = colors;

        if (hasTangents)
            mesh.tangents = newTangents;

        return newMesh;
    }

}

