﻿using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class CameraTools
{


    public static void SaveTexture2D(Texture2D texture, string path)
    {
        byte[] data = texture.EncodeToPNG();
        System.IO.File.WriteAllBytes(path, data);
    }


    public static Texture2D GenerateScreenshot(Camera useCamera, int imageWidth, int imageHeight)
    {


        RenderTexture oldActive = RenderTexture.active;
        RenderTexture oldTarget = useCamera.targetTexture;



        Texture2D output = new Texture2D(imageWidth, imageHeight);
        RenderTexture rt = new RenderTexture(imageWidth, imageHeight, 16);
        
        RenderTexture.active = rt;
        useCamera.targetTexture = rt;
        useCamera.Render();
        
        output.ReadPixels(new Rect(0, 0, imageWidth, imageHeight), 0, 0);
        output.Apply();

        RenderTexture.active = oldActive;
        useCamera.targetTexture = oldTarget;

        GameObject.DestroyImmediate(rt);
        return output;
    }



    public static Texture2D GenerateObjectScreenshot(GameObject target, Vector3 lookDirection, float fromDistance)
    {

        Dictionary<GameObject, int> originalLayers = new Dictionary<GameObject, int>();
        List<GameObject> gos = new List<GameObject>();

        int originalLayer = target.layer;

        Renderer[] renderers = target.GetComponentsInChildren<Renderer>();
        foreach (Renderer rend in renderers)
        {
            GameObject go = rend.gameObject;
            gos.Add(go);
            originalLayers.Add(go, go.layer);
            go.layer = 31;
        }

        Renderer renderer = target.GetComponent<Renderer>();
        Bounds bounds = renderer.bounds;


        target.layer = 31;


        Camera camera = CopyMainCamera();
        camera.cullingMask = 1 << 31;
        camera.clearFlags = CameraClearFlags.Nothing;
        camera.transform.position =  bounds.center + ((lookDirection * -1) * fromDistance);
        camera.transform.LookAt(bounds.center);


        Texture2D output = GenerateScreenshot(camera, camera.pixelWidth, camera.pixelHeight);



        
        foreach (GameObject go in gos)
        {
            go.layer = originalLayers[go];
        }

        gos.Clear();
        originalLayers.Clear();

        #if UNITY_EDITOR
            GameObject.DestroyImmediate(camera.gameObject);
        #else
            GameObject.Destroy(camera.gameObject);
        #endif

        return output;

    }




    public static Texture2D GenerateBillboardScreenshot(GameObject target, Vector3 lookDirection, float fromDistance)
    {

        Dictionary<GameObject, int> originalLayers = new Dictionary<GameObject, int>();
        List<GameObject> gos = new List<GameObject>();

        int originalLayer = target.layer;
        Renderer renderer = target.GetComponent<Renderer>();
        Bounds bounds = renderer.bounds;

        Renderer[] renderers = target.GetComponentsInChildren<Renderer>();
        foreach(Renderer rend in renderers)
        {
            bounds.Encapsulate(rend.bounds);
            GameObject go = rend.gameObject;
            gos.Add(go);
            originalLayers.Add(go, go.layer);
            go.layer = 31;
        }

        
       
        
        target.layer = 31;

        
        Camera camera = CreateBillboardCamera(bounds);

        camera.transform.position = (bounds.center + ((lookDirection * -1) * fromDistance));
        camera.transform.LookAt(bounds.center);

        Vector3 size = bounds.size;
        int width = 1024;
        int height = 1024;

        if (size.x > size.y)
        {
            width = 1024;
            height = (int) (1024 * (size.y / size.x));
        }else
        {
            width = (int)(1024 * (size.x / size.y));
            height = 1024;
        }

        
        Texture2D output = GenerateScreenshot(camera, width, height);
        

        foreach(GameObject go in gos)
        {
            go.layer = originalLayers[go];
        }

        gos.Clear();
        originalLayers.Clear();

        #if UNITY_EDITOR
            GameObject.DestroyImmediate(camera.gameObject);
        #else
            GameObject.Destroy(camera.gameObject);
        #endif

        return output;

    }


    public static Camera CreateBillboardCamera(Bounds bounds)
    {
        GameObject camGo = new GameObject("temp Camera");
        Camera camera = camGo.AddComponent<Camera>();
#if UNITY_EDITOR
        camera.CopyFrom(UnityEditor.SceneView.lastActiveSceneView.camera);
#endif
        camera.orthographic = true;
        camera.orthographicSize = (bounds.max.y - bounds.min.y) / 2.0f;


        camera.clearFlags = CameraClearFlags.Nothing;
        camera.cullingMask = 1 << 31;

        return camera;
    }

    public static Camera CopyMainCamera()
    {
        GameObject go = new GameObject();
        Camera camera = go.AddComponent<Camera>();
        camera.CopyFrom(Camera.main);
        return camera;
    }



}

