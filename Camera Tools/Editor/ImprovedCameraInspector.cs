﻿using UnityEngine;
using UnityEditor;
using SixThreeZero.CameraTool;

namespace Assets._630Studios.Billboard_Generator
{

    [CustomEditor(typeof(Camera))]
    public class ImprovedCameraInspector : Editor
    {


        public override void OnInspectorGUI()
        {

            Camera camera = (Camera)target;
            DrawDefaultInspector();

            if (GUILayout.Button("Take Picture"))
            {
                /*if (Selection.activeGameObject == null)
                    CameraTools.GenerateScreenshot(camera, 100, 250);
                else
                    EditorCameraTools.ScreenshotObjectMenuItem();*/
            }

            if (GUILayout.Button("Match Scene Camera"))
            {
                Camera sceneCam = UnityEditor.SceneView.lastActiveSceneView.camera;
                camera.transform.position = sceneCam.transform.position;
                camera.transform.rotation = sceneCam.transform.rotation;
            }
        }

    }


    
}


