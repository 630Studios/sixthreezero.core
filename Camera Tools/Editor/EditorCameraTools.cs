﻿using UnityEngine;
using UnityEditor;

namespace SixThreeZero.CameraTool
{
    public static class EditorCameraTools
    {


        [MenuItem("GameObject/630 Tools/Screenshot/Object", false, 0)]
        public static void ScreenshotObjectMenuItem()
        {
            if (Selection.activeGameObject != null)
            {
                GameObject target = Selection.activeGameObject;
                
                string path = EditorUtility.SaveFilePanel("Screenshot Save File", Application.dataPath, target.name + "_screenshot", "png");

                bool IsValid = true;

                if (string.IsNullOrEmpty(path))
                    IsValid = false;

                if (System.IO.Path.GetExtension(path) != ".png")
                    IsValid = false;

                if (!IsValid)
                {
                    EditorUtility.DisplayDialog("Invalid save path", "Im not sure how, but you messed something up. No screenshot will be taken.", "Whatever dude");
                    return;
                }

                Texture2D output = CameraTools.GenerateObjectScreenshot(Selection.activeGameObject, Vector3.forward, 10);
                CameraTools.SaveTexture2D(output, path);
                GameObject.DestroyImmediate(output);
                AssetDatabase.Refresh();

            }

        }

        [MenuItem("GameObject/630 Tools/Screenshot/Object Billboard - Front", false, 0)]
        public static void TakeBillboardScreenshotFront()
        {
            TakeBillboardScreenshot(Vector3.forward);
        }

        [MenuItem("GameObject/630 Tools/Screenshot/Object Billboard - Back", false, 0)]
        public static void TakeBillboardScreenshotBack()
        {
            TakeBillboardScreenshot(Vector3.back);
        }

        [MenuItem("GameObject/630 Tools/Screenshot/Object Billboard - Left", false, 0)]
        public static void TakeBillboardScreenshotLeft()
        {
            TakeBillboardScreenshot(Vector3.right);
        }


        [MenuItem("GameObject/630 Tools/Screenshot/Object Billboard - Right", false, 0)]
        public static void TakeBillboardScreenshotRight()
        {
            TakeBillboardScreenshot(Vector3.left);
        }

        public static void TakeBillboardScreenshot(Vector3 lookDirection, string path = "")
        {
            if (Selection.activeGameObject != null)
            {
                GameObject target = Selection.activeGameObject;

                if (string.IsNullOrEmpty(path))
                    path = GetSaveFilePath("Save Billboard Texture Image As", target.name + "_billboard", "png");

                if (string.IsNullOrEmpty(path))
                    return;

                Texture2D output = CameraTools.GenerateBillboardScreenshot(Selection.activeGameObject, lookDirection, 10);
                CameraTools.SaveTexture2D(output, path);
                GameObject.DestroyImmediate(output);
                AssetDatabase.Refresh();
            }

        }

        [MenuItem("GameObject/630 Tools/Billboard/From Selected", false, 0)]
        public static void MakeBillboardObject()
        {
            if (Selection.activeGameObject != null)
            {
                GameObject target = Selection.activeGameObject;

                string path = GetSaveFilePath("Save Billboard Texture Image As", target.name + "_billboard", "png");

                if (string.IsNullOrEmpty(path))
                    return;
                

                Texture2D output = CameraTools.GenerateBillboardScreenshot(Selection.activeGameObject, Vector3.forward, 10);
                CameraTools.SaveTexture2D(output, path);
                GameObject.DestroyImmediate(output);
                AssetDatabase.Refresh();

                


                Renderer renderer = target.GetComponent<Renderer>();
                Bounds bounds = renderer.bounds;

                Vector3 size = bounds.size;

                Texture2D image = (Texture2D)AssetDatabase.LoadMainAssetAtPath(path);
                GameObject go = GenerateBillboard(size.x, size.y);
                MeshRenderer mr = go.GetComponent<MeshRenderer>();

                Material mat = new Material(Shader.Find("Foliage"));
                mat.mainTexture = image;

                mr.sharedMaterial = mat; 

            }

        }

        private static string GetSaveFilePath(string title, string defaultName, string extension)
        {
            string path = EditorUtility.SaveFilePanelInProject(title, defaultName, extension, "");

            bool IsValid = true;

            if (string.IsNullOrEmpty(path))
                IsValid = false;

            if (System.IO.Path.GetExtension(path) != "." + extension)
                IsValid = false;

            if (!IsValid)
            {
                EditorUtility.DisplayDialog("Invalid save path", "Im not sure how, but you messed something up. No screenshot will be taken.", "Whatever dude");
                return "";
            }
            return path;
        }



        public static GameObject GenerateBillboard(float width, float height)
        {
            GameObject go = new GameObject();

            // You can change that line to provide another MeshFilter
            MeshFilter filter = go.AddComponent<MeshFilter>();
            go.AddComponent<MeshRenderer>();

            Mesh mesh = new Mesh();
            mesh.Clear();

            //width = height = (width > height) ? width: height;

            
            int resX = 2; // 2 minimum
            int resZ = 2;

            #region Vertices		
            Vector3[] vertices = new Vector3[resX * resZ];
            for (int z = 0; z < resZ; z++)
            {
                // [ -length / 2, length / 2 ]
                float zPos = ((float)z * height);
                for (int x = 0; x < resX; x++)
                {
                    // [ -width / 2, width / 2 ]
                    float xPos = ((float)x / (resX - 1) - .5f) * width;
                    vertices[x + z * resX] = new Vector3(xPos, zPos, 0f);
                }
            }
            #endregion

            #region Normales
            Vector3[] normales = new Vector3[vertices.Length];
            for (int n = 0; n < normales.Length; n++)
                normales[n] = Vector3.back;
            #endregion

            #region UVs		
            Vector2[] uvs = new Vector2[vertices.Length];
            for (int v = 0; v < resZ; v++)
            {
                for (int u = 0; u < resX; u++)
                {
                    uvs[u + v * resX] = new Vector2((float)u / (resX - 1), (float)v / (resZ - 1));
                }
            }
            #endregion

            #region Triangles
            int nbFaces = (resX - 1) * (resZ - 1);
            int[] triangles = new int[nbFaces * 6];
            int t = 0;
            for (int face = 0; face < nbFaces; face++)
            {
                // Retrieve lower left corner from face ind
                int i = face % (resX - 1) + (face / (resZ - 1) * resX);

                triangles[t++] = i + resX;
                triangles[t++] = i + 1;
                triangles[t++] = i;

                triangles[t++] = i + resX;
                triangles[t++] = i + resX + 1;
                triangles[t++] = i + 1;
            }
            #endregion

            mesh.vertices = vertices;
            mesh.normals = normales;
            mesh.uv = uvs;
            mesh.triangles = triangles;

            mesh.RecalculateBounds();
            filter.sharedMesh = mesh;
            return go;
        }


    }
}
