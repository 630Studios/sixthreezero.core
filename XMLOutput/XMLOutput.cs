﻿using System.Collections.Generic;
using System.Text;

public class XMLElementOutput
{
    public string Name = "";
    public List<string> attributeNames = new List<string>();
    public List<System.Object> attributeValues = new List<System.Object>();
    public List<XMLElementOutput> children = new List<XMLElementOutput>();
    public string InnerText = "";

    public XMLElementOutput() 
    {

    }

    public XMLElementOutput(string name, string innerText)
    {
        this.Name = name;
        this.InnerText = innerText;
    }

    public XMLElementOutput CreateChild(string name, string innerText)
    {
        XMLElementOutput newChild = new XMLElementOutput(name, innerText);
        children.Add(newChild);
        return newChild;
    }

    public void addAttribute(string name, System.Object value)
    {
        attributeNames.Add(name);
        attributeValues.Add(value);
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendFormat("<{0} ", XMLEncodeString(Name));
        for (int i = 0; i < attributeNames.Count; i++)
        {
            if (attributeNames[i] == null)
            {
                UnityEngine.Debug.Log("Null attribute name!!!");
                continue;

            }
            if (attributeValues[i] == null)
            {
                //UnityEngine.Debug.Log("Null attribute vale!!! - " + attributeNames[i]);
                continue;

            }
            sb.AppendFormat("{0}=\"{1}\" ", XMLEncodeString(attributeNames[i]), XMLEncodeString(attributeValues[i].ToString()));
        }

        if (InnerText.Length == 0 && children.Count == 0)
        {
            sb.Append(" />");
        }
        else
        {
            sb.Append(" >");
            for (int i = 0; i < children.Count; i++)
            {
                sb.Append(children[i].ToString());
            }
            sb.Append(InnerText);
            sb.AppendFormat("</{0}>", XMLEncodeString(Name));
        }
        return sb.ToString();
    }

    public void EncodeToInnerText(string text)
    {
        InnerText += XMLEncodeString(text);
    }

    public static string XMLEncodeString(string data)
    {
        StringBuilder output = new StringBuilder();


        for (int i = 0; i < data.Length; i++)
        {
            if (data[i] == '&')
            {
                output.Append("&amp;");
                continue;
            }
            if (data[i] == '<')
            {
                output.Append("&lt;");
                continue;
            }
            if (data[i] == '>')
            {
                output.Append("&gt;");
                continue;
            }

            if (data[i] == '"')
            {
                output.Append("&quot;");
                continue;
            }
            if (data[i] == '\'')
            {
                output.Append("&apos;");
                continue;
            }
            output.Append(data[i]);
        }
        return output.ToString();
        //return data.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;");
    }

}