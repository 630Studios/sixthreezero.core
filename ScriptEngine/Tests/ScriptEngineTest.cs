﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SixThreeZero.Scripting;
public class ScriptEngineTest
{

    protected string _Expression;
    protected System.Type _ReturnType;
    protected string _ExpectedResult;

    public string Expression
    {
        get
        {
            return _Expression;
        }

        set
        {
            _Expression = value;
        }
    }

    public Type ReturnType
    {
        get
        {
            return _ReturnType;
        }

        set
        {
            _ReturnType = value;
        }
    }

    public string ExpectedResult
    {
        get
        {
            return _ExpectedResult;
        }

        set
        {
            _ExpectedResult = value;
        }
    }

    public ScriptEngineTest(string expression, System.Type returnType, string expectedResult)
    {
        Expression = expression;
        ReturnType = returnType;
        ExpectedResult = expectedResult;
    }

    

    public virtual bool Run(ScriptEngine engine)
    {
        CompiledScript script = engine.CompileScript(Expression);
        script.Execute();
        
        return false;
    }
    
        
        
        
    

    

}

