﻿
using System.Collections.Generic;

using SixThreeZero.Core.Dynamics;
namespace SixThreeZero.Scripting
{
    public interface ITokenController
    {
        System.Type ValueType(ITokenObject owner);
        ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens);
        System.Object ExecuteAction(ITokenObject owner);
        System.Object Value(ITokenObject owner);
    }
}