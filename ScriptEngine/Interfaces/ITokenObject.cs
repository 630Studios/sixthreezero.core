﻿namespace SixThreeZero.Scripting
{
    public interface ITokenObject
    {
        string Token { get; set; }
        TokenTypes TokenType { get; }
        ITokenObject PreviousToken { get; set; }
        ITokenObject NextToken { get; set; }
        System.Object Value { get; set; }
        System.Type ValueType { get; }
        System.Object Execute();
        ITokenObject ChildToken { get; set; }
    }

}