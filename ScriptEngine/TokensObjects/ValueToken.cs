﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;

namespace SixThreeZero.Scripting
{

    public class ValueToken : TokenObject, ITokenObject
    {


        public override TokenTypes TokenType { get { return TokenTypes.Value; } }
        protected System.Object _Value;
        public override System.Object Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public override System.Type ValueType
        {
            get { return _Value.GetType(); }
        }


        public ValueToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens, System.Object value)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;
            this.Value = value;
            this.nextToken = CreateToken(this, tokens);

        }


        public override System.Object Execute()
        {
            if (this.nextToken != null)
                return this.NextToken.Execute();
            else
                return Value;
        }

    }
}