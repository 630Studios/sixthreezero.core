﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
namespace SixThreeZero.Scripting
{
    public class OperatorToken : TokenObject, ITokenObject
    {

        public override TokenTypes TokenType { get { return TokenTypes.Operator; } }
        protected VariableToken _Results;
        protected ITokenController _Actions;

        public override System.Type ValueType
        {
            get
            {

                if (this.NextToken != null)
                    return this.NextToken.ValueType;
                else
                    return _Actions.ValueType(this);
            }
        }

        public ITokenController Controller
        {
            get { return _Actions; }
            set { _Actions = value; }
        }

        public OperatorToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            this._LanguageSettings = settings;
            _Actions = settings.Operators[tokenValue[0]];
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;

            if (_Actions != null)
                this.nextToken = _Actions.ConstructorAction(this, settings, scope, previous, tokenValue, tokens);
            else
                this.nextToken = CreateToken(this, tokens);
        }

        public override object Execute()
        {
            if (_Actions != null)
                return _Actions.ExecuteAction(this);
            else

                return base.Execute();
        }

        public virtual VariableToken Results
        {
            get { return _Results; }
        }

        public override System.Object Value
        {
            get { return _Actions.Value(this); }
        }

    }
}