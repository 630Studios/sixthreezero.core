﻿using System.Collections.Generic;
using SixThreeZero.Core.Dynamics;
namespace SixThreeZero.Scripting
{
    public class FunctionToken : TokenObject, IParameterToken, ITokenObject
    {

        public override TokenTypes TokenType { get { return TokenTypes.Function; } }
        protected System.Reflection.MethodInfo _Method;
        protected System.Reflection.MethodInfo[] _Methods;
        protected ITokenObject[] _Parameters;
        public override System.Object Value
        {
            get { return GetValue(); }
        }
        public override System.Type ValueType
        {
            get { return _Method.ReturnType; }

        }
        public ITokenObject[] Parameters
        {
            get { return _Parameters; }
            set { _Parameters = value; }
        }
        public FunctionToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;
            this._Method = scope.functions[tokenValue];
            this.nextToken = CreateToken(this, tokens);

            /*after we beuild the structure we need to find the tokens that make up the parameter group, and individual paramters
             * then we need to relink .nextToken to point to the token after the function closing brace
             */
        }

        public override object Execute()
        {
            if (this.nextToken != null)
                return this.NextToken.Execute();
            else
                return Value;

        }

        private System.Object GetValue()
        {

            if (_Parameters == null)
            {
                return this._Method.Invoke(null, null);
            }
            else
            {
                System.Object[] parameterValues = new System.Object[_Parameters.Length];
                for (int i = 0; i < _Parameters.Length; i++)
                    parameterValues[i] = _Parameters[i].Value;

                return this._Method.Invoke(null, parameterValues);
            }

        }


    }
}