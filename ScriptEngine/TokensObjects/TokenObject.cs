﻿using System.Collections.Generic;
using System.Linq;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Scripting
{
    public enum TokenTypes
    {
        Undefined = 0,
        Operator = 1,
        Variable = 2,
        Function = 3,
        Type = 4,
        Value = 5,
        MemberField = 6,
        MemberProperty = 7,
        MemberMethod = 8,
        GroupResult = 9,

    }
    public class TokenObject
    {

        /* PROTECTED FIELDS
         * 
         * 
         * 
         * 
         */

        protected LanguageController _LanguageSettings;
        protected DynamicScope _Scope;
        protected string token;
        protected TokenTypes tokenType;
        protected ITokenObject previousToken;
        protected ITokenObject nextToken;
        protected ITokenObject _ChildToken;


        /* PROPERTIES
         * 
         * 
         * 
         * 
         */

        public string Token
        {
            get
            {
                return token;
            }

            set
            {
                token = value;
            }
        }

        public virtual TokenTypes TokenType
        {
            get
            {
                return TokenType;
            }
        }

        public virtual ITokenObject PreviousToken
        {
            get
            {
                return previousToken;
            }

            set
            {
                previousToken = value;
            }
        }

        public ITokenObject NextToken
        {
            get
            {
                return nextToken;
            }

            set
            {
                nextToken = value;
            }
        }

        public virtual System.Object Value
        {
            get { return null; }
            set { }
        }

        public virtual System.Type ValueType
        {
            
            get {

                if (this.NextToken != null)
                    return this.NextToken.ValueType;
                else
                    return null;
                }
            
            set { }
        }

        public virtual System.Type SelfValueType
        {

            get
            {
                return null;
            }

            set { }
        }



        public ITokenObject ChildToken
        {
            get { return _ChildToken; }
            set { _ChildToken = value; }
        }

        /* EXECUTE
         * 
         * 
         * 
         * 
         */
        public virtual System.Object Execute()
        {
            return null;
        }



        /* INSTANCE CREATE TOKEN SHORTCUTS
         * 
         * 
         * 
         * 
         */
        protected virtual ITokenObject CreateToken(ITokenObject lastToken, List<string> tokenList)
        {
            return CreateToken(_LanguageSettings, _Scope, lastToken, tokenList);
        }
        protected virtual ITokenObject CreateToken(ITokenObject lastToken, List<string> tokenList, bool isMember)
        {
            return CreateToken(_LanguageSettings, _Scope, lastToken, tokenList, isMember);
        }






        /* STATIC CREATE TOKEN METHOD
         * 
         * 
         * 
         * 
         */
        public static ITokenObject CreateToken(LanguageController settings, DynamicScope globalScope, ITokenObject lastToken, List<string> tokenList, bool isMember = false)
        {
            if (tokenList == null || tokenList.Count == 0)
            {
                //UnityEngine.Debug.Log("null or empty token list, end of list?");
                return null;
            }


            string token = tokenList[0];


            if (token.Length > 1)
                token = token.Trim();

            tokenList.RemoveAt(0);
            ITokenObject newTokenObject = null;

            /*check and see if the token is an operator*/
            if (token.Length == 1 && settings.Operators.ContainsKey(token[0]) == true)
            {
                newTokenObject = new OperatorToken(settings, globalScope, lastToken, token, tokenList);
            }
            else
            {
                /*check and see if this is supposed to be a member token
                 * member tokens access either static or instance funtions,variables on a class/type
                 */
                if (isMember)
                {
                    /*validate this can be a member token*/
                    if (lastToken == null)
                        throw new System.Exception("CreateToken: can not create member token with no parent.");
                    if (lastToken.PreviousToken == null)
                        throw new System.Exception("CreateToken: can not create member token with no grandparent.");


                    ITokenObject parentToken = lastToken.PreviousToken;
                    if ( parentToken.TokenType == TokenTypes.Operator)
                    {
                        parentToken = parentToken.PreviousToken;
                    }

                    if (parentToken == null)
                        throw new System.Exception("CreateToken: parentToken is null.");

                    if (parentToken.TokenType == TokenTypes.Undefined)
                        throw new System.Exception("CreateToken: can not create member token to a Operator or Undefined Token.");

                    System.Type parentValueType = parentToken.ValueType;

                    if (parentValueType == null)
                        throw new System.Exception("CreateToken: can not create member token: parentToken value type is null :" + parentToken.Token);

                    MemberAccessType accessType = MemberAccessType.Instance;

                    if (parentToken.TokenType == TokenTypes.Type)
                        accessType = MemberAccessType.Static;

                    /* Member Field
                     *
                     */
                    System.Reflection.FieldInfo field = parentValueType.GetField(token);
                    if (field != null) /*found a matching field*/
                    {
                        //UnityEngine.Debug.Log("Member Field: " + token);

                        if (!field.IsPublic)
                            throw new System.Exception("TokenObject: CreateToken: MemberFieldToken: You may not target non private fields.");

                        if (accessType == MemberAccessType.Static && !field.IsStatic)
                            throw new System.Exception("TokenObject: CreateToken: MemberFieldToken: Attempt to access instance member on static reference.");

                        newTokenObject = new MemberFieldToken(settings, globalScope, lastToken, token, tokenList, parentToken, accessType, field);
                    }
                    else
                    {
                        /* Member Property
                         *
                         */
                        System.Reflection.PropertyInfo property = parentValueType.GetProperty(token);
                        if (property != null) /*found a matching property*/
                        {
                            //UnityEngine.Debug.Log("Member Property: " + token);

                            if (!property.CanRead && !property.CanWrite)
                                throw new System.Exception("TokenObject: CreateToken: MemberPropertyToken: You may not target properties you can not read or write to.");

                            System.Reflection.MethodInfo getMethod = null;
                            System.Reflection.MethodInfo setMethod = null;
                            if (property.CanRead)
                                getMethod = property.GetGetMethod();

                            if (property.CanWrite)
                                setMethod = property.GetSetMethod();

                            if (getMethod == null && setMethod == null)
                                throw new System.Exception("TokenObject: CreateToken: MemberPropertyToken: No Get or Set Method:" + token);

                            if (!getMethod.IsPublic && !setMethod.IsPublic)
                                throw new System.Exception("TokenObject: CreateToken: MemberPropertyToken: You may not target properties with no public get or set accessor.");


                            
                            if (accessType == MemberAccessType.Static)
                            {
                                if ((getMethod == null && !getMethod.IsStatic) && (getMethod == null && !getMethod.IsStatic))
                                    throw new System.Exception("TokenObject: CreateToken: MemberPropertyToken: Attempt to access instance property member on static reference.");
                            }

                            newTokenObject = new MemberPropertyToken(settings, globalScope, lastToken, token, tokenList, parentToken, accessType, property);


                        }
                        else
                        {
                            /* Member Method
                             * 
                             */
                            System.Reflection.MethodInfo[] methods = parentValueType.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.FlattenHierarchy).Where(m => m.Name == token).ToArray();
                            if (methods != null && methods.Length > 0) /*found a matching method*/
                            {
                                //UnityEngine.Debug.Log("Member method: " + token);
                                newTokenObject = new MemberMethodToken(settings, globalScope, lastToken, token, tokenList, parentToken, accessType, methods);
                            }
                            else
                            {
                                /*no match*/
                                throw new System.Exception("CreateToken: Invalid Member Token: " + token);
                            }
                        }
                    }
                }
                else
                {
                    if (globalScope.variables.ContainsKey(token))
                    {
                        //UnityEngine.Debug.Log("New Variable Token: " + token);
                        newTokenObject = new VariableToken(settings, globalScope, lastToken, token, tokenList);
                    }
                    else if (globalScope.functions.ContainsKey(token))
                    {
                        //UnityEngine.Debug.Log("New Function Token: " + token);
                        newTokenObject = new FunctionToken(settings, globalScope, lastToken, token, tokenList);

                    }
                    else if (globalScope.RegisteredTypes.ContainsKey(token))
                    {
                        //UnityEngine.Debug.Log("New Type Token: " + token);
                        newTokenObject = new TypeToken(settings, globalScope, lastToken, token, tokenList);

                    }
                    else
                    {
                        /*Values - numers, strings, etc*/
                        int outVal;
                        if (int.TryParse(token, out outVal))
                        {

                            /*We have a numeric token, so now we need to make sure we grab
                             * all the parts of the number since the parser will have split any decimals
                             */
                            if (tokenList != null && tokenList.Count > 0 && tokenList[0] == ".")
                            {
                                /*Next token is a decimal, so this is a split number lets grab more;*/
                                if (tokenList.Count > 1)
                                {
                                    string nextNextToken = tokenList[1];
                                    /*lets see if its a number, should check for prescence of 'f' character at end to denote the float type*/
                                    int secondOutVal;
                                    if (int.TryParse(token, out secondOutVal))
                                    {
                                        /*good it was a number, lets make them all one token*/
                                        token += tokenList[0] + tokenList[1];
                                        tokenList.RemoveRange(0, 2); //remove the next two tokens because we ate them.

                                        /*token is now in the format of int.int which SHOULD be a valid float. so lets check that.*/
                                        float outFloat;

                                        if (float.TryParse(token, out outFloat))
                                        {
                                            /*success we have a float*/
                                            //UnityEngine.Debug.Log("New Value Token (float): " + token);
                                            newTokenObject = new ValueToken(settings, globalScope, lastToken, token, tokenList, outFloat);
                                        }
                                        else
                                        {
                                            throw new System.Exception("Invalid Token: Numeric token with decimal detected and trailing number but couldnt convert to float" + token);
                                            /*not a float... */
                                        }

                                    }
                                    else
                                    {
                                        /*uh oh wasnt a token, this is a problem as we have 'number.' in the chain*/
                                        throw new System.Exception("Invalid Token: Numeric token with decimal detected but trailing token is not numeric" + token);
                                    }
                                }
                                else
                                {
                                    throw new System.Exception("Invalid Token: Numeric token with decimal detected but no trailing numbers" + token);
                                    /*no more tokens??? uh oh*/
                                }
                            }
                            else
                            {
                                /*no decimals. we are dealing with an integer*/
                                //UnityEngine.Debug.Log("New Value Token (int): " + token);
                                newTokenObject = new ValueToken(settings, globalScope, lastToken, token, tokenList, outVal);
                            }



                        }
                        else
                        {
                            throw new System.Exception("Invalid Token: " + token);
                        }


                    }
                }
            }


            return newTokenObject;
        }


    }
}