﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
using System.Linq;


namespace SixThreeZero.Scripting
{


    public class MemberMethodToken : TokenObject, IParameterToken, ITokenObject
    {

        public override TokenTypes TokenType { get { return TokenTypes.MemberMethod; } }

        protected ITokenObject _ParentToken;
        protected MemberAccessType _AccessType;
        protected System.Reflection.MethodInfo _Method;
        protected System.Reflection.MethodInfo[] _Methods;
        protected ITokenObject[] _Parameters;

        public MemberAccessType MemberType { get { return _AccessType; } }
        public System.Reflection.MethodInfo Method { get { return _Method; } }
        public ITokenObject[] Parameters
        {
            get { return _Parameters; }
            set
            {
                _Parameters = value;
                updateMethodLink();
            }
        }
        public override System.Object Value { get { return GetValue(); } }
        public override System.Type ValueType {
                get {
                    if (this.NextToken != null)
                        return this.NextToken.ValueType;
                    else
                        return _Method.ReturnType;
                }
        }

        public MemberMethodToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens, ITokenObject parentToken, MemberAccessType accessType, System.Reflection.MethodInfo[] methods)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;
            this._ParentToken = parentToken;
            this._AccessType = accessType;
            this._Methods = methods;

            CreateToken(this, tokens, true);

        }

        public override object Execute()
        {
            if (this.nextToken != null)
                return this.NextToken.Execute();
            else
                return Value;

        }

        private System.Object GetValue()
        {
            if (this._AccessType == MemberAccessType.Static)
            {
                if (_Parameters == null)
                {
                    return this._Method.Invoke(null, null);
                }
                else
                {
                    System.Object[] parameterValues = new System.Object[_Parameters.Length];
                    for (int i = 0; i < _Parameters.Length; i++)
                        parameterValues[i] = _Parameters[i].Execute();

                    return this._Method.Invoke(null, parameterValues);
                }
            }
            else
            {
                if (_Parameters == null)
                {
                    return this._Method.Invoke(this._ParentToken.Value, null);
                }
                else
                {
                    System.Object[] parameterValues = new System.Object[_Parameters.Length];
                    for (int i = 0; i < _Parameters.Length; i++)
                        parameterValues[i] = _Parameters[i].Execute();

                    return this._Method.Invoke(this._ParentToken.Value, parameterValues);
                }
            }
        }


        public void updateMethodLink()
        {

            if (_Parameters == null)
            {
                for (int i = 0; i < _Methods.Length; i++)
                {
                    System.Reflection.ParameterInfo[] methodParams = _Methods[i].GetParameters();
                    if (methodParams.Length != 0)
                        continue;

                    _Method = _Methods[i];
                    return;
                }
                throw new System.Exception("No mathching parameterless methods.");

            }
            else
            {


                System.Type[] parameterTypes = _Parameters.Select(p => p.ValueType).ToArray();

                for (int i = 0; i < _Methods.Length; i++)
                {
                    if (_Methods[i].IsStatic == false && _AccessType == MemberAccessType.Static)
                        continue;

                    System.Reflection.ParameterInfo[] methodParams = _Methods[i].GetParameters();



                    if (methodParams.Length != parameterTypes.Length)
                        continue;

                    bool isMatch = true;

                    for (int n = 0; n < methodParams.Length; n++)
                    {
                        if (methodParams[n].ParameterType != parameterTypes[n])
                        {
                            isMatch = false;
                            break;
                        }
                    }

                    if (isMatch)
                    {
                        _Method = _Methods[i];
                        break;
                    }

                }

                if (_Method == null)
                    throw new System.Exception(string.Format("No matching method with params: {0} - {1}", token, string.Join(",", parameterTypes.Select(p => p.Name).ToArray())));
            }
        }
    }

}