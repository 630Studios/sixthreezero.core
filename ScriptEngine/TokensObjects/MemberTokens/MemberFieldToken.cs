﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;



namespace SixThreeZero.Scripting
{

    public class MemberFieldToken : TokenObject, ITokenObject
    {

        public override TokenTypes TokenType { get { return TokenTypes.MemberField; } }

        protected ITokenObject _ParentToken;
        protected MemberAccessType _AccessType;
        protected System.Reflection.FieldInfo _Field;


        public MemberAccessType MemberType { get { return _AccessType; } }
        public System.Reflection.FieldInfo Field { get { return _Field; } }
        public override System.Object Value
        {
            get
            {
                if (this._AccessType == MemberAccessType.Static)
                    return this._Field.GetValue(null);
                else
                    return this._Field.GetValue(this._ParentToken.Value);

            }
        }


        public override System.Type ValueType {
            get
            {
                if (this.nextToken == null)
                    return this.nextToken.ValueType;
                else
                    return _Field.FieldType;
            }
        }

        public MemberFieldToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens, ITokenObject parentToken, MemberAccessType accessType, System.Reflection.FieldInfo field)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;
            this._ParentToken = parentToken;
            this._AccessType = accessType;
            this._Field = field;

            this.nextToken = CreateToken(this, tokens, true);

            /*after we beuild the structure we need to find the tokens that make up the parameter group, and individual paramters
             * then we need to relink .nextToken to point to the token after the function closing brace
             */
        }

        public override object Execute()
        {
            if (this.nextToken != null)
            {
                return this.NextToken.Execute();
            }
            else
            {
                if (this._AccessType == MemberAccessType.Static)
                    return this._Field.GetValue(null);
                else
                    return this._Field.GetValue(this._ParentToken.Value);
            }

        }
    }

}