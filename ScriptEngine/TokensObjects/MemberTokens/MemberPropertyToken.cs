﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;

namespace SixThreeZero.Scripting
{



    public class MemberPropertyToken : TokenObject, ITokenObject
    {

        public override TokenTypes TokenType { get { return TokenTypes.MemberProperty; } }

        protected ITokenObject _ParentToken;
        protected MemberAccessType _AccessType;
        protected System.Reflection.PropertyInfo _Property;


        public MemberAccessType MemberType { get { return _AccessType; } }
        public System.Reflection.PropertyInfo Property { get { return _Property; } }
        public override System.Object Value
        {
            get
            {
                if (this._AccessType == MemberAccessType.Static)
                    return this._Property.GetValue(null, null);
                else
                    return this._Property.GetValue(this._ParentToken.Value, null);
            }
        }


        public override System.Type ValueType {
            get {
                if (this.NextToken != null)
                    return this.nextToken.ValueType;
                else
                    return _Property.PropertyType;
            }

        }

        public MemberPropertyToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens, ITokenObject parentToken, MemberAccessType accessType, System.Reflection.PropertyInfo property)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;
            this._ParentToken = parentToken;
            this._AccessType = accessType;
            this._Property = property;

            this.nextToken = CreateToken(this, tokens, true);
            /*if (this.nextToken.Token != "(")
                throw new System.Exception("MemberMethodToken: Can not have method token without following '(' and ')'");

            ITokenObject groupOpen = this.nextToken;
            ITokenObject groupClose = null;

            ITokenObject currentObj = groupOpen;

            while (currentObj != null)
            {
                if (currentObj.Token == ")")
                {
                    groupClose = currentObj;
                    break;
                }
                currentObj = currentObj.NextToken;
            }

            if (groupClose == null)
                throw new System.Exception("MemberMethodToken: Can not have method call without open and close group symbols '(' & ')'");*/
            /*after we beuild the structure we need to find the tokens that make up the parameter group, and individual paramters
             * then we need to relink .nextToken to point to the token after the function closing brace
             */
        }

        public override object Execute()
        {
            if (this.nextToken != null)
            {
                return this.NextToken.Execute();
            }
            else
            {
                if (this._AccessType == MemberAccessType.Static)
                {

                    return this._Property.GetValue(null, null);

                }
                else
                {
                    return this._Property.GetValue(this._ParentToken.Value, null);
                }
            }

        }
    }

}