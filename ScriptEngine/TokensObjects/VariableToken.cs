﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
namespace SixThreeZero.Scripting
{

    public class VariableToken : TokenObject, ITokenObject
    {


        public override TokenTypes TokenType { get { return TokenTypes.Variable; } }
        public override System.Object Value
        {
            get { return _Scope.variables[token].Value; }
            set { _Scope.variables[token].Value = value; }
        }
        public override System.Type ValueType
        {
            get {
                if (this.NextToken != null)
                    return this.NextToken.ValueType;
                else
                    return _Scope.variables[token].Type;
            }

        }


        public VariableToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;

            this.nextToken = CreateToken(this, tokens);

        }


        public override System.Object Execute()
        {
            if (this.nextToken != null)
                return this.NextToken.Execute();
            else
                return Value;
        }

    }
}