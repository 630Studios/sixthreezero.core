﻿using System.Collections.Generic;
using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Scripting
{

    public class TypeToken : TokenObject, ITokenObject
    {
        public override TokenTypes TokenType { get { return TokenTypes.Type; } }

        protected System.Type _TargetType;


        public override System.Type ValueType
        {
            get {

                if (this.NextToken != null)
                    return this.NextToken.ValueType;
                else
                    return _TargetType;
            }
        }


        public TypeToken(LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            this._LanguageSettings = settings;
            this._Scope = scope;
            this.previousToken = previous;
            this.token = tokenValue;
            this._TargetType = scope.RegisteredTypes[tokenValue];

            this.nextToken = CreateToken(this, tokens);

            
        }

        public override object Execute()
        {
            if (this.nextToken != null)
                return this.NextToken.Execute();
            else
                throw new System.Exception("Type reference with nothing?");
        }

        public System.Type TargetType { get { return _TargetType; } }
        
        public override System.Object Value
        {
            get { return _TargetType; }
        }
    }
}