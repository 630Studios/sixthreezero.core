﻿
using System.Collections.Generic;


namespace SixThreeZero.Scripting
{

    public class LanguageController: ILanguageController
    {


        public Dictionary<char, ITokenController> _Operators = new Dictionary<char, ITokenController>();
        public Dictionary<string, IKeywordController> _Keywords = new Dictionary<string, IKeywordController>();

        public Dictionary<char, ITokenController> Operators
        {
            get { return _Operators; }
            set { _Operators = value; }
        }

        public Dictionary<string, IKeywordController> Keywords
        {
            get { return _Keywords; }
            set { _Keywords = value; }
        }

        public void LoadDeafults()
        {

            Operators['.'] = new PeriodController();
            Operators['='] = new EqualsController();

            Operators['('] = new GroupOpenController();
            Operators[')'] = new GroupCloseController();

            Operators['['] = new BracketOpenController();
            Operators[']'] = new BracketCloseController();

            Operators['"'] = new StringController();

            Operators[','] = new OperatorController(); // no controller needed, gets eaten butother op controllers, just needs to exist to split thigns up

            

            Operators['+'] = new AdditionController();
            Operators['-'] = new SubtractionController();
            Operators['*'] = new MultiplicationController();
            Operators['/'] = new DivisionController();

            Operators[':'] = new OperatorController();
            Operators['!'] = new OperatorController();
            Operators['<'] = new OperatorController();
            Operators['>'] = new OperatorController();
            Operators[';'] = new OperatorController();


            /*KEYWORDS*/
            Keywords["new"] = new KeywordController();
            
        }


    }

}