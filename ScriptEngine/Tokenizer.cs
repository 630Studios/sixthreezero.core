﻿
using System.Collections.Generic;

namespace SixThreeZero.Scripting
{
    public class Tokenizer
    {

        private LanguageController _Settings;

        public LanguageController Settings { get { return _Settings; } }

        public Tokenizer(LanguageController settings)
        {
            _Settings = settings;
        }
        /*
         * tokenizes a string. Format for return list is keyword, operator, keyword, operator
         */
        public List<string> tokenizeString(string str)
        {
            List<string> tokens = new List<string>();

            str = str.Trim();

            int startOfToken = 0;


            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                if (isOperator(_Settings, str[i]))
                {
                    /*back to back operators*/
                    if (startOfToken == i)
                    {
                        tokens.Add(char.ToString(c));
                        startOfToken = i + 1;
                    }
                    else
                    {
                        string token = getToken(str, startOfToken, i);
                        tokens.Add(token);
                        tokens.Add(char.ToString(c));
                        startOfToken = i + 1;
                    }
                }
            }

            if (startOfToken < str.Length)
            {
                string token = getToken(str, startOfToken, str.Length);
                tokens.Add(token);
            }
            return tokens;
        }



        private static bool isOperator(LanguageController settings, char c)
        {
            if (settings.Operators.ContainsKey(c))
                return true;

            //UnityEngine.Debug.Log("Invalid Operator: " + c);
            return false;
        }

        private static string getToken(string str, int start, int end)
        {
            if (end <= start)
            {
                throw new System.Exception("getToken: 0 length token request");
            }

            if ((start >= str.Length) || (end > str.Length))
            {
                throw new System.Exception("getToken: invalid start or end position");
            }

            return str.Substring(start, end - start);

        }

    }
}

