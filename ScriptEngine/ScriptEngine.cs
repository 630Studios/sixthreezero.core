﻿using SixThreeZero.Core.Dynamics;

using System.Collections.Generic;


namespace SixThreeZero.Scripting
{




    public enum MemberAccessType
    {
        Static = 0,
        Instance = 1,
    }



    public class ScriptEngine
    {
        private DynamicScope _Scope;
        private Tokenizer _Tokenizer;
        private LanguageController _Settings;

        public DynamicScope Scope
        {
            get { return _Scope; }
        }

        public LanguageController Settings
        {
            get { return _Settings; }
        }

        public ScriptEngine()
        {
            _Scope = new DynamicScope();
            _Settings = new LanguageController();
            _Tokenizer = new Tokenizer(_Settings);
        }

        public ScriptEngine(DynamicScope scope, LanguageController settings)
        {
            _Scope = scope;
            _Tokenizer = new Tokenizer(settings);

        }

        public CompiledScript CompileScript(string script)
        {
            List<string> tokens = _Tokenizer.tokenizeString(script);
            ITokenObject rootToken = TokenObject.CreateToken(_Settings, _Scope, null, tokens);

            CompiledScript compiledScript = new CompiledScript(_Scope, script, rootToken);
            return compiledScript;

        }
#if UNITY_EDITOR
        [UnityEditor.MenuItem("Window/Scriptengine Test")]
        public static void TestMeMenu()
        {
            ScriptEngineTest[] tests = new ScriptEngineTest[]
            {

                new ScriptEngineTest("1+1", typeof(int), "2"),
                new ScriptEngineTest("2*4", typeof(int), "8"),
                new ScriptEngineTest("4/2", typeof(int), "2"),
                new ScriptEngineTest("4-1", typeof(int), "3"),
                new ScriptEngineTest("(Mathf.Max(0.0,1.0)*5.5)+(20.0/4.0)", typeof(float), "10,5"),
            };
            int length = 6;
            string test = "yup";
            ScriptEngine se = new ScriptEngine();
            se.Settings.LoadDeafults();
            se.Scope.RegisterType(typeof(UnityEngine.Debug));
            se.Scope.variables["testVarLength"] = new DynamicVariable("testVarLength", typeof(int), ()=>length, (l) => length = (int)l);
            se.Scope.RegisterType(typeof(UnityEngine.Mathf));
            se.Scope.variables["testVar2"] = new DynamicVariable("testVar2", typeof(string), ()=>test, (t)=>t.ToString());


            System.Text.StringBuilder output = new System.Text.StringBuilder();
            for (int i = 0; i < tests.Length; i++)
            {
                CompiledScript cs = se.CompileScript(tests[i].Expression);

                /*
                    ITokenObject croot = cs.Root;
                    while (croot != null)
                    {
                        UnityEngine.Debug.Log("Token - " + croot.Token);
                        croot = croot.NextToken;
                    }
                */
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                int n = 0;
                UnityEngine.Profiling.Profiler.BeginSample("script engine " + i);
                while(n++ < 10000 )
                {
                    cs.Execute();
                }
                UnityEngine.Profiling.Profiler.EndSample();
                sw.Stop();
                UnityEngine.Debug.Log("time: " + sw.ElapsedMilliseconds);

                /*if (result.GetType() != tests[i].ReturnType)
                    output.AppendLine("[FAILED]" + tests[i].Expression);
                    

                if (result.ToString() != tests[i].ExpectedResult)
                    output.AppendLine("[FAILED]" + tests[i].Expression);*/

                output.AppendLine("[Success]" + tests[i].Expression);
                
            }

            UnityEngine.Debug.Log(output.ToString());
            

        }


#endif
    }


}

