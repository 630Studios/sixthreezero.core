﻿using SixThreeZero.Core.Dynamics;

namespace SixThreeZero.Scripting
{
    public class CompiledScript
    {
        protected DynamicScope _Scope;
        protected string _Source;
        protected ITokenObject _Root;

        public CompiledScript(DynamicScope scope, string source, ITokenObject root)
        {
            _Scope = scope;
            _Source = source;
            _Root = root;
        }

        public ITokenObject Root
        {
            get
            {
                return _Root;
            }

            set
            {
                _Root = value;
            }
        }

        public object Execute()
        {
            return _Root.Execute();
        }

    }
}