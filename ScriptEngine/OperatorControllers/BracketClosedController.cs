﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
/*
 * Group Controller 
 * Activated by '(' char
 * is use it f(), (T), and math operations
 * 
 */

namespace SixThreeZero.Scripting
{
    public class BracketCloseController : OperatorController, ITokenController
    {
        protected ITokenObject groupOpenObj = null;

        public override ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            if (owner.PreviousToken == null)
                throw new System.Exception("GroupCloseController: can not have group operator without previous token.");

            if (owner.PreviousToken.TokenType == TokenTypes.Undefined)
                throw new System.Exception("GroupCloseController: can not have group operator after undefined token type.");

            return null;// TokenObject.CreateToken(settings, scope, owner, tokens);

        }
        public override System.Object ExecuteAction(ITokenObject owner)
        {

            if (owner.NextToken == null)
                throw new System.Exception("GroupCloseController: This shouldnt happen.");

            return owner.NextToken.Execute();
        }

        public override object Value(ITokenObject owner)
        {
            return null;
        }
    }
}