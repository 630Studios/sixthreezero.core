﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
namespace SixThreeZero.Scripting
{
    public class OperatorController : ITokenController
    {

        public virtual System.Type ValueType(ITokenObject owner)
        {


            if (owner.NextToken != null)
            {
                return owner.NextToken.ValueType;
            }
            else
            {
                if (owner.ChildToken != null)
                {
                    return owner.ChildToken.ValueType;
                }
                else
                {
                    return null;
                }
            }
            
        }

        public virtual ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            return TokenObject.CreateToken(settings, scope, owner, tokens);
        }

        public virtual System.Object ExecuteAction(ITokenObject owner)
        {
            return null;
        }

        public virtual System.Object Value(ITokenObject owner)
        {
            return null; 
        }

    }

}