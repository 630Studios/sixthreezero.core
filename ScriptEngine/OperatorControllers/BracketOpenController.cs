﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
using System.Linq;
/*
 * Group Controller 
 * Activated by '(' char
 * is use it f(), (T), and math operations
 * 
 */
namespace SixThreeZero.Scripting
{
    public class BracketOpenController : OperatorController, ITokenController
    {



        public override ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {

            

            if (owner.PreviousToken != null)
            {

                switch (owner.PreviousToken.TokenType)
                {
                    case TokenTypes.Undefined:
                    case TokenTypes.Type:
                    case TokenTypes.Variable:
                    case TokenTypes.MemberField:
                    case TokenTypes.MemberProperty:
                    case TokenTypes.GroupResult:
                    case TokenTypes.Value:
                    case TokenTypes.Function:
                    case TokenTypes.MemberMethod:
                    case TokenTypes.Operator:
                        break;
                    default:
                        throw new System.Exception("BracketOpenController: you shouldnt be here!.");

                }

            }
            else
            {
                throw new System.Exception("BracketOpenController: can not have bracket open token with no previous tokens");
            }

            /* Generate the rest of the subsequent tokens */
            ITokenObject startToken = TokenObject.CreateToken(settings, scope, owner, tokens);

            ITokenObject tempObj = startToken;
            ITokenObject closeToken = null;
            //ITokenObject nextToken;

            List<ITokenObject> containsTokens = new List<ITokenObject>();

            int childToken = 0;
            /* look for the matching close group symbol ')' building a list of tokens in between.
             * this works because as each new open group is found it consumes its close group as well
             */

            while (tempObj != null)
            {
                if (tempObj.Token == "]")
                {
                    closeToken = tempObj;
                    break;
                }
                else
                {
                    containsTokens.Add(tempObj);
                }
                tempObj = tempObj.NextToken;
                childToken++;
            }

            /* We did not find a closing ')' symbol. This is an issue. We shall cry about it.*/
            if (closeToken == null)
                throw new System.Exception("BracketOpenController: Group Open found, no matching group close.");


            
                owner.PreviousToken.NextToken = null;

                owner.NextToken = null;

                closeToken.NextToken = null;
                closeToken.PreviousToken.NextToken = null;
                closeToken.PreviousToken = null;

                startToken.PreviousToken = null;


                ITokenObject ctoken = startToken;
                List<ITokenObject> parameterHeads = new List<ITokenObject>();
                parameterHeads.Add(ctoken);
                ctoken = ctoken.NextToken;
                while (ctoken != null)
                {
                    if (ctoken.Token == ",")
                    {
                        ctoken.PreviousToken.NextToken = null;
                        ctoken.NextToken.PreviousToken = null;
                        parameterHeads.Add(ctoken.NextToken);
                    }
                    ctoken = ctoken.NextToken;
                }

                switch (owner.PreviousToken.TokenType)
                {
                    case TokenTypes.MemberMethod:
                        MemberMethodToken mmt;
                        switch (childToken)
                        {
                            case 0:
                                mmt = (MemberMethodToken)owner.PreviousToken;
                                mmt.Parameters = null;
                                break;

                            case 1:
                                mmt = (MemberMethodToken)owner.PreviousToken;
                                mmt.Parameters = parameterHeads.ToArray();
                                break;

                            default:
                                mmt = (MemberMethodToken)owner.PreviousToken;
                                mmt.Parameters = parameterHeads.Where(p => p.Token != ",").ToArray();
                                break;
                        }

                        break;
                    default:

                        break;
                }

                owner.PreviousToken.NextToken = TokenObject.CreateToken(settings, scope, owner.PreviousToken, tokens);
                return null;
        }


        public override System.Object ExecuteAction(ITokenObject owner)
        {
            /*System.Object returnObj = 
            owner.PreviousToken.Value = owner.NextToken.Value;*/
            if (owner.NextToken == null)
            {
                if (owner.ChildToken == null)
                    throw new System.Exception("BracketOpenController: can not have group open operator with no tokens after it.");
                else
                    return owner.ChildToken.Execute();
            }
            else
            {
                return owner.NextToken.Execute(); ;
            }
        }

        public override object Value(ITokenObject owner)
        {
            if (owner.ChildToken == null)
                throw new System.Exception("BracketOpenController: can not have group open operator with no tokens after it.");
            else
                return owner.ChildToken.Execute();

        }
    }
}