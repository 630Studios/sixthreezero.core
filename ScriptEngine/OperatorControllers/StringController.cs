﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
using System.Linq;
/*
 * Group Controller 
 * Activated by '(' char
 * is use it f(), (T), and math operations
 * 
 */
namespace SixThreeZero.Scripting
{
    public class StringController : OperatorController, ITokenController
    {

        protected string _StrValue;


        public override System.Type ValueType(ITokenObject owner)
        {
            return typeof(string);
        }

        public override ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {

            

            if (owner.PreviousToken != null)
            {

                switch (owner.PreviousToken.TokenType)
                {
                    case TokenTypes.Undefined:
                    case TokenTypes.Type:
                    case TokenTypes.Variable:
                    case TokenTypes.MemberField:
                    case TokenTypes.MemberProperty:
                    case TokenTypes.GroupResult:
                    case TokenTypes.Value:
                    case TokenTypes.Function:
                    case TokenTypes.MemberMethod:
                    case TokenTypes.Operator:
                    default:
                        break;

                }

            }

            int endIndex = -1;
            for (int i = 0; i < tokens.Count; i++)
            {
                if (tokens[i] == "\"")
                {
                    endIndex = i;
                    break;
                }
            }

            if (endIndex == -1)
            {
                throw new System.Exception("String open with no string close");
            }else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                for (int i =0; i < endIndex; i++)
                {
                    sb.Append(tokens[i]);
                }
                tokens.RemoveRange(0, endIndex + 1);
                _StrValue = sb.ToString();
                
            }


            return TokenObject.CreateToken(settings, scope, owner, tokens);


        }


        public override System.Object ExecuteAction(ITokenObject owner)
        {
            /*System.Object returnObj = 
            owner.PreviousToken.Value = owner.NextToken.Value;*/
            if (owner.NextToken == null)
            {
                return _StrValue;
            }
            else
            {
                return owner.NextToken.Execute(); ;
            }
        }

        public override object Value(ITokenObject owner)
        {

            if (owner.NextToken == null)
            {
                return _StrValue;
            }
            else
            {
                return owner.NextToken.Execute(); ;
            }
        }

        
    }
}