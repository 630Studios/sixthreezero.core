﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;

namespace SixThreeZero.Scripting
{
    public class EqualsController : OperatorController, ITokenController
    {
        public override ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            if (owner.PreviousToken == null)
                throw new System.Exception("EqualsController: can not have equals operator without previous token.");

            if (owner.PreviousToken.TokenType == TokenTypes.Undefined)
                throw new System.Exception("EqualsController: can not have equals operator after undefined token type.");

            if (owner.PreviousToken.TokenType == TokenTypes.Operator)
                throw new System.Exception("EqualsController: can not have equals operator after previous operator token type. Previous operator should have consumer this?");

            if (owner.PreviousToken.TokenType == TokenTypes.Type)
                throw new System.Exception("EqualsController: can not have equals operator after type token.");

            if (owner.PreviousToken.TokenType == TokenTypes.MemberMethod)
                throw new System.Exception("EqualsController: can not have equals operator after member method token.");

            if (owner.PreviousToken.TokenType == TokenTypes.Function)
                throw new System.Exception("EqualsController: can not have equals operator after member function token.");

            return TokenObject.CreateToken(settings, scope, owner, tokens);

        }
        public override System.Object ExecuteAction(ITokenObject owner)
        {
            /*System.Object returnObj = 
            owner.PreviousToken.Value = owner.NextToken.Value;*/
            if (owner.NextToken == null)
                throw new System.Exception("EqualsController: can not have equals operator with no tokens after it.");

            System.Object val = owner.NextToken.Execute(); 
            owner.PreviousToken.Value = val;
            return val;
        }
    }
}