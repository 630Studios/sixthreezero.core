﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
namespace SixThreeZero.Scripting
{

    public class PeriodController : OperatorController, ITokenController
    {
        public override ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            if (owner.PreviousToken == null)
                throw new System.Exception("PeriodController: can not have member access token without previous token.");

            if (owner.PreviousToken.TokenType == TokenTypes.Undefined)
                throw new System.Exception("PeriodController: can not have member access token previous undefined token type.");


            return TokenObject.CreateToken(settings, scope, owner, tokens, true);

        }

        public override System.Object ExecuteAction(ITokenObject owner)
        {
            if (owner.NextToken != null)
                return owner.NextToken.Execute();
            else
                throw new System.Exception("wtf?");
        }
    }
}