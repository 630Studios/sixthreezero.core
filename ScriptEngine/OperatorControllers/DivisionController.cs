﻿using SixThreeZero.Core.Dynamics;
using System.Collections.Generic;
using System.Reflection;
using SixThreeZero.Core.Extensions;

namespace SixThreeZero.Scripting
{

    public class DivisionController : OperatorController, ITokenController
    {
        public override ITokenObject ConstructorAction(OperatorToken owner, LanguageController settings, DynamicScope scope, ITokenObject previous, string tokenValue, List<string> tokens)
        {
            if (owner.PreviousToken == null)
                throw new System.Exception("SubtractionController: can not have member access token without previous token.");

            if (owner.PreviousToken.TokenType == TokenTypes.Undefined)
                throw new System.Exception("SubtractionController: can not have member access token previous undefined token type.");


            return TokenObject.CreateToken(settings, scope, owner, tokens, false);

        }

        public override System.Object ExecuteAction(ITokenObject owner)
        {

            if (owner.NextToken != null)
            {
                System.Object valA = owner.PreviousToken.Value;
                System.Object valB = owner.NextToken.Execute();


                System.Type valAType = valA.GetType();
                System.Type valBType = valB.GetType();



                if (valAType.IsNumeric())
                {
                    double valueA = (double)System.Convert.ChangeType(valA, typeof(double));
                    double valueB = (double)System.Convert.ChangeType(valB, typeof(double));
                    double outcome = valueA / valueB;

                    return System.Convert.ChangeType(outcome, valAType);
                }
                else
                {
                
                        MethodInfo divisionOp = valAType.GetDivisionOp(valBType);

                        if (divisionOp == null)
                            throw new System.Exception("no division-op.");
                        else
                            return divisionOp.Invoke(null, new object[] { valA, valB });

                        
                }

            }
            else
                throw new System.Exception("Addition operator with no token after it? comeon.");
        }
    }


}