﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SixThreeZero.Core.Dynamics;

using System.Reflection;

namespace SixThreeZero.Scripting
{


    public class GroupResultController : OperatorController, ITokenController
    {
        public override System.Object ExecuteAction(ITokenObject owner)
        {
            /*System.Object returnObj = 
            owner.PreviousToken.Value = owner.NextToken.Value;*/


            if (owner.NextToken == null)
            {
                if (owner.ChildToken != null)
                    return owner.ChildToken.Execute();
                else
                    throw new System.Exception("Group Result with no next action or child");
            }
            else
            {
                return owner.NextToken.Execute();
            }
        }

        public override object Value(ITokenObject owner)
        {

            if (owner.ChildToken != null)
                return owner.ChildToken.Execute();
            else
                return null;
            
        }
    }

}