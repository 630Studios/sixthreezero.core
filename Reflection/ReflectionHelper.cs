﻿using System.Reflection;
using System.Linq;

public static class ReflectionHelper
{

    public static System.Type FindTypeByFullName(string fullname)
    {
        return System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).FirstOrDefault(t => t.FullName.Equals(fullname));
    }

    public static System.Type FindTypeByAssemblyQualifiedName(string assemblyQualifiedName)
    {
        return System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).FirstOrDefault(t => t.AssemblyQualifiedName.Equals(assemblyQualifiedName));
    }

    public static System.Type[] FindClassesWithInterface(System.Type requiredInterface)
    {

        if (!requiredInterface.IsInterface)
            throw new System.Exception("Come on, thats not an interface your passed in.");


        return System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).Where(t => requiredInterface.IsAssignableFrom(t) && t.IsClass && !t.IsInterface && !t.IsAbstract).ToArray();

    }


    public static MethodInfo GetPublicStaticMethod(this System.Type type, string name, System.Type[] parameterTypes)
    {
        MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
        for (int i = 0; i < methods.Length; i++)
        {
            if (methods[i].Name == name)
            {
                ParameterInfo[] paramdata = methods[i].GetParameters();
                if (paramdata.Length != parameterTypes.Length)
                    continue;
                 
                bool isMatch = true;

                if (paramdata.Length != parameterTypes.Length)
                    continue;

                for (int n = 0; n < paramdata.Length; n++)
                {
                    if (paramdata[n].ParameterType != parameterTypes[n])
                    {
                        isMatch = false;
                        break;
                    }
                }

                if (isMatch)
                {
                    return methods[i];
                }

            }
        }
        return null;
    }

    public static MethodInfo GetPublicInstaceMethod(this System.Type type, string name, System.Type[] parameterTypes)
    {
        MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
        for (int i = 0; i < methods.Length; i++)
        {
            if (methods[i].Name == name)
            {
                ParameterInfo[] paramdata = methods[i].GetParameters();
                if (paramdata.Length != parameterTypes.Length)
                    continue;

                bool isMatch = true;

                if (paramdata.Length != parameterTypes.Length)
                    continue;

                for (int n = 0; n < paramdata.Length; n++)
                {
                    if (paramdata[n].ParameterType != parameterTypes[n])
                    {
                        isMatch = false;
                        break;
                    }
                }

                if (isMatch)
                {
                    return methods[i];
                }

            }
        }
        return null;
    }

    public static MethodInfo GetAdditionOp(this System.Type type, System.Type targetType)
    {
        return type.GetMethods(BindingFlags.Static | BindingFlags.Public).Where(p => p.Name == "op_Addition" && p.GetParameters()[1].ParameterType == targetType).FirstOrDefault(); ;
    }

    public static MethodInfo GetSubtractionOp(this System.Type type, System.Type targetType)
    {
        return type.GetMethods(BindingFlags.Static | BindingFlags.Public).Where(p => p.Name == "op_Subtraction" && p.GetParameters()[1].ParameterType == targetType).FirstOrDefault(); ; ;
    }

    public static MethodInfo GetMultiplicationOp(this System.Type type, System.Type targetType)
    {
        return type.GetMethods(BindingFlags.Static | BindingFlags.Public).Where(p=> p.Name == "op_Multiply" && p.GetParameters()[1].ParameterType == targetType).FirstOrDefault();
    }

    public static MethodInfo GetDivisionOp(this System.Type type, System.Type targetType)
    {
        return type.GetMethods( BindingFlags.Static | BindingFlags.Public).Where(p => p.Name == "op_Division" && p.GetParameters()[1].ParameterType == targetType).FirstOrDefault(); ; ;
    }
}

