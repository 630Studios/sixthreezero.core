﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SixThreeZero
{
    [System.AttributeUsage(System.AttributeTargets.Field)]
    class PositionHandle : System.Attribute { }

    [System.AttributeUsage(System.AttributeTargets.Field)]
    class RotationHandle : System.Attribute { }

    [System.AttributeUsage(System.AttributeTargets.Field)]
    class ScaleHandle : System.Attribute { }
}
